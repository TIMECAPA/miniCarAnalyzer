#ifndef DNDTOOLBAR_H
#define DNDTOOLBAR_H

#include <QToolBar>

/**
 * @brief QToolBar 확장 class
 */
class DnDToolBar : public QToolBar
{
    Q_OBJECT
public:
    explicit DnDToolBar(const QString &title, QWidget *parent = Q_NULLPTR);
    explicit DnDToolBar(QWidget *parent = Q_NULLPTR);
    ~DnDToolBar();
private:
    /**
     * @brief 미 구현
     */
    QColor color;

    /**
     * @brief 제목
     */
    QString m_strTitle;

    /**
     * @brief 마우스 Press 상태
     */
    bool    m_bPressed;
protected:
    void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
};

#endif // DNDTOOLBAR_H
