#ifndef TRANSFORMFRAME_H
#define TRANSFORMFRAME_H


#include <QFrame>
#include <QPushButton>
#include <QPixmap>
#include <QSizeGrip>

/**
 * @brief QFrame 확장 클래스
 */
class TransformFrame : public QFrame
{
    Q_OBJECT
    Q_PROPERTY(bool m_bEditable READ editable WRITE setEditable)
    Q_PROPERTY(QString m_strFrameName READ frameName WRITE setFrameName)
public:
    explicit TransformFrame(QWidget* parent = Q_NULLPTR, Qt::WindowFlags f = Qt::WindowFlags());
    ~TransformFrame();
private:
    /**
     * @brief 백 버퍼
     */
    QPixmap m_pixBackBuffer;

    /**
     * @brief QSizeGrip 인스턴스
     */
    QSizeGrip* m_pSizeGrip;    
protected:
    void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;

    void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseDoubleClickEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;

    void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;
private:
    /**
     * @brief Mouse Press 상태
     */
    bool m_bPressed;

    /**
     * @brief 편집 가능 상태
     */
    bool m_bEditable;

    /**
     * @brief 마우스 Press 전역 위치 x
     */
    int  m_nMPressGlobalX;

    /**
     * @brief 마우스 Press 전역 위치 y
     */
    int  m_nMPressGlobalY;

    /**
     * @brief 마우스 Press Rect
     */
    QRect m_rectMPressGeo;

    /**
     * @brief 부모
     */
    QWidget* m_pParent;

    /**
     * @brief 숨김 버튼 Rect
     */
    QRect m_rectHideButton;

    /**
     * @brief Frame 이름
     */
    QString m_strFrameName;

protected:
    void darawTitle(QPainter *painter,QRect& rect);
    void drawFrameBorder(QPainter *painter,QRect& rect);
    void drawHideButton(QPainter *painter,QRect& rect);
    void updateBackBuffer();
public:
    bool editable() const;
    void setEditable(bool);
    QString frameName() const;
    void setFrameName(QString strFrameName);
};

#endif // TRANSFORMFRAME_H
