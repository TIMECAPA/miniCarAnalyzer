#ifndef DBDATAANALTOOLDLG_H
#define DBDATAANALTOOLDLG_H

#include <QDialog>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QTextCodec>
#include <QPrinter>
#include "qcustomplot.h"

namespace Ui {
class DBDataAnalToolDlg;
}

/**
 * @brief DB 데이터 분석 툴 다이얼로그 클래스
 */
class DBDataAnalToolDlg : public QDialog
{
    Q_OBJECT
public:
    explicit DBDataAnalToolDlg(QWidget *parent = 0);
    ~DBDataAnalToolDlg();

private slots:
    void on_btnDoAnal_clicked();
    void slotFinished(QNetworkReply *reply);
    void on_btnPrint_clicked();
    void on_btnPreviewPrint_clicked();
    void slotPrint(QPrinter*);
    void on_btnOpenPrintedFile_clicked();
    void itemClick(QCPAbstractItem *item, QMouseEvent *event);
    void plottableClick(QCPAbstractPlottable *plottable, int dataIndex, QMouseEvent *event);
    void mouseMove(QMouseEvent *event);
    void on_bntSaveDataToFile_clicked();

private:
    Ui::DBDataAnalToolDlg *ui;

    /**
     * @brief 네트워크 접속 관리
     */
    QNetworkAccessManager *nam;
private:
    /**
     * @brief 데이터 최대 값
     */
    double         m_dbMax;

    /**
     * @brief 데이터 최소 값
     */
    double         m_dbMin;

    /**
     * @brief 분석 데이터 수
     */
    QString g_strDataNum;

    /**
     * @brief 사용자
     */
    QString        m_strUser;

    /**
     * @brief 요청 URL
     */
    QString        m_strReqURL;

    /**
     * @brief 요청 호스트
     */
    QString        m_strReqHost;

    /**
     * @brief 요청 아이템 이름
     */
    QString        m_strReqItemName;

    /**
     * @brief 요청 시작 일
     */
    QString        m_strReqStartDate;

    /**
     * @brief 요청 종료 일
     */
    QString        m_strReqEndDate;

    /**
     * @brief Raw 데이터 여부
     */
    bool           m_bRawData;

    /**
     * @brief Raw 분석 컨테이너 이름
     */
    QString m_strContainerName;

private:
    void replotAnalToolDemo(QString strGraphName, QVector<double>& x, QVector<double>& y);
    void print();
    void printPlot();
    void previewPrintPlot();
    void setupPrint(QPrinter* printer);
    QString getPrintHTML();
    void setEnableCtrls(bool bEnable=true);

};

#endif // DBDATAANALTOOLDLG_H
