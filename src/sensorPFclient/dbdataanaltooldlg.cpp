#include "dbdataanaltooldlg.h"
#include "ui_dbdataanaltooldlg.h"



#include <QtSql>
#include <QNetworkAccessManager>
#include <QtXml>

#include <QDebug>
#include <QMessageBox>


/**
 * @brief 생성자
 * @param parent
 */
DBDataAnalToolDlg::DBDataAnalToolDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DBDataAnalToolDlg)
{

    ui->setupUi(this);
    ui->dateTimeEditStart->setDateTime(QDateTime::currentDateTime().addSecs(-60*10));
    ui->dateTimeEditStart->setDisplayFormat("yyyy.MM.dd hh.mm.ss");
    ui->dateTimeEditEnd->setDateTime(QDateTime::currentDateTime());
    ui->dateTimeEditEnd->setDisplayFormat("yyyy.MM.dd hh.mm.ss");

    m_dbMax = std::numeric_limits<double>::lowest();
    m_dbMin = std::numeric_limits<double>::max();

    m_strUser         = "raynor";
    // m_strReqHost      =  "http://raynor2.codns.com:7579";
    m_strReqHost      =  "http://garigulls.com:7579";
    m_strReqURL       = m_strReqHost;
    m_strReqItemName  = ui->cboContainer->currentText();
    m_strReqStartDate = ui->dateTimeEditStart->dateTime().toString("yyyyMMddThhmmss");
    m_strReqEndDate   = ui->dateTimeEditEnd->dateTime().toString("yyyyMMddThhmmss");


    //RESTful
    nam = new QNetworkAccessManager(this);

    connect(nam,SIGNAL(finished(QNetworkReply*)),this,SLOT(slotFinished(QNetworkReply*)));

    //MySQL
    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");
    // db.setHostName("raynor2.codns.com");
    db.setHostName("garigulls.com");
    db.setDatabaseName("mobiusdb3");
    db.setUserName("root");
    db.setPassword("soft0987");

    if (!db.open()){
        QMessageBox::critical(0, QObject::tr("Database Error"), db.lastError().text());
    }
    QSqlQuery query("SELECT * FROM cnt");

    QString containerName[10000];

    int index=0;
    while(query.next())
    {
        containerName[index] = query.value(0).toString();
        ui->cboContainer->addItem(containerName[index]);
        index++;
    }
    db.close();

    //Plot
#if QT_VERSION < QT_VERSION_CHECK(4, 7, 0)
  QMessageBox::critical(this, "", "You're using Qt < 4.7, the realtime data demo needs functions that are available with Qt 4.7 to work properly");
#endif

    QFont serifFont("Times", 8, QFont::Bold);

    ui->customPlot->legend->setVisible(true);
    ui->customPlot->legend->setFont(serifFont);

    QSharedPointer<QCPAxisTickerDateTime> timeTicker(new QCPAxisTickerDateTime);
    ui->customPlot->xAxis->setRange(-60*3.5, 60*11);

    //timeTicker->setTimeFormat("%m:%s");
    //timeTicker->setDateTimeFormat("day dd\nhh:mm:ss");
    timeTicker->setDateTimeFormat("yy.MM.dd \nhh:mm:ss");\
    timeTicker->setDateTimeSpec(Qt::UTC);
    //timeTicker->setTickStepStrategy(QCPAxisTicker::TickStepStrategy::tssReadability);
    timeTicker->setTickCount(10);
    ui->customPlot->xAxis->setTicker(timeTicker);
    ui->customPlot->axisRect()->setupFullAxesBox();


    // set some pens, brushes and backgrounds:
    ui->customPlot->xAxis->setBasePen(QPen(Qt::white, 1));
    ui->customPlot->yAxis->setBasePen(QPen(Qt::white, 1));
    ui->customPlot->xAxis->setTickPen(QPen(Qt::white, 1));
    ui->customPlot->yAxis->setTickPen(QPen(Qt::white, 1));
    ui->customPlot->xAxis->setSubTickPen(QPen(Qt::white, 1));
    ui->customPlot->yAxis->setSubTickPen(QPen(Qt::white, 1));
    ui->customPlot->xAxis->setTickLabelColor(Qt::white);
    ui->customPlot->yAxis->setTickLabelColor(Qt::white);
    ui->customPlot->xAxis->grid()->setPen(QPen(QColor(140, 140, 140), 1, Qt::DotLine));
    ui->customPlot->yAxis->grid()->setPen(QPen(QColor(140, 140, 140), 1, Qt::DotLine));
    ui->customPlot->xAxis->grid()->setSubGridPen(QPen(QColor(80, 80, 80), 1, Qt::DotLine));
    ui->customPlot->yAxis->grid()->setSubGridPen(QPen(QColor(80, 80, 80), 1, Qt::DotLine));
    ui->customPlot->xAxis->grid()->setSubGridVisible(true);
    ui->customPlot->yAxis->grid()->setSubGridVisible(true);
    ui->customPlot->xAxis->grid()->setZeroLinePen(Qt::NoPen);
    ui->customPlot->yAxis->grid()->setZeroLinePen(Qt::NoPen);
    ui->customPlot->xAxis->setUpperEnding(QCPLineEnding::esSpikeArrow);
    ui->customPlot->yAxis->setUpperEnding(QCPLineEnding::esSpikeArrow);

    QLinearGradient plotGradient;
    plotGradient.setStart(0, 0);
    plotGradient.setFinalStop(0, 350);
    plotGradient.setColorAt(0, QColor(80, 80, 80));
    plotGradient.setColorAt(1, QColor(50, 50, 50));
    ui->customPlot->setBackground(plotGradient);

    QLinearGradient axisRectGradient;
    axisRectGradient.setStart(0, 0);
    axisRectGradient.setFinalStop(0, 350);
    axisRectGradient.setColorAt(0, QColor(80, 80, 80));
    axisRectGradient.setColorAt(1, QColor(30, 30, 30));
    ui->customPlot->axisRect()->setBackground(axisRectGradient);

    ui->customPlot->setInteraction(QCP::iRangeDrag);
    //ui->customPlot->axisRect()->setRangeDrag(Qt::Vertical|Qt::Horizontal);
    ui->customPlot->axisRect()->setRangeDragAxes(ui->customPlot->xAxis,ui->customPlot->yAxis);

    ui->customPlot->setInteraction(QCP::iRangeZoom);
    ui->customPlot->axisRect()->setRangeZoom(Qt::Vertical|Qt::Horizontal);
    // make left and bottom axes transfer their ranges to right and top axes:
    //  connect(ui->customPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), ui->customPlot->xAxis2, SLOT(setRange(QCPRange)));
    //  connect(ui->customPlot->yAxis, SIGNAL(rangeChanged(QCPRange)), ui->customPlot->yAxis2, SLOT(setRange(QCPRange)));
    ui->customPlot->setInteraction(QCP::iSelectItems);
    connect(ui->customPlot, SIGNAL(itemClick(QCPAbstractItem*, QMouseEvent*)), this, SLOT(itemClick(QCPAbstractItem*, QMouseEvent*)));
    connect(ui->customPlot, SIGNAL(plottableClick(QCPAbstractPlottable*,int ,QMouseEvent*)), this, SLOT(plottableClick(QCPAbstractPlottable*,int ,QMouseEvent*)));
    connect(ui->customPlot, SIGNAL(mouseMove(QMouseEvent*)), this, SLOT(mouseMove(QMouseEvent*)));


    m_bRawData = false;
}

/**
 * @brief 소멸자
 */
DBDataAnalToolDlg::~DBDataAnalToolDlg()
{
    delete ui;
}

/**
 * @brief 분석 처리 클릭 이벤트
 */
void DBDataAnalToolDlg::on_btnDoAnal_clicked()
{
    setEnableCtrls(false);
    QApplication::setOverrideCursor(Qt::WaitCursor);

    m_dbMax = std::numeric_limits<double>::lowest();
    m_dbMin = std::numeric_limits<double>::max();

    ui->customPlot->clearGraphs();
    ui->customPlot->replot();

    QDateTime dtStart = ui->dateTimeEditStart->dateTime();
    dtStart = dtStart.addSecs(-9*60*60);
    QDateTime dtEnd = ui->dateTimeEditEnd->dateTime();
    dtEnd   = dtEnd.addSecs(-9*60*60);
    QString strContainerName =  ui->cboContainer->currentText();
    m_strContainerName = strContainerName;
    QString strReqStartDate = dtStart.toString("yyyyMMddThhmmss");
    QString strReqEndDate   = dtEnd.toString("yyyyMMddThhmmss");

    QStringList strlistRI = strContainerName.split('/');
    QString strSQLQuery = "";
    int nRICnt = strlistRI.count();
    if(nRICnt > 4){
        QString strRI = strlistRI[nRICnt-1];
        QString strSection = strRI.section("raw",0,0);
        if(strSection.compare(strRI)==0){
            m_bRawData = false;
            strSection = strRI.section("p2p",0,0);
            if(strSection.compare(strRI)==0){//RMS
                strSection = strRI.section("rms",0,0);
                if(strSection.compare(strRI)!=0){
                   strSQLQuery = "CALL analysisRangeRMS2('" + strContainerName + "' , '" + strReqStartDate + "' , '" + strReqEndDate +"')";
                }
            }else{//P2P
                strSQLQuery = "CALL analysisRangeP2P2('" + strContainerName + "' , '" + strReqStartDate + "' , '" + strReqEndDate +"')";
            }
        }else{
            m_bRawData = true;
            strSQLQuery = "CALL analysisRangeRaw2('" + strContainerName + "' , '" + strReqStartDate + "' , '" + strReqEndDate +"')";
        }

        QVector<double> vXAxisVal, vYAxisVal;
        if(!strSQLQuery.isEmpty()){
            QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");
#ifdef QT_DEBUG
            //QLibraryInfo::isDebugBuild())
            //db.setHostName("112.166.14.252");
            db.setHostName("garigulls.com");
#else
            db.setHostName("garigulls.com");
#endif
            db.setDatabaseName("mobiusdb3");
            db.setUserName("root");
            db.setPassword("soft0987");
            if (!db.open()){
                QMessageBox::critical(0, QObject::tr("Database Error"), db.lastError().text());
            }else{
                double dbInterval = 0.0;
                int    nContentsSize = 0;

                QSqlQuery query(strSQLQuery);
                //query.record().count());
                //query.size());

                int index=0;
                double dbX = 0, dbY =0;
                QString strContents = "";

                while (query.next()){
                    if(m_bRawData){
                        strContents = query.value(3).toString();
                        QStringList strListContents = strContents.split(' ');
                        nContentsSize = strListContents.size();

                        if(nContentsSize>2){
                           dbX = strListContents[0].toDouble() + (9*60*60);//+9시간 1479939172.68
                           double dbDiff = strListContents[nContentsSize-1].toDouble() - strListContents[0].toDouble();
                           dbInterval = dbDiff/(nContentsSize-2);
                           for(int i=1; i< nContentsSize-1;i++){
                               dbY = strListContents[i].toDouble();
                               vXAxisVal.push_back(dbX);
                               vYAxisVal.push_back(dbY);
                               dbX += dbInterval;
                           }
                        }else{
                           dbX = strListContents[0].toDouble() + (9*60*60);
                           dbY = strListContents[1].toDouble();
                           vXAxisVal.push_back(dbX);
                           vYAxisVal.push_back(dbY);
                        }
                    }else{
                        QString strCreationTime = query.value(0).toString();
                        const QDateTime dateCT = QDateTime::fromString(strCreationTime, "yyyyMMddTHHmmss");
                        QDateTime dateKoCT = dateCT.addSecs(9*60*60); ////구해진 시간에 +9 더해진값이 됨
                        dateKoCT.setTimeSpec(Qt::UTC);
                        qint64 nMSec = dateKoCT.toMSecsSinceEpoch(); //1479939599000
                        //qDebug() << QLocale().name() << endl;
                        dbX = nMSec/1000.0;
                        //dbX = nMSec/1.0;
                        vXAxisVal.push_back(dbX);
                        dbY = query.value(3).toDouble();
                        vYAxisVal.push_back(dbY);
                    }
                    index++;
                }
            }
            m_dbMax = *std::max_element(vYAxisVal.constBegin(),vYAxisVal.constEnd());
            m_dbMin = *std::min_element(vYAxisVal.constBegin(),vYAxisVal.constEnd());
            replotAnalToolDemo(strContainerName, vXAxisVal,vYAxisVal);
            db.close();
        }else{
            QApplication::setOverrideCursor(Qt::ArrowCursor);
            QMessageBox::information(this,"알림","해당 컨테이너는 질의 할 데이터가 존재하지 않습니다." );
        }

    }else{
        QApplication::setOverrideCursor(Qt::ArrowCursor);
        QMessageBox::information(this,"알림","해당 컨테이너는 데이터 분석에 해당되지 않습니다." );
    }
    setEnableCtrls(true);
    QApplication::setOverrideCursor(Qt::ArrowCursor);


//    ui->customPlot->clearGraphs();
//    ui->customPlot->replot();
//    ui->lblStatus->setText("분석 시작  ..........");
//    //qDebug() << "on_btnDoAnal_clicked" << endl;
//    // QString url = "http://192.168.0.2:7579/mobius-yt/bridgeA/soc1_ch1_item03?cra=20151019T074218";
//    QString url = "http://210.105.85.7:7579/mobius-yt/bridgeA/soc1_ch1_item03?cra=20151006T074218?crb=20161006T074218";
//    m_strReqURL = m_strReqHost;
//    m_strReqItemName = ui->cboContainer->currentText();
//    QStringList strlistRI = m_strReqItemName.split('/');
//    int nRICnt = strlistRI.count();
//    if(nRICnt > 0){
//        QString strRI = strlistRI[nRICnt-1];
////        QString str;
////        QString csv = "forename,middlename,surname,phone";
////        QString path = "/usr/local/bin/myapp"; // First field is empty
////        QString::SectionFlag flag = QString::SectionSkipEmpty;

////        str = csv.section(',', 2, 2);   // str == "surname"
////        str = path.section('/', 3, 4);  // str == "bin/myapp"
////        str = path.section('/', 3, 3, flag); // str == "myapp"
//        QString strSection = strRI.section("raw",0,0);
//        if(strSection.compare(strRI)==0){
//            //QMessageBox::information(this,strRI,strSection );
//            m_bRawData = false;
//        }else{
//            //QMessageBox::information(this,strRI,strSection );
//            m_bRawData = true;
//        }

//    }

//    m_strReqURL += m_strReqItemName;
//    //strURL += "?cra=20151019T074218&crb=20161020T074218";
//    m_strReqStartDate = ui->dateTimeEditStart->dateTime().toString("yyyyMMddThhmmss");
//    m_strReqEndDate   = ui->dateTimeEditEnd->dateTime().toString("yyyyMMddThhmmss");

//    m_strReqURL += "?rcn=4&cra=" + m_strReqStartDate; // + "&";
//    // m_strReqURL += "crb="  + m_strReqEndDate;

//    // qDebug() << m_strReqURL;

//    //QString paras = ui->pTextEdit_paras->toPlainText();
//    QByteArray post_data;
//    /*
//    QByteArray b((const char*) (paras.utf16()), paras.size() * 2);
//    post_data.append(b);
//    */

//    //url = url + QString("?") + paras;
//    QNetworkRequest request= QNetworkRequest(QUrl(m_strReqURL));

//    request.setRawHeader("accept", "application/xml");
//    request.setRawHeader("x-m2m-ri", "12345");
//    request.setRawHeader("x-m2m-origin", "/0.2.481.1.21160310105204806");
//    request.setRawHeader("nmtype", "long");
//    request.setRawHeader("cache-control", "no-cache");
//    request.setRawHeader("postman-token", "3690e173-5a9e-5c6a-3e69-d7a802cafc8a");

//    if(post_data.isEmpty())
//    {
//        nam->get(request);
//    }
//    else
//    {
//        nam->post(request,post_data);
//    }
}

/**
 * @brief 네트워크 응답 처리 Slot
 * @param reply
 */
void DBDataAnalToolDlg::slotFinished(QNetworkReply *reply)
{
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));

    if(reply->error() == QNetworkReply::NoError)
    {
        QString strStart="0.0";
        QString strEnd  ="0.0";

        double dbInterval = 0.0;
        int    nContentsSize = 0;

        QString strGraphName="NoName";
        QVector<double> x, y;
        QXmlStreamReader xml(reply->readAll());
        double dbX=0.0;
        bool bContainer=false;
        while(!xml.atEnd()) //끝이 아니면 반복한다
        {
            QXmlStreamReader::TokenType token=xml.readNext(); //

            if(token==QXmlStreamReader::StartDocument){
                continue;
            }
            if(token==QXmlStreamReader::StartElement)
            {
                QStringRef strElementName = xml.name();

                QStringRef strAttrName;
                if(strElementName=="container"){
                    bContainer = true;
                    continue;
                }
                if(strElementName=="contentInstance"){
                    strAttrName = xml.attributes().at(0).name();
                    bContainer = false;
                    continue;
                }
                if(bContainer){
                    strElementName = xml.name();
                    if(strElementName == "labels"){
                        strGraphName =  xml.readElementText();;
                    }
                    qDebug() << strElementName <<  endl;
                    continue;
                }
                strElementName = xml.name();
                if(strElementName =="creationTime")
                {
                   QString strCreationTime =  xml.readElementText();
                   //qDebug() << strElementName << " : " << strCreationTime << endl;
                   //if(xml.readElementText().toInt()==0)
                   //isChecked=false;
                   //else
                   //isChecked=true;
                   //20161019T051936
//                   QDateTime date = QDateTime::fromString("2010-10-25T10:28:58.570Z", "yyyy-MM-ddTHH:mm:ss.zzzZ");
//                   date.setTimeSpec(Qt::UTC);
//                   QDateTime local = date.toLocalTime();
                   if(m_bRawData){
//                       QDateTime date = QDateTime::fromString(strCreationTime, "yyyyMMddTHHmmss");
//                       date.setTimeSpec(Qt::UTC);//GMT->UTC 변경 후 Epoch 값을 구한 후 처리(GMT, UTC는 소수점 차이)
//                       uint time_t  = date.toTime_t();
//                       dbX = static_cast<double>(time_t);
                       dbX = strStart.toDouble();
                       for(int i=1; i<nContentsSize-1; i++){
                           x.push_back(dbX);
                           dbX += dbInterval;
                       }
                   }else{
                       QDateTime date = QDateTime::fromString(strCreationTime, "yyyyMMddTHHmmss");
                       date.setTimeSpec(Qt::UTC);//GMT->UTC 변경 후 Epoch 값을 구한 후 처리
                       qint64 qiMSec = date.toMSecsSinceEpoch();
                       QDateTime date2 = QDateTime::fromMSecsSinceEpoch(qiMSec);
                       date2.setTimeSpec(Qt::UTC);//GMT->UTC 변경 후 Epoch 값을 구한 후 처리

//                       QTime time = date.time();
                       //dbX = time.elapsed()/1000.0;
                       dbX = qiMSec/1000.0;
                       x.push_back(dbX);
                   }
                   //dbX += 1.0;
                }
                strElementName = xml.name();
                if(strElementName=="content")
                {
                    QString strContents =  xml.readElementText();
                    double dbVal = 0.0;
                    if(m_bRawData){
                        QStringList strListContents = strContents.split(' ');
                        nContentsSize = strListContents.size();
                        if(nContentsSize>2){
                            strStart = strListContents[0];
                            //QDateTime date3 = QDateTime::fromMSecsSinceEpoch(strStart.toInt());
                            strEnd   = strListContents[nContentsSize-1];
                            double dbDiff = strEnd.toDouble() - strStart.toDouble();
                            dbInterval = dbDiff/(nContentsSize-2);
                            QString strVal = "";
                            for(int i=1; i< nContentsSize-1;i++){
                                strVal = strListContents[i];
                                dbVal = strVal.toDouble();
                                y.push_back(dbVal);
                            }
                        }else{
                            y.push_back(dbVal);
                        }
                    }else{
                        dbVal = strContents.toDouble();
                        y.push_back(dbVal);
                    }

                }
            }

        }

        m_dbMax = *std::max_element(y.constBegin(),y.constEnd());
        m_dbMin = *std::min_element(y.constBegin(),y.constEnd());
        replotAnalToolDemo(strGraphName, x,y);
    }
    else
    {
        //ui->textEdit_result->setPlainText(reply->errorString());
        QString strError =  "상태 " + reply->errorString();
        ui->lblStatus->setText(strError);

        m_bRawData = false;
    }
    setEnableCtrls(true);
    QApplication::setOverrideCursor(Qt::ArrowCursor);
}

/**
 * @brief 분석 툴 Replot
 * @param strGraphName
 * @param x
 * @param y
 */
void DBDataAnalToolDlg::replotAnalToolDemo(QString strGraphName,QVector<double>& x, QVector<double>& y)
{

    //strWinTitle = "Quadratic Demo";
    // generate some data:

//    QVector<double> x(101), y(101); // initialize with entries 0..100
//    for (int i=0; i<101; ++i)
//    {
//      x[i] = i/50.0 - 1; // x goes from -1 to 1
//      y[i] = x[i]*x[i];  // let's plot a quadratic function
//    }
    // create graph and assign data to it:
    ui->customPlot->addGraph();
    ui->customPlot->graph(0)->setName(strGraphName);
    ui->customPlot->graph(0)->setData(x, y);
    // give the axes some labels:
    ui->customPlot->xAxis->setLabel("시간");
    ui->customPlot->yAxis->setLabel("값");
    // set axes ranges, so we see all data:
//    ui->customPlot->xAxis->setRange(-1, 1000);
//    ui->customPlot->yAxis->setRange(-1, 1000);

//    QCPGraph *graph2 = ui->customPlot->addGraph();
//    graph2->setData(x, y);
//    graph2->setPen(Qt::NoPen);
//    graph2->setBrush(QColor(200, 200, 200, 20));
//    graph2->setChannelFillGraph(ui->customPlot->graph(0));


    //ui->customPlot->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, QPen(Qt::black, 1.5), QBrush(Qt::white), 9));
    ui->customPlot->graph(0)->setPen(QPen(QColor(120, 120, 120), 2));

//    QCPBars *bars1 = new QCPBars(ui->customPlot->xAxis, ui->customPlot->yAxis);
//    bars1->setWidth(9/(double)x.size());
//    bars1->setData(x, y);
//    bars1->setPen(Qt::NoPen);
//    bars1->setBrush(QColor(10, 140, 70, 160));



//    // move bars above graphs and grid below bars:
//    ui->customPlot->addLayer("abovemain", ui->customPlot->layer("main"), QCustomPlot::limAbove);
//    ui->customPlot->addLayer("belowmain", ui->customPlot->layer("main"), QCustomPlot::limBelow);
//    ui->customPlot->graph(0)->setLayer("abovemain");
//    ui->customPlot->xAxis->grid()->setLayer("belowmain");
//    ui->customPlot->yAxis->grid()->setLayer("belowmain");



    ui->customPlot->graph(0)->rescaleAxes();
    ui->customPlot->replot();

    QString strStatus = QString("%1 %2  %3 %4").arg("그래프명 : ",strGraphName,"데이터 수: ",QString::number(ui->customPlot->graph(0)->data()->size()));

    g_strDataNum = QString::number(ui->customPlot->graph(0)->data()->size());

    ui->lblStatus->setText(strStatus);

    QSharedPointer<QCPGraphDataContainer> data = ui->customPlot->graph(0)->data();


}

/**
 * @brief 프린트 클릭 이벤트
 */
void DBDataAnalToolDlg::on_btnPrint_clicked()
{
    print();
}

/**
 * @brief 미리보기 클릭 이벤트
 */
void DBDataAnalToolDlg::on_btnPreviewPrint_clicked()
{
    previewPrintPlot();
}

/**
 * @brief 프린트
 */
void DBDataAnalToolDlg::print()
{
#ifndef QT_NO_PRINTDIALOG
//    QTextDocument *document = textEdit->document();
//    QPrinter printer;

//    QPrintDialog dlg(&printer, this);
//    if (dlg.exec() != QDialog::Accepted) {
//        return;
//    }

//    document->print(&printer);
//    statusBar()->showMessage(tr("Ready"), 2000);

    printPlot();
#endif
}

/**
 * @brief 프린터 처리
 */
void DBDataAnalToolDlg::printPlot()
{

    QPrinter printer;
//    QPrintDialog dlg(&printer, this);
//    dlg.setWindowTitle("Print Document");
//    dlg.setVisible(false);
    //if (dlg.exec()) {
    printer.setOutputFileName("1.pdf");
    setupPrint(&printer);
    //}
}

/**
 * @brief 미리보기 프린트
 */
void DBDataAnalToolDlg::previewPrintPlot()
{
    //QPrinter             printer( QPrinter::HighResolution );
    QPrinter             printer;
    QPrintPreviewDialog  preview( &printer, this );

    connect( &preview, SIGNAL(paintRequested(QPrinter*)), SLOT(slotPrint(QPrinter*)) );
    preview.exec();

}

/**
 * @brief 프린트 처리 Slot
 * @param printer
 */
void  DBDataAnalToolDlg::slotPrint( QPrinter* printer )
{
  // create painter for drawing print page  
    setupPrint(printer);
}

/**
 * @brief 프린트 셋업
 * @param printer
 */
void DBDataAnalToolDlg::setupPrint(QPrinter* printer)
{
    QPainter painter(printer);
    int      w = printer->pageRect().width();
    int      h = printer->pageRect().height();
    QRect    page( 0, 0, w, h );

    QFont    font = painter.font();
    font.setPixelSize( (w+h) / 100 );
    painter.setFont( font );

    // draw labels in corners of page
    painter.drawText( page, Qt::AlignTop    | Qt::AlignLeft, "QSimulate" );
    painter.drawText( page, Qt::AlignBottom | Qt::AlignLeft, QString(getenv("USERNAME")) );
    painter.drawText( page, Qt::AlignBottom | Qt::AlignRight,
                      QDateTime::currentDateTime().toString( Qt::DefaultLocaleShortDate ) );

//    QString strTitle = "QString.toHtmlEscaped()";
//    QString html = strTitle.toHtmlEscaped();
//    QString table_style= QString("<style type=\"text/css\">"
//                                 ".tg  { table-layout: fixed; width: 200; }"
//                                 ".tg td{padding-bottom: 5px;border-style:solid;border-width:0px;}"
//                                 ".tg .tg-3x1q{color: rgba(255, 255, 255, 0.5); text-align:right}"
//                                 ".tg .tg-6bqv{color: rgba(255, 255, 255, 0.5); padding-left: 5px;}"
//                                 "</style>");
//    //QLocale::toString();
//    QTextDocument textDoc;
//    QSize sizePrinterPage = printer.pageRect().size();
//    textDoc.setPageSize(sizePrinterPage);
//    QSizeF sizeF = textDoc.size();
//    //QTextCursor cur(&doc);
//    //cur.insertBlock(format);
//    //textDoc.setDefaultStyleSheet(table_style);
//    textDoc.setHtml(getPrintHTML());
//    textDoc.drawContents(&painter);

    QTextDocument textDoc;
    textDoc.setHtml(getPrintHTML());
    textDoc.print(printer);
    textDoc.drawContents(&painter);

    QPixmap pixmap = ui->customPlot->toPixmap();
    QImage image = pixmap.toImage();

    QRect rect = painter.viewport();
    QSize size = image.size();
    size.scale(rect.size(),Qt::KeepAspectRatio);
    painter.setViewport(rect.x(),rect.y(),size.width(),size.height());
    painter.setWindow(image.rect());

    painter.drawImage(0,1010,image);
//  painter.drawText(100, 100, 500, 500, Qt::AlignLeft|Qt::AlignTop, text);

    // painter.drawText(QPoint(0,1450), "분석서버 : " + m_strReqURL);

    //// draw simulated landscape
    page.adjust( w/20, h/20, -w/20, -h/20 );

}

/**
 * @brief HTML 형태 가져오기
 * @return
 */
QString DBDataAnalToolDlg::getPrintHTML()
{
    QString strMax = QString::number(m_dbMax,'f',12);
    QString strMin = QString::number(m_dbMin,'f',12);
    QString strPeak2Peak = QString::number((m_dbMax - m_dbMin), 'f',12);
    QString strPeak = QString::number((m_dbMax - m_dbMin)/2, 'f',12);
    QString strDataNum = g_strDataNum;

    QString strTableHtml = QString(
                "<style>"
                "table.imagetable {"
                "    font-family: verdana,arial,sans-serif;"
                "    font-size:19px;"
                "    color:#333333;"
                "    border-width: 1px;"
                "    border-color: #999999;"
                "    border-collapse: collapse;"
                "}"
                "table.imagetable th {"
                "    background:#b5cfd2;"
                "    border-width: 1px;"
                "    padding: 8px;"
                "    border-style: solid;"
                "    border-color: #999999;"
                "}"
                "table.imagetable td {"
                "    background:#dcddc0;"
                "    border-width: 1px;"
                "    padding: 8px;"
                "    border-style: solid;"
                "    border-color: #999999;"
                "}"
                "</style>"

                "<br><br><br>"

                                 "<table width=750 border=0><tr><td align=center><font size=25><b>성능분석 결과보고서</b></font></td></tr></table>"
                                 "<br>"
                                 "<table width=750 class=imagetable>"
                                 "<tr>"
                                   "<th>분석가</th>"
                                   "<th>" + m_strUser + "</th>"
                                 "</tr>"
                                 "<tr>"
                                   "<td width=300>분석서버</td>"
                                   "<td>" + m_strReqURL + "</td>"
                                 "</tr>"
                                 "<tr>"
                                   "<td>분석시작시간</td>"
                                   "<td>" + QDateTime::fromString(m_strReqStartDate, "yyyyMMddTHHmmss").toString("yyyy.MM.dd ddd hh:mm:ss")+"</td>"
                                 "</tr>"
                                 "<tr>"
                                   "<td>분석종료시간</td>"
                                   "<td>" + QDateTime::fromString(m_strReqEndDate, "yyyyMMddTHHmmss").toString("yyyy.MM.dd ddd hh:mm:ss ") + "</td>"
                                 "</tr>"
                                  "<tr>"
                                    "<td>분석 데이터 갯수</td>"
                                    "<td>" + strDataNum + "</td>"
                                  "</tr>"
                                 "</table>");

    QString strTableHtml2 = QString(

                                    "<br><br><br><br>"
                                    "<table width=750 border=0><tr><td align=left><font size=15><b>항목별 분석결과</b></font></td></tr></table>"
                                    "<table width=750 class=imagetable>"
                                    "<tr>"
                                      "<th>분석컨테이너</th>"
                                      "<th><font size=4>" + m_strContainerName + "</font></th>"
                                    "</tr>"
                                    "<tr>"
                                      "<td width=300>Peak</td>"
                                      "<td>" + strPeak + "</td>"
                                    "</tr>"
                                    "<tr>"
                                      "<td>Peak to Peak</td>"
                                      "<td>" + strPeak2Peak +"</td>"
                                    "</tr>"
                                    "<tr>"
                                      "<td>Maximum value</td>"
                                      "<td>" + strMax + "</td>"
                                    "</tr>"
                                     "<tr>"
                                       "<td>Minimum value</td>"
                                       "<td>" + strMin + "</td>"
                                     "</tr>"
                                    "</table>"
                "<br><br><br>"
                "<table width=750 border=0><tr><td align=left><font size=15><b>분석 그래프</b></font></td></tr></table>"
                             );

   strTableHtml = strTableHtml + strTableHtml2;

   return strTableHtml;
}



/**
 * @brief 컨트롤 활성화/비활성화
 * @param bEnable
 */
void DBDataAnalToolDlg::setEnableCtrls(bool bEnable)
{
   ui->btnDoAnal->setEnabled(bEnable);
   ui->btnOpenPrintedFile->setEnabled(bEnable);
   ui->btnPreviewPrint->setEnabled(bEnable);
   ui->btnPrint->setEnabled(bEnable);
   ui->dateTimeEditEnd->setEnabled(bEnable);
   ui->dateTimeEditStart->setEnabled(bEnable);

}

/**
 * @brief 프린트된 파일 열기 클릭 이벤트
 */
void DBDataAnalToolDlg::on_btnOpenPrintedFile_clicked()
{
    QDesktopServices::openUrl(QUrl::fromLocalFile("1.pdf"));
}

/**
 * @brief 데이터 파일 저장하기 클릭 이벤트
 */
void DBDataAnalToolDlg::on_bntSaveDataToFile_clicked()
{
    //QString strSaveFileName = QFileDialog::getOpenFileName(this,QString::fromLocal8Bit("저장할 파일을 선택하세요"),QString::fromLocal8Bit("압축파일 (*.txt)"));

    QString filters("Text files (*.txt);;All files (*.*)");
    QString defaultFilter("Text files (*.txt)");


    QTextCodec *textCodec = QTextCodec::codecForName("eucKR");
    QString strTitle = "저장하기";
    strTitle = textCodec->toUnicode(strTitle.toLocal8Bit());

    QString strSaveFileName = QFileDialog::getSaveFileName(0, strTitle, QDir::currentPath(),
        filters, &defaultFilter);
    QFile file(strSaveFileName);
    if(file.open(QIODevice::WriteOnly)){
        if(ui->customPlot->graph(0)){
            QSharedPointer<QCPGraphDataContainer> data =ui->customPlot->graph(0)->data();
            if(data){
                QString strKeyVal="0.0";
                QString strVal="0.0";
                QTextStream out(&file);
                QVector<QCPGraphData>::iterator it = data->begin();
                const QVector<QCPGraphData>::iterator itEnd = data->end();
                int i = 0;
                QDateTime dateTime;
                qint64 qiMSec = 0;
                while (it != itEnd) {
                    qiMSec = (it->key)*1000;
                    dateTime = QDateTime::fromMSecsSinceEpoch(qiMSec);
                    dateTime = dateTime.addSecs(-9*60*60);
                    //strKeyVal.sprintf("%.3f %.18f",it->key,it->value);
                    strKeyVal.sprintf("%s %.18f",dateTime.toString("yyyyMMdd_HHmmss.zzz").toLocal8Bit().constData(),it->value);
                    out << strKeyVal << "\r\n";
                    ++it;
                    ++i;
                }
            }
        }
    }
    /* Direct object construction approach */
//    QFileDialog fileDialog(0, "Save file", QDir::currentPath(), filters);
//    fileDialog.selectNameFilter(defaultFilter);
//    fileDialog.exec();

//    QFileDialog fileDialog(this, tr("Open File"),
//                            tr("C:\\Documents and Settings\\jihan-Desktop\\My Documents\\My Pictures"),
//                            tr("Images (*.bmp *.png *.jpeg *.jpg)"));

//    QStringList fileNames;
//    if (fileDialog.exec()) {
//            fileNames = fileDialog.selectedFiles();
//    }

//    QString selectedFile;
//    for (int nIndex = 0; nIndex < fileNames.size(); nIndex++) {
//            selectedFile.append(fileNames.at(nIndex).toLocal8Bit().constData());
//    }

//    QTextCodec *textCodec = QTextCodec::codecForName("eucKR");
//    setWindowTitle( textCodec->toUnicode(selectedFile.toAscii()) );


}

/**
 * @brief customplot itemClik Slot
 * @param item
 * @param event
 */
void DBDataAnalToolDlg::itemClick(QCPAbstractItem *item, QMouseEvent *event)
{
    //item->userData;
    QMessageBox::information(this,"dd","dd");
}

/**
 * @brief customplot plottableClick Slot
 * @param plottable
 * @param dataIndex
 * @param event
 */
void DBDataAnalToolDlg::plottableClick(QCPAbstractPlottable *plottable, int dataIndex, QMouseEvent *event)
{
    QMessageBox::information(this,"plottableClick","plottableClick");
}

/**
 * @brief customplot 마우스 Move  이벤트
 * @param event
 */
void DBDataAnalToolDlg::mouseMove(QMouseEvent *event)
{
    //QMessageBox::information(this,"mouseMove","mouseMove");
}


