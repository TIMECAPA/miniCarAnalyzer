/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

//! [0]
#include <QtWidgets>
#ifndef QT_NO_PRINTDIALOG
#include <QtPrintSupport>
#endif

#include "mainwindow.h"
#include <QImage>

#include <QJsonValue>
#include <QJsonObject>
#include <QJsonDocument>

#include "qcustomplot.h"
#include "dialog.h"
#include "displaydbsensor.h"
#include "controlsensordlg.h"
#include "dbdataanaltooldlg.h"
#include "logindlg.h"
#include "playmqtt.h"
#include "raydbmngr.h"

/**
 * @brief 생성자
 */
MainWindow::MainWindow() : textEdit(new QTextEdit)
{


    QString strIniFile = QDir::currentPath();
    strIniFile += QDir::separator();
    strIniFile += "config.ini";
    //QSettings settings(strIniFile, QSettings::IniFormat);

    QSettings settings("config.ini", QSettings::IniFormat);
    settings.beginGroup("APP");

    const QStringList childKeys = settings.childKeys();
    m_strVerName = "sensorPFclient-R";

    foreach (const QString &childKey, childKeys){
        if(childKey.compare("VERSION")==0){
            m_strVerName = settings.value(childKey).toString();
            break;
        }
        qDebug() << settings.value(childKey) << endl;
    }
    settings.endGroup();

    setWindowTitle(m_strVerName);

    m_pDialog = new Dialog();
    m_pControlDlg = new ControlSensorDlg();
    //setCentralWidget(m_pStartPageDock);
    createActions();
    createStatusBar();
    createDockWindows();

    // DockWidget
    m_pMainRealtimePlotDock    = new MainRealtimePlotDock(this);
    m_pMainRealtimePlotDock->setWindowFlags(Qt::WindowFlags(0));
    m_pMainRealtimePlotDock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    addDockWidget(Qt::RightDockWidgetArea, m_pMainRealtimePlotDock);
    //m_pMainRealtimePlotDock->setFixedSize(900,600);
    //m_pMainRealtimePlotDock->resize(900,500);
    //QLayout* layout = this->layout();
    //centralWidget()->setFixedSize(100,100);
    //socket
    sockConn();

// DB Manager dll TEST
//    RayDBMngr *m_pRayDBMngr = new RayDBMngr();
//    m_pRayDBMngr->init();
//    m_pRayDBMngr->open();

//    QSqlQuery query;
//    query.exec("select * from cnt");
    setUnifiedTitleAndToolBarOnMac(true);
}

/**
 * @brief 소멸자
 */
MainWindow::~MainWindow()
{

}

/**
 * @brief 분석 툴 다이얼로그 보기
 */
void MainWindow::showAnalToolDlg()
{
    DBDataAnalToolDlg* dbdataAnalToolDlg = new DBDataAnalToolDlg(this);
    dbdataAnalToolDlg->show();
}

/**
 * @brief 로그인 다이얼로그 보기
 */
void MainWindow::showLoginDlg()
{
    LoginDlg* loginDlg = new LoginDlg(this);
    loginDlg->show();
}

/**
 * @brief MQTT Widget 보기
 */
void MainWindow::showPlayMqttWidget()
{
    PlayMQTT* mqttWidget = new PlayMQTT(this);
    mqttWidget->show();
}

/**
 * @brief 테스트 다이얼로그 보기
 */
void MainWindow::TestDialog()
{
    m_pDialog->show();    
}

/**
 * @brief 센서 상태 및 제어 다이얼로그 보기
 */
void MainWindow::ControlSensor()
{
    m_pControlDlg->show();
}


/**
 * @brief 실시간 Plot 그래프 추가/삭제 Slot
 * @param item
 * @param column
 */
void MainWindow::slotUpdatePlot(QTreeWidgetItem *item, int column)
{
    qDebug() << "slotUpdatePlot";
    if(item->parent()){//Only Child
        Qt::CheckState checkState = item->checkState(1); // checkState(1) 바꾸고
        QString strRI = "/" + item->parent()->parent()->parent()->text(column - 1) + "/" + item->parent()->parent()->text(column - 1) + "/" + item->parent()->text(column - 1 );
        QString strGraphName = "/" + item->parent()->parent()->parent()->text(column - 1) + "/" + item->parent()->parent()->text(column - 1) + "/" + item->parent()->text(column - 1 ) + "/" + item->text(column);

        if(strGraphName.contains("data_ch1_raw") == true){ // raw 데이터일 경우 가속도x, y, z 처리
            if(checkState  != Qt::Unchecked){
               m_pMainRealtimePlotDock->addGraphToPlot(strGraphName + "X");
               m_pMainRealtimePlotDock->addGraphToPlot(strGraphName + "Y");
               m_pMainRealtimePlotDock->addGraphToPlot(strGraphName + "Z");
            }else{
               m_pMainRealtimePlotDock->removeGraphToPlot(strGraphName + "X");
               m_pMainRealtimePlotDock->removeGraphToPlot(strGraphName + "Y");
               m_pMainRealtimePlotDock->removeGraphToPlot(strGraphName + "Z");
            }
            manageGraphToPlot(strRI,strGraphName, checkState);
        }
        else{
            if(checkState  != Qt::Unchecked){
               m_pMainRealtimePlotDock->addGraphToPlot(strGraphName);
            }else{
               m_pMainRealtimePlotDock->removeGraphToPlot(strGraphName);
            }
            manageGraphToPlot(strRI,strGraphName, checkState);
        }

    }
}

/**
 * @brief customPlot 그래프 관리
 * @param strRI
 * @param strGraphName
 * @param checkState
 */
void MainWindow::manageGraphToPlot(QString strRI, QString strGraphName, Qt::CheckState checkState)
{
    RealtimePlotDock* pRealtimePlotDock = qobject_cast<RealtimePlotDock*>(realtimePlotDock(strRI));
    if(pRealtimePlotDock){
        if(strGraphName.contains("data_ch1_raw") == true){ // raw 데이터일 경우 가속도x, y, z 처리
            if(checkState  != Qt::Unchecked){
                pRealtimePlotDock->addGraphToPlot(strGraphName + "X");
                pRealtimePlotDock->addGraphToPlot(strGraphName + "Y");
                pRealtimePlotDock->addGraphToPlot(strGraphName + "Z");
             }else{
                pRealtimePlotDock->removeGraphToPlot(strGraphName + "X");
                pRealtimePlotDock->removeGraphToPlot(strGraphName + "Y");
                pRealtimePlotDock->removeGraphToPlot(strGraphName + "Z");
            }

        }else{
            if(checkState  != Qt::Unchecked){ // p2p, rms
               pRealtimePlotDock->addGraphToPlot(strGraphName);
            }else{
               pRealtimePlotDock->removeGraphToPlot(strGraphName);
            }
        }
    }else{
//            QMessageBox::information(this,"매칭 정보","그래프와 연동되어 있지 않습니다.");
//            dispDBsensor->setTreeWidgetItemChangedConnect(false);
//            if(checkState  != Qt::Unchecked){
//               dispDBsensor->setTreeWidgetItemChangedConnect(false);
//               item->setCheckState(1,Qt::Unchecked);
//            }else{
//               item->setCheckState(1,Qt::Checked);
//            }
//            dispDBsensor->setTreeWidgetItemChangedConnect(true);
    }
}

/**
 * @brief 소켓 연결
 */
void MainWindow::sockConn()
{
    socket = new QTcpSocket(this);


    m_pMqttClient = new QMQTT::Client();

/*
    connect(socket,SIGNAL(connected()), this, SLOT(connected()));
    connect(socket,SIGNAL(disconnected()), this, SLOT(disconnected()));
    connect(socket,SIGNAL(readyRead()), this, SLOT(readyRead()));
    connect(socket,SIGNAL(bytesWritten(qint64)),this,SLOT(bytesWritten(qint64)));

    // real chart
    connect(socket,SIGNAL(readyRead()), this, SLOT(realtimeDataSlot()));
*/

    // mqtt signal & slot
    connect(m_pMqttClient, SIGNAL(connected()), this, SLOT(MqttConnSlot()));
    connect(m_pMqttClient, SIGNAL(disconnected()), this, SLOT(MqttDisconnSlot()));
    connect(m_pMqttClient, SIGNAL(received(const QMQTT::Message)),  this, SLOT(MqttReceivedSlot(const QMQTT::Message)));
    connect(m_pMqttClient, SIGNAL(error(QMQTT::ClientError)), this, SLOT(MqttErrorSlot()));

    // mqtt connect
    // m_pMqttClient->setHost(QHostAddress("118.45.113.158")); // IP로 접속
    // m_pMqttClient->setHostName("raynor2.codns.com"); // 도메인으로 접속
    m_pMqttClient->setHostName("garigulls.com"); // 도메인으로 접속
    m_pMqttClient->setPort(1883);
    m_pMqttClient->setClientId(m_strVerName);
    m_pMqttClient->connectToHost();

    m_pMqttClient->setAutoReconnect(true);
    m_pMqttClient->setKeepAlive(30);



/*
    if(!socket->waitForConnected(5000))
    {
       qDebug() << "Error: " <<  socket->errorString();
    }
*/


}

/**
 * @brief MQTT 에러 Slot
 */
void MainWindow::MqttErrorSlot()
{
    qDebug() << "mqtt connect error"   ;
}

/**
 * @brief MQTT 연결 Slot
 */
void MainWindow::MqttConnSlot()
{
    qDebug() << "\nqmqtt connected";
}

/**
 * @brief MQTT 연결 끊기 Slot
 */
void MainWindow::MqttDisconnSlot()
{
    if(m_pMqttClient->isConnectedToHost())
    {
        m_pMqttClient->disconnectFromHost();
        qDebug() << "\nmqtt disconnected";
    }
}


/**
 * @brief MQTT 메세지 수신 Slot
 * @param message
 */
void MainWindow::MqttReceivedSlot(const QMQTT::Message message)
{
    // QMessageBox::information(this, "temp", "strData");
    //m_widgetMainRealtimePlot->receivedMqttData(message);
    qDebug() << "Topic : " << message.topic() << "Data" << message.payload() << endl;


    // mqtt get DATA
    QString strTopicName = message.topic();
    QString strData = message.payload();

    QStringList  strListTopic = strTopicName.split("/");
    int nSize = strListTopic.size();
    if(nSize>4){
        QString      strTopicType = strListTopic.at(4);
        QString      strRI = strListTopic.at(0) + "/" + strListTopic.at(1) + "/" + strListTopic.at(2) + "/" + strListTopic.at(3);
        RealtimePlotDock* pDock = qobject_cast<RealtimePlotDock*>(realtimePlotMap.value(strRI));

        if(pDock){
            pDock->processMQTTMsg(strTopicName,strData,strTopicType);
        }
        m_pMainRealtimePlotDock->processMQTTMsg(strTopicName,strData,strTopicType);
    }
}

/**
 * @brief 소켓 연결됨
 */
void MainWindow::connected()
{
    qDebug() << "socket Connected!";

/*
    // #1

    socket->write("{\"ctname\":\"cnt-led\",\"con\":\"hello\"}");

    socket->write("{\"ctname\":\"control_soc01_ch1\",\"con\":\"hello\"}");

    socket->write("{\"ctname\":\"smartcs_soc01_ch1_p2p\",\"con\":\"hello\"}");
    socket->write("{\"ctname\":\"smartcs_soc01_ch1_rms\",\"con\":\"hello\"}");
    socket->write("{\"ctname\":\"event_soc01_ch1_p2p\",\"con\":\"hello\"}");
*/
}

/**
 * @brief 소켓 연결 안됨
 */
void MainWindow::disconnected()
{
    qDebug() << "socket Disconnected!";
}


/**
 * @brief 소켓 데이터 읽기
 */
void MainWindow::readyRead()
{
/*
    QDate currDay = QDate::currentDate();
    QTime currTime = QTime::currentTime();

    QString strDay = currDay.toString() + " - ";
    QString strNow = currTime.toString() + ": ";

    qDebug() << "Reading...";
    QString strReply = (QString)socket->readAll();

    qDebug() << "Response: " << strReply;

    metaInfoList->addItem(strDay + strNow + strReply);

    QJsonDocument jsonResponse = QJsonDocument::fromJson(strReply.toUtf8());
    QJsonObject jsonObj = jsonResponse.object();

    qDebug() << "con: " << jsonObj["con"].toString();

    QString strName = jsonObj["ctname"].toString();
    QString strData = jsonObj["con"].toString();

    QString temp;


    ///////////////////////// soc #1 ////////////////////////////
    if( strName.startsWith("/mobius-yt/bridgea/mobius-yt/bridgea/smartcs_soc01_ch1_p2p") )
    {
        temp = "P-P : ";
        g_dblReceivePeak[1] = strData.toDouble();
        //timecapaList->addItem(strNow + temp + strData);
        qDebug() << "item02 (Peak)";
    }

    if( strName.startsWith("/mobius-yt/bridgea/smartcs_soc01_ch1_rms") )
    {
        temp = "RMS : ";
        g_dblReceiveRMS[1] = strData.toDouble();
        //timecapaList->addItem(strNow + temp + strData);
        qDebug() << "item03 (RMS)";
    }

    if( strName.startsWith("event_soc01_ch1_p2p") )
    {
        temp = "P-P EVENT!!! : ";
        g_dblEventPeak[1] = strData.toDouble();
        //timecapaList->addItem(strNow + temp + strData);
        QMessageBox::information(this, temp, strData);
        qDebug() << "P2P (P-P EVENT)";
    }

    if( strName.startsWith("event_soc01_ch1_rms") )
    {
        temp = "RMS EVENT!!! : ";
        g_dblEventRMS[1] = strData.toDouble();
        //timecapaList->addItem(strNow + temp + strData);
        QMessageBox::information(this, temp, strData);
        qDebug() << "RMS (RMS EVENT)";
    }
    //////////////////////////////////////////////////////////
*/
}

/**
 * @brief 프린트
 */
void MainWindow::print()
{
#ifndef QT_NO_PRINTDIALOG
    QTextDocument *document = textEdit->document();
    QPrinter printer;

    QPrintDialog dlg(&printer, this);
    if (dlg.exec() != QDialog::Accepted) {
        return;
    }

    document->print(&printer);
    statusBar()->showMessage(tr("Ready"), 2000);
#endif
}

/**
 * @brief 저장하기
 */
void MainWindow::save()
{
    QMimeDatabase mimeDatabase;
    QString fileName = QFileDialog::getSaveFileName(this,
                        tr("Choose a file name"), ".",
                        mimeDatabase.mimeTypeForName("text/html").filterString());
    if (fileName.isEmpty())
        return;
    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text)) {
        QMessageBox::warning(this, tr("Dock Widgets"),
                             tr("Cannot write file %1:\n%2.")
                             .arg(QDir::toNativeSeparators(fileName), file.errorString()));
        return;
    }

    QTextStream out(&file);
    QApplication::setOverrideCursor(Qt::WaitCursor);
    out << textEdit->toHtml();
    QApplication::restoreOverrideCursor();

    statusBar()->showMessage(tr("Saved '%1'").arg(fileName), 2000);
}

/**
 * @brief 이전 작업으로
 */
void MainWindow::undo()
{
    QTextDocument *document = textEdit->document();
    document->undo();
}


/**
 * @brief 프로그램 관하여
 */
void MainWindow::about()
{
   QMessageBox::about(this, tr("About miniCARclient"),
            tr("The <b>miniCARclient</b> application  "
               "."));
}

/**
 * @brief 메뉴 및 툴바 생성
 */
void MainWindow::createActions()
{
    QMenu *fileMenu = menuBar()->addMenu(tr("&File"));
    QToolBar *fileToolBar = addToolBar(tr("File"));

    const QIcon newIcon = QIcon::fromTheme("document-new", QIcon(":/images/new.png"));
    QAction *newLetterAct = new QAction(newIcon, tr("&New Letter"), this);
    newLetterAct->setShortcuts(QKeySequence::New);
    newLetterAct->setStatusTip(tr("Create a new form letter"));

    // connect(newLetterAct, &QAction::triggered, this, &MainWindow::newLetter);

    fileMenu->addAction(newLetterAct);
    fileToolBar->addAction(newLetterAct);

    const QIcon saveIcon = QIcon::fromTheme("document-save", QIcon(":/images/save.png"));
    QAction *saveAct = new QAction(saveIcon, tr("&Save..."), this);
    saveAct->setShortcuts(QKeySequence::Save);
    saveAct->setStatusTip(tr("Save the current form letter"));
    connect(saveAct, &QAction::triggered, this, &MainWindow::save);
    fileMenu->addAction(saveAct);
    fileToolBar->addAction(saveAct);

    const QIcon printIcon = QIcon::fromTheme("document-print", QIcon(":/images/print.png"));
    QAction *printAct = new QAction(printIcon, tr("&Print..."), this);
    printAct->setShortcuts(QKeySequence::Print);
    printAct->setStatusTip(tr("Print the current form letter"));
    connect(printAct, &QAction::triggered, this, &MainWindow::print);
    fileMenu->addAction(printAct);
    fileToolBar->addAction(printAct);

    fileMenu->addSeparator();

    QAction *quitAct = fileMenu->addAction(tr("&Quit"), this, &QWidget::close);
    quitAct->setShortcuts(QKeySequence::Quit);
    quitAct->setStatusTip(tr("Quit the application"));

    QMenu *editMenu = menuBar()->addMenu(tr("&Edit"));
    QToolBar *editToolBar = addToolBar(tr("Edit"));
    const QIcon undoIcon = QIcon::fromTheme("edit-undo", QIcon(":/images/undo.png"));
    QAction *undoAct = new QAction(undoIcon, tr("&Undo"), this);
    undoAct->setShortcuts(QKeySequence::Undo);
    undoAct->setStatusTip(tr("Undo the last editing action"));
    connect(undoAct, &QAction::triggered, this, &MainWindow::undo);
    editMenu->addAction(undoAct);
    editToolBar->addAction(undoAct);

    //
    mainWindowMenu = menuBar()->addMenu(tr("Main window"));

    QAction *action = mainWindowMenu->addAction(tr("Animated docks"));
    action->setCheckable(true);
    action->setChecked(dockOptions() & AnimatedDocks);
    connect(action, &QAction::toggled, this, &MainWindow::setDockOptions);

    action = mainWindowMenu->addAction(tr("Allow nested docks"));
    action->setCheckable(true);
    action->setChecked(dockOptions() & AllowNestedDocks);
    connect(action, &QAction::toggled, this, &MainWindow::setDockOptions);

    action = mainWindowMenu->addAction(tr("Allow tabbed docks"));
    action->setCheckable(true);
    action->setChecked(dockOptions() & AllowTabbedDocks);
    connect(action, &QAction::toggled, this, &MainWindow::setDockOptions);

    action = mainWindowMenu->addAction(tr("Force tabbed docks"));
    action->setCheckable(true);
    action->setChecked(dockOptions() & ForceTabbedDocks);
    connect(action, &QAction::toggled, this, &MainWindow::setDockOptions);

    action = mainWindowMenu->addAction(tr("Vertical tabs"));
    action->setCheckable(true);
    action->setChecked(dockOptions() & VerticalTabs);
    connect(action, &QAction::toggled, this, &MainWindow::setDockOptions);

    action = mainWindowMenu->addAction(tr("Grouped dragging"));
    action->setCheckable(true);
    action->setChecked(dockOptions() & GroupedDragging);
    connect(action, &QAction::toggled, this, &MainWindow::setDockOptions);


    viewMenu = menuBar()->addMenu(tr("&View"));

    menuBar()->addSeparator();

    //////////////////////// 사용자 다이얼로그 ////////////////////////////////
    // add test menu
    QMenu *toolMenu = menuBar()->addMenu(tr("&SERVICE"));

    // mqtt
    // QAction *mqttAction = toolMenu->addAction(tr("&mqtt"), this, &MainWindow::showPlayMqttWidget);
    // mqttAction->setStatusTip(tr("mqtt"));

    // login
    QAction *loginAction = toolMenu->addAction(tr("&login"), this, &MainWindow::showLoginDlg);
    loginAction->setStatusTip(tr("login"));

    // 센서제어
    QAction *toolAct = toolMenu->addAction(tr("&remoteControl"), this, &MainWindow::TestDialog);
    toolAct->setStatusTip(tr("Test which controls the sensor"));

    // 센서상태
    QAction *controlAct = toolMenu->addAction(tr("&statusSensor"), this, &MainWindow::ControlSensor);
    controlAct->setStatusTip(tr("controls the sensor"));

    // 분석툴
    QAction *analToolAction = toolMenu->addAction(tr("&analysisChart"), this, &MainWindow::showAnalToolDlg);
    analToolAction->setStatusTip(tr("analysisChart"));


    /////////////////////////////////////////////////////////////////////////


    QMenu *helpMenu = menuBar()->addMenu(tr("&Help"));

    QAction *aboutAct = helpMenu->addAction(tr("&About"), this, &MainWindow::about);
    aboutAct->setStatusTip(tr("Show the application's About box"));

    // QAction *aboutQtAct = helpMenu->addAction(tr("About &Qt"), qApp, &QApplication::aboutQt);
    // aboutQtAct->setStatusTip(tr("Show the Qt library's About box"));
}

/**
 * @brief 상태 바 생성
 */
void MainWindow::createStatusBar()
{
    statusBar()->showMessage(tr("Ready"));
}

/**
 * @brief 도킹 창 생성
 */
void MainWindow::createDockWindows()
{

    dispDBsensor = new DisplayDBsensor(this);
    dispDBsensor->addData("SELECT * FROM cnt");
    dispDBsensor->setAllowedAreas(Qt::LeftDockWidgetArea|Qt::RightDockWidgetArea);
    //dispDBsensor->setWIndowTitle(tr("AAAAAAAAAAAAAA"));
    dispDBsensor->setSizePolicy(QSizePolicy::Policy::Minimum,QSizePolicy::Policy::Maximum);
    dispDBsensor->setMinimumHeight(600);
    dispDBsensor->setMaximumWidth(300);
//  dispDBsensor->setMaximumHeight(1080);

    addDockWidget(Qt::LeftDockWidgetArea, dispDBsensor);
    viewMenu->addAction(dispDBsensor->toggleViewAction());

/*
    QDockWidget *dock;
    dock = new QDockWidget(tr("monitoring container data"), this);
    metaInfoList = new QListWidget(dock);
    // metaInfoList->addItems(QStringList() << "Thank you for your payment which we have received today."
    dock->setWidget(metaInfoList);
    addDockWidget(Qt::BottomDockWidgetArea, dock);
    viewMenu->addAction(dock->toggleViewAction());
    // dock->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
    dock->setSizePolicy(QSizePolicy::Policy::Minimum,QSizePolicy::Policy::Maximum);
    dock->setMinimumHeight(70);
    dock->setMaximumHeight(120);

    connect(metaInfoList, &QListWidget::currentTextChanged,
            this, &MainWindow::addParagraph);
    connect(timecapaList, &QListWidget::currentTextChanged,
            this, &MainWindow::addParagraph);
*/

}

/**
 * 도킹 옵션 설정
 */
void MainWindow::setDockOptions()
{
    DockOptions opts;
    QList<QAction*> actions = mainWindowMenu->actions();

    if (actions.at(0)->isChecked())
        opts |= AnimatedDocks;
    if (actions.at(1)->isChecked())
        opts |= AllowNestedDocks;
    if (actions.at(2)->isChecked())
        opts |= AllowTabbedDocks;
    if (actions.at(3)->isChecked())
        opts |= ForceTabbedDocks;
    if (actions.at(4)->isChecked())
        opts |= VerticalTabs;
    if (actions.at(5)->isChecked())
        opts |= GroupedDragging;

    QMainWindow::setDockOptions(opts);
}

/**
 * @brief MQTT 인스턴스 가져오기
 * @return
 */
QMQTT::Client* MainWindow::getMQTTInstance()
{
    return m_pMqttClient;
}

/**
 * @brief 센서 목록 도킹 창 인스턴스 가져오기
 * @return
 */
DisplayDBsensor*   MainWindow::getDispSensorInstance()
{
    return dispDBsensor;
}

/**
 * @brief 실시간 Plot 도킹 창 관리
 * @param nIdx
 * @param strSensorName
 */
void MainWindow::manageRealtimeDock(const int nIdx, QString strSensorName)
{
    if(nIdx>-1){
//       m_widgetMainRealtimePlot->removeAllGraphToPlot(nIdx);
//       m_widgetMainRealtimePlot->setPlotMainTitle(nIdx,strSensorName);
        if(nIdx==0){
           createRealtimeDock(strSensorName);
        }else{//nIdx ==1
           destroyRealtimeDock(strSensorName);
        }
    }else{
       QMessageBox::information(this,"setMatchingSensor","센서 매칭 정보를 설정하지 못했습니다..");
    }

}

/**
 * @brief 도킹 이름과 일치한 실시간 Plot 가져오기
 * @param strDockName
 * @return
 */
QDockWidget* MainWindow::realtimePlotDock(QString strDockName)
{
    return realtimePlotMap.value(strDockName);
}

/**
 * @brief 실시간 Plot 도킹 창 생성
 * @param strRI
 */
void MainWindow::createRealtimeDock(QString strRI)
{
    if(!realtimePlotMap.contains(strRI)){
        RealtimePlotDock *dock = new RealtimePlotDock(this);
        dock->setAllowedAreas(Qt::RightDockWidgetArea);
        addDockWidget(Qt::RightDockWidgetArea, dock);
        //dock->setBaseSize(1920,1080);
        dock->setFloating(true);
        dock->setWindowTitle(strRI);
        realtimePlotMap.insert(strRI,dock);
    }else{
        RealtimePlotDock* pDock = qobject_cast<RealtimePlotDock*>(realtimePlotMap.value(strRI));
        pDock->show();
    }
}

/**
 * @brief 실시간 Plot 도킹 창 삭제
 * @param strRI
 */
void MainWindow::destroyRealtimeDock(QString strRI)
{
    delete realtimePlotMap.take(strRI);
}
