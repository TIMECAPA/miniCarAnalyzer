#include "displaydbsensor.h"
#include "ui_displaydbsensor.h"

#include <QTreeWidgetItem>
#include <QDebug>
#include <QStringList>

#include <QtSql>
#include <QMenu>
#include <QDebug>
#include <QMessageBox>

#include "mainwindow.h"

/**
 * @brief 생성자
 * @param parent
 */
DisplayDBsensor::DisplayDBsensor(QWidget *parent) :
    QDockWidget(parent),
    ui(new Ui::DisplayDBsensor)
{
    ui->setupUi(this);

    // 우버튼 단축메뉴
    ui->treeWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->treeWidget, SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(slotShowContextMenu(QPoint)));

    //connect(ui->treeWidget, SIGNAL(itemChanged(QTreeWidgetItem*,int)), this, SLOT(slotTreeWidgetItemChanged(QTreeWidgetItem*, int)));
    setTreeWidgetItemChangedConnect(true);
    connect(this, SIGNAL(signalUpdatePlot(QTreeWidgetItem *, int )), parent, SLOT(slotUpdatePlot(QTreeWidgetItem *, int)));


//    connect(ui->treeWidget, SIGNAL(itemDoubleClicked(QTreeWidgetItem*,int)),
//            this, SLOT(slotSensorControl()));
    m_treeItemOnContext = NULL;
}

/**
 * @brief 소멸자
 */
DisplayDBsensor::~DisplayDBsensor()
{
    delete ui;
}

/**
 * @brief TreeWiget 클리어
 */
void DisplayDBsensor::treeWidgetClear()
{
    ui->treeWidget->clear();
}

/**
 * @brief 센서 목록 가져오기
 * @param strQuery
 */
void DisplayDBsensor::addData(QString strQuery)
{
    ui->treeWidget->clear();


    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName("garigulls.com");
    db.setDatabaseName("mobiusdb3");
    db.setUserName("root");
    db.setPassword("soft0987");

    if (!db.open())
    {
        QMessageBox::critical(0, QObject::tr("Database Error"),
               db.lastError().text());
    }

    // QSqlQuery query("SELECT * FROM cnt where ri like '%smartcs%'");
    QSqlQuery query;
    query.exec(strQuery);


    QString cntName;
    QStringList listCheckContainer;


    int index=0;
    while(query.next())
    {
        cntName = query.value(0).toString();
        listCheckContainer = cntName.split("/");

        if(listCheckContainer.size() != 5) // 총 5개로 구분된 컨테이너만 정상적인 컨테이너로 저장함
        {
            qDebug() << "제외된 컨테이너의 묶음갯수 = " << listCheckContainer.count() << cntName;
            continue;
        }
        else
        {
            containerName[index] = cntName;
            index++;
        }
    }

    numIndex = index;

    ui->treeWidget->setColumnCount(3);
    ui->treeWidget->setHeaderLabels(QStringList() << "No. miniCAR" << "dataTYPE" << "chk");
    ui->treeWidget->header()->resizeSection(0, 140);
    ui->treeWidget->header()->resizeSection(1, 120);

    QMap<QString, QTreeWidgetItem*> mapAE;
    QMap<QString, QTreeWidgetItem*> mapTYPE;

    QStringList List;

    QString cseNAME = ""; // cse
    QString aeNAME = ""; // ae
    QString sensorNAME = "";
    QString typeNAME = ""; // sensor type

    QString strContainerID;

/*
    const int numIndex = 9;
    QString containerName[numIndex] = {
        "/mobius-yt/bridgea/soc01/item01", "/mobius-yt/bridgeb/soc01/item02", "/mobius-yt/bridgea/soc04/item01" ,
        "/mobius-yt/bridgea/soc02/item01", "/mobius-yt/bridgeb/soc01/item02", "/mobius-yt/bridgea/soc03/item01" ,
        "/mobius-yt/bridgea/soc03/item02", "/mobius-yt/bridgec/soc05/item04", "/mobius-yt/bridgea/soc05/item05"
    };
*/

    for(int i=0; i < numIndex; i++)
    {
        List = containerName[i].split("/");


//        if(List.size() != 5) // 총 5개로 구분된 컨테이너만 트리구조로 처리함
//        {
//            qDebug() << "갯수 = " << List.count();
//            continue;
//        }


        //////////////////////////////////// CSE  ///////////////////////////////////
        strContainerID = List.at(1); //
        cseNAME = List.at(1);
        if(mapAE.contains(strContainerID))
        {
            QTreeWidgetItem *it = mapAE.value(strContainerID);
            it->setText(0, cseNAME);
            it->setText(1, "");
            it->setText(2, "");

            it->setExpanded(true);
        }
        else // 새로운 컨테이너
        {
            // aeNAME = strContainerID;
            QTreeWidgetItem *item =new QTreeWidgetItem(ui->treeWidget);
            mapAE.insert(strContainerID, item);
            item->setText(0, cseNAME);
            item->setText(1, "");
            item->setText(2, "");

            item->setExpanded(true);
        }


        /////////////////////////////  CSE child AE  ////////////////////////////////////

        strContainerID = List.at(1) + List.at(2);
        aeNAME = List.at(2);
        if(mapTYPE.contains(strContainerID)) //
        {
            QTreeWidgetItem *it = mapTYPE.value(strContainerID);
            it->setText(0, aeNAME); //
            it->setText(1, "");
            it->setText(2, "");

            it->setExpanded(true);
            mapAE.value(List.at(1))->addChild(it);
        }
        else
        {
            // typeNAME = strContainerID;
            QTreeWidgetItem *item =new QTreeWidgetItem();
            mapTYPE.insert(strContainerID, item);
            item->setText(0, aeNAME); //
            item->setText(1, "");
            item->setText(2, "");

            item->setExpanded(true);
            mapAE.value(List.at(1))->addChild(item);
        }


        /////////////////////////////  sensor  ////////////////////////////////////
        strContainerID = List.at(1) + List.at(2) + List.at(3); //
        sensorNAME = List.at(3);
        if(mapTYPE.contains(strContainerID)) //
        {
            QTreeWidgetItem *it = mapTYPE.value(strContainerID);
            it->setText(0, sensorNAME); //
            it->setText(1, "");
            it->setText(2, "");
            it->setExpanded(true);

            mapTYPE.value(List.at(1) + List.at(2))->addChild(it);
        }
        else
        {
            QTreeWidgetItem *item =new QTreeWidgetItem();
            mapTYPE.insert(strContainerID, item);
            item->setText(0, sensorNAME); //
            item->setText(1, "");
            item->setText(2, "");

            item->setExpanded(true);
            mapTYPE.value(List.at(1) + List.at(2))->addChild(item);
        }

        /////////////////////////////  SENSOR child Type  ////////////////////////////////////
        strContainerID = List.at(1) + List.at(2) + List.at(3) + List.at(4);
        typeNAME = List.at(4);
        if(mapTYPE.contains(strContainerID))
        {
            QTreeWidgetItem *it = mapTYPE.value(strContainerID);
            it->setText(0, ""); //
            it->setText(1, typeNAME);
            it->setText(2, "-");
            it->setFlags(it->flags() | Qt::ItemIsEditable);
            it->setFlags(it->flags() | Qt::ItemIsUserCheckable);
            it->setCheckState(1, Qt::Unchecked);

            mapTYPE.value(List.at(1) + List.at(2) + List.at(3))->addChild(it);
        }
        else
        {
            // typeNAME = strContainerID;
            QTreeWidgetItem *item =new QTreeWidgetItem();
            mapTYPE.insert(strContainerID, item);
            item->setText(0, "");
            item->setText(1, typeNAME);
            item->setText(2, "-");
            item->setFlags(item->flags() | Qt::ItemIsEditable);
            item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
            item->setCheckState(1, Qt::Unchecked);

            mapTYPE.value(List.at(1) + List.at(2) + List.at(3))->addChild(item);
        }


    }

    db.close();
}

/**
 * @brief 컨텍스트 메뉴 보이기 Slot
 * @param pos
 */
void DisplayDBsensor::slotShowContextMenu(QPoint pos)
{
    //QTreeWidgetItem* item =  ui->treeWidget->itemAt(pos);
    m_treeItemOnContext = ui->treeWidget->itemAt(pos);
    int nDepth = treeWidgetDepth(m_treeItemOnContext);
    int column = 1;
    QString strTemp = m_treeItemOnContext->text(0);

    if(nDepth==3){
        //if(item->parent()){//Only Child
        QSignalMapper *pSignalMapper = new QSignalMapper(this);
        QString strSensorName = "/" + m_treeItemOnContext->parent()->parent()->text(column - 1) + "/" + m_treeItemOnContext->parent()->text(column - 1) + "/" + m_treeItemOnContext->text(column - 1 );
        QMenu *menu=new QMenu(this);
        QAction* actionPlotCreation = menu->addAction(QString("실시간 그래프 생성"));
        QAction* actionPlotDeletion = menu->addAction(QString("실시간 그래프 삭제"));


        connect(actionPlotCreation,SIGNAL(triggered()),pSignalMapper,SLOT(map()));
        connect(actionPlotDeletion,SIGNAL(triggered()),pSignalMapper,SLOT(map()));


        pSignalMapper->setMapping(actionPlotCreation,"0;" +strSensorName );
        pSignalMapper->setMapping(actionPlotDeletion,"1;" +strSensorName);


        connect(pSignalMapper, SIGNAL(mapped(QString)),this, SLOT(slotSetPlot(QString)));


        menu->popup(ui->treeWidget->viewport()->mapToGlobal(pos));


        //}
    }

}

/**
 * @brief Plot 지정하기 Slot
 * @param strSelInfo
 */
void DisplayDBsensor::slotSetPlot(QString strSelInfo)
{
//    QTreeWidgetItemIterator it(treeWidget);
//    while (*it) {
//        if ((*it)->text(0) == "ITEM03")
//            (*it)->setSelected(true);
//         ++it;
//    }
    MainWindow* mainWindow = qobject_cast<MainWindow*>(this->parent());

    QStringList  strListInfo = strSelInfo.split(';');
    if(strListInfo.size()==2 && m_treeItemOnContext != NULL){
        //ui->treeWidget->model()->
        setTreeWidgetItemChangedConnect(false);

        //Dock Window 생성/삭제 후
        QString strIndex = strListInfo.at(0);
        int nIdx = strIndex.toInt();
        mainWindow->manageRealtimeDock(nIdx,strListInfo.at(1));

        Qt::CheckState state = Qt::Unchecked;
        QList<QTreeWidgetItem*> children = m_treeItemOnContext->takeChildren();
        foreach(QTreeWidgetItem* child, children)
        {
            state = child->checkState(1);
            child->setCheckState(1, state);
            m_treeItemOnContext->addChild(child);
            if(nIdx == 0){
                QString strRI = "/" + child->parent()->parent()->parent()->text(0) + "/" + child->parent()->parent()->text(0) + "/" + child->parent()->text(0);
                QString strGraphName = "/" + child->parent()->parent()->parent()->text(0) + "/" + child->parent()->parent()->text(0) + "/" + child->parent()->text(0) + "/" + child->text(1);
                mainWindow->manageGraphToPlot(strRI,strGraphName,state);
            }
        }

        setTreeWidgetItemChangedConnect(true);
        qDebug()<< "Plot Index  : " << strListInfo.at(0) << "Sensor Name  : " <<strListInfo.at(1) << endl;
    }else{
        QMessageBox::information(this,"PLOT INFO","정보가 잘못되었습니다.");
    }
}


/**
 * @brief TreeWighet 깊이
 * @param item
 * @return
 */
int DisplayDBsensor::treeWidgetDepth(QTreeWidgetItem* item)
{
    int nDepth = 0;
    //QTreeWidgetItem *item = ...;
    while(item!=0){
      nDepth++;
      item = item->parent();
    }
    return nDepth;
}

/**
 * @brief TreeWighet 아이템 변경 Connect 설정
 * @param bConnect
 */
void DisplayDBsensor::setTreeWidgetItemChangedConnect(bool bConnect)
{
    if(bConnect){
        connect(ui->treeWidget, SIGNAL(itemChanged(QTreeWidgetItem*,int)), this, SLOT(slotTreeWidgetItemChanged(QTreeWidgetItem*, int)));
    }else{
        disconnect(ui->treeWidget, SIGNAL(itemChanged(QTreeWidgetItem*,int)), this, SLOT(slotTreeWidgetItemChanged(QTreeWidgetItem*, int)));
    }

}

/**
 * @brief TreeWiget 아이템 변경 Slot
 * @param item
 * @param column
 */
void DisplayDBsensor::slotTreeWidgetItemChanged(QTreeWidgetItem *item, int column)
{
    // Qt::ItemFlags  flags = item->flags();

     //qDebug() << "treeWidgetItemChanged - " << column << item->checkState(column);
//    if(item->checkState(0)==Qt::Unchecked){
//       //QMessageBox::information(this, "UnChecked", "item1name");
//        emit updatePlot(0);
//    }else{
//       //QMessageBox::information(this, "Checked", "Checked");
//        emit updatePlot(2);
//    }
    emit signalUpdatePlot(item, column);
}

