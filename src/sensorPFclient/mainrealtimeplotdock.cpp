#include "mainrealtimeplotdock.h"
#include "ui_mainrealtimeplotdock.h"

#include "mainwindow.h"
#include <QtMath>

/**
 * @brief 생성자
 * @param parent
 */
MainRealtimePlotDock::MainRealtimePlotDock(QWidget *parent) :
    QDockWidget(parent),
    ui(new Ui::MainRealtimePlotDock)
{
    ui->setupUi(this);

    setupMainRealtimePlot(ui->customplot);

    m_dbYMax = -100.0f;
    m_dbYMin = 100.0f;

}

/**
 * @brief 소멸자
 */
MainRealtimePlotDock::~MainRealtimePlotDock()
{
    delete ui;
}

/**
 * @brief 실시간 Plot 설정
 * @param customPlot
 */
void MainRealtimePlotDock::setupMainRealtimePlot(QCustomPlot *customPlot)
{
#if QT_VERSION < QT_VERSION_CHECK(4, 7, 0)
  QMessageBox::critical(this, "", "You're using Qt < 4.7, the realtime data demo needs functions that are available with Qt 4.7 to work properly");
#endif
  // demoName = "Real Time Data Demo";

  customPlot->legend->setVisible(true);

  QSharedPointer<QCPAxisTickerDateTime> timeTicker(new QCPAxisTickerDateTime);
  customPlot->xAxis->setRange(-60*3.5, 60*11);
  //timeTicker->setTimeFormat("%m:%s");
  //timeTicker->setDateTimeFormat("day dd\nhh:mm:ss");
  timeTicker->setDateTimeFormat("hh:mm:ss");\
  //timeTicker->setTickStepStrategy(QCPAxisTicker::TickStepStrategy::tssReadability);
  timeTicker->setTickCount(10);
  customPlot->xAxis->setTicker(timeTicker);


  customPlot->axisRect()->setupFullAxesBox();

  // make left and bottom axes transfer their ranges to right and top axes:
  connect(customPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), customPlot->xAxis2, SLOT(setRange(QCPRange)));
  connect(customPlot->yAxis, SIGNAL(rangeChanged(QCPRange)), customPlot->yAxis2, SLOT(setRange(QCPRange)));

  connect(customPlot, SIGNAL(itemClick(QCPAbstractItem*, QMouseEvent*)),this, SLOT(slotPlotItemClick(QCPAbstractItem*, QMouseEvent*)));
  // setup a timer that repeatedly calls MainWindow::realtimeDataSlot:
  // 이벤트 및 리얼타임 의한 차트 그리기
  connect(&dataTimer, SIGNAL(timeout()), this, SLOT(slotUpdateMainRealtimePlot()));
  dataTimer.start(0); // Interval 0 means to refresh as fast as possible
}

/**
 * @brief 그래프 추가
 * @param strName
 */
void MainRealtimePlotDock::addGraphToPlot(QString strName)
{

    int nIndex = 0;
    int nGraphCount = ui->customplot->graphCount();
    bool isExisted=false;

    for(int nIndex=0; nIndex < nGraphCount; nIndex++){
        QString strGraphName = ui->customplot->graph(nIndex)->name();
        if(strGraphName.compare(strName)==0){
            isExisted = true;
            break;
        }
    }

   if(!isExisted && !strName.isEmpty() ){
       ui->customplot->addGraph();
       QColor color = QColor(rand() % 255 , rand() % 255 , rand() % 255);
       ui->customplot->graph(nGraphCount)->setPen(color);
       // customplot->graph(nGraphCount)->setLineStyle(QCPGraph::lsNone);
       ui->customplot->graph(nGraphCount)->setLineStyle(QCPGraph::lsLine);
       ui->customplot->graph(nGraphCount)->setScatterStyle(QCPScatterStyle::ssDisc);
       ui->customplot->graph(nGraphCount)->setName(strName);
       ui->customplot->replot();

       // mqtt
       MainWindow* mainWindow = qobject_cast<MainWindow*>(this->parent());
       mainWindow->getMQTTInstance()->subscribe(strName,0);

   }

}

/**
 * @brief 그래프 제거
 * @param strRemoveGraphName
 */
void MainRealtimePlotDock::removeGraphToPlot(QString strRemoveGraphName)
{
    int nGraphCount =   ui->customplot->graphCount();

    if(nGraphCount > 0){
      for(int i=0; i < nGraphCount; i++){
          QString strGraphName = ui->customplot->graph(i)->name();
          if(strGraphName.compare(strRemoveGraphName)==0){
              ui->customplot->removeGraph(i);
              MainWindow* mainWindow = qobject_cast<MainWindow*>(this->parent());
              mainWindow->getMQTTInstance()->unsubscribe(strRemoveGraphName);
              // qDebug() << "remove graphName : " << strRemoveGraphName << " Index number : " << i;
              break;
          }
      }
    }
    ui->customplot->replot();
}

/**
 * @brief 실시간 Plot 업데이트 Slot
 */
void MainRealtimePlotDock::slotUpdateMainRealtimePlot()
{
#if QT_VERSION < QT_VERSION_CHECK(4, 7, 0)
  double key = 0;
#else
  double key = QDateTime::currentDateTime().toMSecsSinceEpoch()/1000.0;
#endif
  // calculate two new data points:
  static double lastPointKey = 0;
  if (key-lastPointKey > 0.01){ // at most add point every 10 ms
    int nGraphCount = ui->customplot->graphCount();

    if(nGraphCount==0){
        m_dbYMax = -100.0f;
        m_dbYMin = 100.0f;
        ui->customplot->yAxis->setRange(m_dbYMin, m_dbYMax);
    }
    else{
        // double dbAvg = (qFabs(m_dbYMax) + qFabs(m_dbYMin))/2;
        // ui->customplot->yAxis->setRange(m_dbYMin-dbAvg, m_dbYMax+dbAvg);
        ui->customplot->yAxis->setRangeLower(m_dbYMin + (-0.05f));
        ui->customplot->yAxis->setRangeUpper(m_dbYMax + 0.05f);
    }

    lastPointKey = key;
  }
  // make key axis range scroll with the data (at a constant range size of 8):
  ui->customplot->xAxis->setRange(key+0.25, 8, Qt::AlignRight);
  ui->customplot->replot();
}

/**
 * @brief 실시간 그래프 그리기
 * @param strName
 * @param dblValue
 */
void MainRealtimePlotDock::realRenderingChart(QString strName, double dblValue)
{
  // calculate two new data points:
  #if QT_VERSION < QT_VERSION_CHECK(4, 7, 0)
    double key = 0;
  #else
    double key = QDateTime::currentDateTime().toMSecsSinceEpoch()/1000.0;
  #endif

    // 차트찾기
    int findChartIndex = -1;
    int nGraphCount = ui->customplot->graphCount();
    bool isExisted=false;

    QString strGraphName;
    for(int i=0; i < nGraphCount; i++){
        strGraphName = ui->customplot->graph(i)->name();
        if(strGraphName.compare(strName)==0){
            isExisted = true;
            findChartIndex = i;
            break;
        }
    }
    // 찾은 차트 그리기
    QSharedPointer<QCPGraphDataContainer> data;
    if( findChartIndex != -1 ){
        double dblData = dblValue;
        static double lastPointKey = 0;
        if (key-lastPointKey > 0.01) // at most add point every 10 ms
        {
            if(m_dbYMin > dblData){
                m_dbYMin = dblData;
            }
            if(m_dbYMax < dblData){
                m_dbYMax = dblData;
            }


            ui->customplot->graph(findChartIndex)->addData(key, dblData);
            ui->customplot->graph(findChartIndex)->data()->removeBefore(key-8);
            QString str;
            str.sprintf("%.3f", key);
            //QString("%1").arg(key)
            //qDebug() << "Key Name" << strName << "Key Value : " <<  str << "," << dblData << endl;
            qDebug() << "Min : " << m_dbYMin << "Max : " << m_dbYMax << endl;

        }
        data = ui->customplot->graph(findChartIndex)->data();
     }

}

/**
 * @brief MQTT 메세지 처리
 * @param strTopicName
 * @param strData
 * @param strTopicType
 */
void MainRealtimePlotDock::processMQTTMsg(QString strTopicName,QString strData,QString strTopicType)
{
    double dblValue = 0.0f;
    if(strTopicType.compare("data_ch1_raw") == 0) // raw
    {
        dblValue = strData.toDouble();
        realRenderingChart(strTopicName, dblValue);
    }

    if(strTopicType.compare("data_ch1_rawX") == 0) // raw
    {
        dblValue = strData.toDouble();
        realRenderingChart(strTopicName, dblValue);
    }

    if(strTopicType.compare("data_ch1_rawY") == 0) // raw
    {
        dblValue = strData.toDouble();
        realRenderingChart(strTopicName, dblValue);
    }

    if(strTopicType.compare("data_ch1_rawZ") == 0) // raw
    {
        dblValue = strData.toDouble();
        realRenderingChart(strTopicName, dblValue);
    }

    if(strTopicType.compare("data_ch1_p2p") == 0) // p2p
    {
        dblValue = strData.toDouble();
        ////timecapaList->addItem(strNow + strTopicName + strData);
        realRenderingChart(strTopicName, dblValue);
    }

    if(strTopicType.compare("data_ch1_rms") == 0) // rms
    {
        dblValue = strData.toDouble();
        realRenderingChart(strTopicName, dblValue);
    }

    if(strTopicType.compare("event_ch1_p2p") == 0) // rms
    {
        dblValue = strData.toDouble();
        realRenderingChart(strTopicName, dblValue);
    }


}

/**
 * @brief customplot itemClick  Slot
 * @param item
 * @param event
 */
void MainRealtimePlotDock::slotPlotItemClick(QCPAbstractItem *item, QMouseEvent *event)
{
    qDebug() << "***********************************************" << endl;
}
