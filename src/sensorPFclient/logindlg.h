#ifndef LOGINDLG_H
#define LOGINDLG_H

#include <QDialog>
#include <QtSql>

namespace Ui {
class LoginDlg;
}

/**
 * @brief 로그인 다이얼로그 클래스
 */
class LoginDlg : public QDialog
{
    Q_OBJECT
public:
    explicit LoginDlg(QWidget *parent = 0);
    ~LoginDlg();

private slots:
    void on_btnClear_clicked();
    void on_btnLogin_clicked();
private:
    Ui::LoginDlg *ui;

    /**
     * @brief 데이터베이스 인스턴스
     */
    QSqlDatabase db;
};

#endif // LOGINDLG_H
