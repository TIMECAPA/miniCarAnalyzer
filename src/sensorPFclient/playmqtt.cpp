#include "playmqtt.h"
#include "ui_playmqtt.h"
#include <qmqtt.h>
#include <QDebug>

/**
 * @brief 생성자
 * @param parent
 */
PlayMQTT::PlayMQTT(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PlayMQTT)
{
    ui->setupUi(this);

    client = new QMQTT::Client();

    connect(client, SIGNAL(connected()), this, SLOT(onMQTT_Connected()));
    connect(client, SIGNAL(diconnected()), this, SLOT(onMQTT_disconnected()));

    connect(client, SIGNAL(received(QMQTT::Message)), this, SLOT(onMQTT_Received(QMQTT::Message)));
}

/**
 * @brief 소멸자
 */
PlayMQTT::~PlayMQTT()
{
    delete ui;
}


/**
 * @brief MQTT 메세지 수신
 * @param message
 */
void PlayMQTT::onMQTT_Received(QMQTT::Message message)
{
    ui->logsText->append(message.topic());
    ui->logsText->append(message.payload());

//    qDebug() << message.topic();
//    qDebug() << message.payload();
}

/**
 * @brief MQTT 연결됨 Slot
 */
void PlayMQTT::onMQTT_Connected()
{
    qDebug() << "connect";
}

/**
 * @brief MQTT 연결 끊김 Slot
 */
void PlayMQTT::onMQTT_disconnected()
{
    qDebug() << "dis connect";
}

/**
 * @brief MQTT 연결 클릭 이벤트
 */
void PlayMQTT::on_pushButton_clicked()
{
    client->setHost(QHostAddress(ui->brokerLE->text()));
    client->setPort(ui->PortLE->text().toInt());

    client->setClientId("testMQTT-dialog");
    client->connectToHost();
}

/**
 * @brief MQTT 연결 끊음 클릭 이벤트
 */
void PlayMQTT::on_pushButton_2_clicked()
{
    client->disconnectFromHost();
}

/**
 * @brief MQTT  구독 클릭 이벤트
 */
void PlayMQTT::on_pushButton_4_clicked()
{
    client->subscribe(ui->Sub_topicLE_2->text(),0);
}

/**
 * @brief MQTT 구독 해지 클릭 이벤트
 */
void PlayMQTT::on_pushButton_5_clicked()
{
    client->unsubscribe(ui->Sub_topicLE_2->text());
}

/**
 * @brief 메세지 공표 클릭 이벤트
 */
void PlayMQTT::on_pushButton_3_clicked()
{
    QMQTT::Message msg(0, ui->Pub_topicLE->text(), ui->Pub_valueLE->text().toUtf8());
    client->publish(msg);
}
