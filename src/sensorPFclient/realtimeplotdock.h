#ifndef REALTIMEPLOTDOCK_H
#define REALTIMEPLOTDOCK_H

#include <QDockWidget>
#include <QTimer>

#include "qmqtt.h"

class QCustomPlot;

namespace Ui {
class RealtimePlotDock;
}

/**
 * @brief 실시간 Plot 도킹 창 클래스
 */
class RealtimePlotDock : public QDockWidget
{
    Q_OBJECT
public:
    explicit RealtimePlotDock(QWidget *parent = 0);
    ~RealtimePlotDock();
private:
    Ui::RealtimePlotDock *ui;

    /**
     * @brief 타이머
     */
    QTimer m_timerReplot;

    /**
     * @brief 최대 값
     */
    double         m_dbYMax;

    /**
     * @brief 최소 값
     */
    double         m_dbYMin;

private slots:
    void realtimeDataSlot(); //chart
private:
    void setupRealtimeDataDemo(QCustomPlot *customPlot);
    void realRenderingChart(QCustomPlot *customPlot,QString strName, double dblValue);
public:
    void addGraphToPlot(QString strName);
    void removeGraphToPlot(QString strRemoveGraphName);
    void replot(double dbStartPosX,double dbEndPosX,double dbStartPosY,double dbEndPosY);
    void removeAllGraphToPlot();
    void setPlotMainTitle(QString setPlotMainTitle);
    void processMQTTMsg(QString strTopicName,QString strData,QString strTopicType);
};

#endif // REALTIMEPLOTDOCK_H
