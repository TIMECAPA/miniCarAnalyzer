#ifndef PLAYMQTT_H
#define PLAYMQTT_H

#include <QWidget>
#include <qmqtt.h>

namespace Ui {
class PlayMQTT;
}

/**
 * @brief MQTT Wrapper Widget
 */
class PlayMQTT : public QWidget
{
    Q_OBJECT

public:
    explicit PlayMQTT(QWidget *parent = 0);
    ~PlayMQTT();
private slots:
    void onMQTT_Connected();
    void onMQTT_disconnected();
    void onMQTT_Received(QMQTT::Message message);
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();
    void on_pushButton_4_clicked();
    void on_pushButton_5_clicked();
    void on_pushButton_3_clicked();
private:
    Ui::PlayMQTT *ui;

    /**
     * @brief MQTT 클라이언트
     */
    QMQTT::Client* client;

};

#endif // PLAYMQTT_H
