#include "realtimeplotdock.h"
#include "ui_realtimeplotdock.h"

#include "qcustomplot.h"
#include <QMessageBox>
#include "mainwindow.h"
#include "displaydbsensor.h"



/**
 * @brief 생성자
 * @param parent
 */
RealtimePlotDock::RealtimePlotDock(QWidget *parent) :
    QDockWidget(parent),
    ui(new Ui::RealtimePlotDock)
{
    ui->setupUi(this);

    m_dbYMax = -100.0f;
    m_dbYMin = 100.0f;

    // 이벤트 및 리얼타임 의한 차트 그리기
    connect(&m_timerReplot, SIGNAL(timeout()), this, SLOT(realtimeDataSlot()));
    m_timerReplot.start(0); // Interval 0 means to refresh as fast as possible

    setupRealtimeDataDemo(ui->customplot);
}

/**
 * @brief 소멸자
 */
RealtimePlotDock::~RealtimePlotDock()
{
    delete ui;
}

/**
 * @brief 실시간 데이타 데모 설정
 * @param customPlot
 */
void RealtimePlotDock::setupRealtimeDataDemo(QCustomPlot *customPlot)
{
#if QT_VERSION < QT_VERSION_CHECK(4, 7, 0)
  QMessageBox::critical(this, "", "You're using Qt < 4.7, the realtime data demo needs functions that are available with Qt 4.7 to work properly");
#endif
  // demoName = "Real Time Data Demo";

  customPlot->legend->setVisible(true);

  QSharedPointer<QCPAxisTickerDateTime> timeTicker(new QCPAxisTickerDateTime);
  customPlot->xAxis->setRange(-60*3.5, 60*11);
  //timeTicker->setTimeFormat("%m:%s");
  //timeTicker->setDateTimeFormat("day dd\nhh:mm:ss");
  timeTicker->setDateTimeFormat("hh:mm:ss");\
  //timeTicker->setTickStepStrategy(QCPAxisTicker::TickStepStrategy::tssReadability);
  timeTicker->setTickCount(10);
  customPlot->xAxis->setTicker(timeTicker);


  customPlot->axisRect()->setupFullAxesBox();

  // make left and bottom axes transfer their ranges to right and top axes:
  connect(customPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), customPlot->xAxis2, SLOT(setRange(QCPRange)));
  connect(customPlot->yAxis, SIGNAL(rangeChanged(QCPRange)), customPlot->yAxis2, SLOT(setRange(QCPRange)));

  // add the text label at the top:
//  QCPItemText *textLabel = new QCPItemText(customPlot);
//  textLabel->setPositionAlignment(Qt::AlignTop|Qt::AlignHCenter);
//  textLabel->position->setType(QCPItemPosition::ptAxisRectRatio);
//  textLabel->position->setCoords(0.5, 0); // place position at center/top of axis rect
//  textLabel->setText("Plot1");
//  textLabel->setFont(QFont(font().family(), 16)); // make font a bit larger
//  textLabel->setPen(QPen(Qt::black)); // show black border around text

}

/**
 * @brief 그래프 추가
 * @param strName
 */
void RealtimePlotDock::addGraphToPlot(QString strName)
{

    int nGraphCount = ui->customplot->graphCount();
    bool isExisted=false;

    for(int nIndex=0; nIndex < nGraphCount; nIndex++){
        QString strGraphName = ui->customplot->graph(nIndex)->name();
        if(strGraphName.compare(strName)==0){
            isExisted = true;
            break;
        }
    }

   if(!isExisted && !strName.isEmpty() ){
       //qvariant_cast
       MainWindow* mainWindow = qobject_cast<MainWindow*>(this->parent());
       ui->customplot->addGraph();
       //ui->customplot->graph(nGraphCount)->setPen(QPen(Qt::blue));
       QColor color = QColor(rand() % 255 , rand() % 255 , rand() % 255);
       ui->customplot->graph(nGraphCount)->setPen(QPen(color));

       // customPlot->graph(nGraphCount)->setLineStyle(QCPGraph::lsNone);
       ui->customplot->graph(nGraphCount)->setLineStyle(QCPGraph::lsLine);
       ui->customplot->graph(nGraphCount)->setScatterStyle(QCPScatterStyle::ssDisc);
       ui->customplot->graph(nGraphCount)->setName(strName);
       ui->customplot->replot();

       // mqtt
       mainWindow->getMQTTInstance()->subscribe(strName,0);

       // qDebug() << "add graphName  : " << strName << " Index number : " << nIndex;

   }

}

/**
 * @brief 그래프 삭제
 * @param strRemoveGraphName
 */
void RealtimePlotDock::removeGraphToPlot(QString strRemoveGraphName)
{
    MainWindow* mainWindow = qobject_cast<MainWindow*>(this->parent());

    int nGraphCount =   ui->customplot->graphCount();

    if(nGraphCount > 0){
      for(int i=0; i < nGraphCount; i++){
          QString strGraphName = ui->customplot->graph(i)->name();
          if(strGraphName.compare(strRemoveGraphName)==0){
              ui->customplot->removeGraph(i);
              //mqtt
              mainWindow->getMQTTInstance()->unsubscribe(strRemoveGraphName);
              // qDebug() << "remove graphName : " << strRemoveGraphName << " Index number : " << i;
              break;
          }
      }
    }
    ui->customplot->replot();
}

/**
 * @brief Replot
 * @param dbStartPosX
 * @param dbEndPosX
 * @param dbStartPosY
 * @param dbEndPosY
 */
void RealtimePlotDock::replot(double dbStartPosX,double dbEndPosX,double dbStartPosY,double dbEndPosY)
{

    ui->customplot->xAxis->setRange(dbStartPosX, dbEndPosX, Qt::AlignRight);
    ui->customplot->yAxis->setRange(dbStartPosY,dbEndPosY);
    ui->customplot->replot();

}

/**
 * @brief 그래프 모두 제거
 */
void RealtimePlotDock::removeAllGraphToPlot()
{
    ui->customplot->clearGraphs();
}

/**
 * @brief customplot 메인 타이틀 설정
 * @param strPlotMainTitle
 */
void RealtimePlotDock::setPlotMainTitle(QString strPlotMainTitle)
{
    QCustomPlot* customPlot = ui->customplot;
    if(customPlot){
        //int nCount = customPlot->plotLayout()->elementCount();
        if(customPlot->plotLayout()->elementCount() == 1){
            customPlot->plotLayout()->insertRow(0);
            customPlot->plotLayout()->addElement(0, 0, new QCPTextElement(customPlot, strPlotMainTitle));
        }else{
            //QCPLayoutElement *QCPLayoutGrid::elementAt(int index) const
            QCPTextElement* mainTitle = qobject_cast<QCPTextElement*>(customPlot->plotLayout()->elementAt(0));
            mainTitle->setText(strPlotMainTitle);
        }
    }
}

/**
 * @brief 차크 그리기
 * @param customPlot
 * @param strName
 * @param dblValue
 */
void RealtimePlotDock::realRenderingChart(QCustomPlot *customPlot,QString strName, double dblValue)
{
  // calculate two new data points:
  #if QT_VERSION < QT_VERSION_CHECK(4, 7, 0)
    double key = 0;
  #else
    double key = QDateTime::currentDateTime().toMSecsSinceEpoch()/1000.0;
  #endif

    // 차트찾기
    int findChartIndex = -1;
    int nGraphCount = customPlot->graphCount();    

    QString strGraphName;
    for(int i=0; i < nGraphCount; i++){
        strGraphName = customPlot->graph(i)->name();
        if(strGraphName.compare(strName)==0){            
            findChartIndex = i;
            break;
        }
    }

    // 찾은 차트 그리기
    if( findChartIndex != -1 ){
        double dblData = dblValue;

        // customPlot->yAxis->setRange(-0.05, 0.05);
        // QMessageBox::information(this, "TEST", "TEST");

        static double lastPointKey = 0;
        if (key-lastPointKey > 0.002) // at most add point every 2 ms
        {
            customPlot->graph(findChartIndex)->addData(key, dblData);
            // customPlot->graph(findChartIndex)->data()->removeBefore(key-8);

            if(m_dbYMin > dblData){
                m_dbYMin = dblData;
            }
            if(m_dbYMax < dblData){
                m_dbYMax = dblData;
            }

            // qDebug() << "그려진 렌더링 챠트 : " << strName << "값 : " << dblData;
        }

     }

}

/**
 * @brief 실시간 데이터 처리  Slot
 */
void RealtimePlotDock::realtimeDataSlot()
{
#if QT_VERSION < QT_VERSION_CHECK(4, 7, 0)
    double dbKey = 0;
#else
    double dbKey = QDateTime::currentDateTime().toMSecsSinceEpoch()/1000.0;
#endif
    static double dbLastPointKey = 0;
    if (dbKey-dbLastPointKey > 0.002) // at most add point every 10 ms
    {
        //customPlot->yAxis->setRange(-0.05, 0.08);
        dbLastPointKey = dbKey;
        // ui->customplot->yAxis->setRange(m_dbYMax, m_dbYMax);
        // ui->customplot->graph(g_rescaleChartIndex)->rescaleValueAxis(true);

        ui->customplot->yAxis->setRangeLower(m_dbYMin);
        ui->customplot->yAxis->setRangeUpper(m_dbYMax);

    }

    ui->customplot->xAxis->setRange(dbKey+0.25,8,Qt::AlignRight);
    ui->customplot->replot();

    //dbKey +=0.25;

//    QList<QDockWidget*> list = realtimePlotMap.values();
//    QDockWidget* pDock;
//    foreach(pDock,list){
//        RealtimePlotDock* pPlotDock = qobject_cast<RealtimePlotDock*>(pDock);
//        if(pPlotDock){
//            pPlotDock->replot(dbKey,8,-0.05,0.08);
//        }
//    }
    //replot(dbKey,8,-0.05,0.08);
    //m_widgetMainRealtimePlot->replot(-1,key,8,-0.05,0.08);
}

/**
 * @brief MQTT 메세지 처리
 * @param strTopicName
 * @param strData
 * @param strTopicType
 */
void RealtimePlotDock::processMQTTMsg(QString strTopicName,QString strData,QString strTopicType)
{
    double dblValue = 0.0f;
    if(strTopicType.compare("data_ch1_raw") == 0) // raw
    {
        dblValue = strData.toDouble();
        ////timecapaList->addItem(strNow + strTopicName + strData);

        qDebug() << strTopicName << dblValue;
        realRenderingChart(ui->customplot,strTopicName, dblValue);
    }

    if(strTopicType.compare("data_ch1_rawX") == 0) // raw
    {
        dblValue = strData.toDouble();
        realRenderingChart(ui->customplot, strTopicName, dblValue);
    }

    if(strTopicType.compare("data_ch1_rawY") == 0) // raw
    {
        dblValue = strData.toDouble();
        realRenderingChart(ui->customplot, strTopicName, dblValue);
    }

    if(strTopicType.compare("data_ch1_rawZ") == 0) // raw
    {
        dblValue = strData.toDouble();
        realRenderingChart(ui->customplot, strTopicName, dblValue);
    }

    if(strTopicType.compare("data_ch1_p2p") == 0) // p2p
    {
        dblValue = strData.toDouble();
        ////timecapaList->addItem(strNow + strTopicName + strData);

        // QColor color = Qt::red;
        qDebug() << strTopicName << dblValue;
        realRenderingChart(ui->customplot,strTopicName, dblValue);
    }

    if(strTopicType.compare("data_ch1_rms") == 0) // rms
    {
        dblValue = strData.toDouble();
        ////timecapaList->addItem(strNow + strTopicName + strData);

        // QColor color = Qt::blue;
        qDebug() << strTopicName << dblValue;
        realRenderingChart(ui->customplot,strTopicName, dblValue);
    }

    if (strTopicType.compare("event_ch1_p2p") == 0) // event
    {
        QDateTime strCurrentDateTime;
        // strCurrentDateTime = QDateTime::currentDateTimeUtc();
        // strCurrentDateTime = strCurrentDateTime.addSecs(9 * 60 * 60); // UTC -> SEOUL
        strCurrentDateTime = QDateTime::currentDateTime();
        QString now = strCurrentDateTime.toString(Qt::TextDate);

        // QMessageBox::information(this, strTopicName, strData);
        QMessageBox* msgBox = new QMessageBox(this);
        msgBox->setWindowTitle("!!! 충돌/전복");
        msgBox->setText(strTopicName + "\nPeak to Peak value= " + strData + "\n" + now);
        msgBox->setWindowModified(Qt::NonModal);
        msgBox->show();

        // QMessageBox::information(this, "!!! 충돌/전복 :" + strTopicName, "P2P Value = " + strData);
    }
}
