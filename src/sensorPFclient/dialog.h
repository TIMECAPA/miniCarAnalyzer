#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QTextCodec>

// ui comment TEST
namespace Ui {
class Dialog;
}

/**
 * @brief 패킷 전송 및 데이터베이스 연동 다이얼로그 클래스
 */
class Dialog : public QDialog
{
    Q_OBJECT
public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();
private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();
    void on_pushButton_3_clicked();
    void finished(QNetworkReply *reply);
    void on_pushButton_4_clicked();
    void on_pushButton_5_clicked();
    void on_comboBox_currentIndexChanged(const QString &arg1);
    void on_pushButton_9_clicked();

private:
    Ui::Dialog *ui;

    /**
     * @brief 네트워크 관리자
     */
    QNetworkAccessManager *nam;
private:
    void sendControlHttpString(QByteArray strBody);


};

#endif // DIALOG_H
