#ifndef CONTROLSENSORDLG_H
#define CONTROLSENSORDLG_H

#include <QDialog>
#include <QStandardItemModel>
#include <QAbstractItemModel>
#include <QTime>

namespace Ui {
class ControlSensorDlg;
}

/**
 * @brief 센서 상태 및 제어 다이얼로그 클래스
 */
class ControlSensorDlg : public QDialog
{
    Q_OBJECT
public:
    explicit ControlSensorDlg(QWidget *parent = 0);
    ~ControlSensorDlg();
private:
    Ui::ControlSensorDlg *ui;

public:
    void addData(QString strQuery);
};

#endif // CONTROLSENSORDLG_H
