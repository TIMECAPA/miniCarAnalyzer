#include "logindlg.h"
#include "ui_logindlg.h"

#include <QtSql>
#include <QDebug>
#include <QMessageBox>

#include "mainwindow.h"
#include "displaydbsensor.h"

/**
 * @brief 생성자
 * @param parent
 */
LoginDlg::LoginDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoginDlg)
{
    ui->setupUi(this);
}

/**
 * @brief 소멸자
 */
LoginDlg::~LoginDlg()
{
    delete ui;
}

/**
 * @brief 클리어 클릭 이벤트
 */
void LoginDlg::on_btnClear_clicked()
{
    ui->txtUser->setText("");
    ui->txtPass->setText("");
}

/**
 * @brief 로그인 클릭 이벤트
 */
void LoginDlg::on_btnLogin_clicked()
{
    QString Username, Password;

    Username = ui->txtUser->text();
    Password = ui->txtPass->text();

    db = QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName("garigulls.com");
    db.setDatabaseName("examTEST");
    db.setUserName("iot");
    db.setPassword("iot001");

    if (!db.open())
    {
        ui->lblResult->setText("Database Error!!");
        QMessageBox::critical(0, QObject::tr("Database Error"),
               db.lastError().text());
    }
    else
        ui->lblResult->setText("Connected to Database!!!");

    if(!db.isOpen())
    {
        qDebug() << "No connecton to db ";
        return;
    }

    QSqlQuery qry;
    if(qry.exec("select 사용자명, 비밀번호, 전화번호 from user where 사용자명=\'" + Username +
                "\' and 비밀번호 = \'" + Password + "\'"))
    {
        if(qry.next())
        {
            ui->lblResult->setText("Valid Username and Password");
            QString msg = "Username = " + qry.value(0).toString() + " \n" +
                          "Password = " + qry.value(1).toString() + " \n" +
                          "Phone = " + qry.value(2).toString();

            QMessageBox::warning(this, "login confirm", msg);

            // MainWindow의 pointer
            MainWindow* parent = qobject_cast<MainWindow*>(this->parent());

            if(Username.compare("admin")==0)
                parent->dispDBsensor->addData("SELECT * FROM cnt"); // admin은 모든 디바이스 전시
            else
                parent->dispDBsensor->addData("SELECT ri FROM cnt where ri like '%" + Username + "%'"); // 사용자 디바이스
        }
        else
            ui->lblResult->setText("Wrong Username or Password!!");
    }

    db.close();
}
