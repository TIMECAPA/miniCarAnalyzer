#include "controlsensordlg.h"
#include "ui_controlsensordlg.h"
#include <QStandardItemModel>
#include <QTime>
#include <QtSql>
#include <QMenu>
#include <QDebug>
#include <QMessageBox>


/**
 * @brief 생성자
 * @param parent
 */
ControlSensorDlg::ControlSensorDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ControlSensorDlg)
{
    ui->setupUi(this);
    addData("SELECT * FROM cnt");
}

/**
 * @brief 소멸자
 */
ControlSensorDlg::~ControlSensorDlg()
{
    delete ui;
}

/**
 * @brief 센서 목록 가져오기
 * @param strQuery
 */
void ControlSensorDlg::addData(QString strQuery)
{
    ui->treeWidget->clear();


    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName("garigulls.com");
    db.setDatabaseName("mobiusdb3");
    db.setUserName("root");
    db.setPassword("soft0987");

    if (!db.open())
    {
        QMessageBox::critical(0, QObject::tr("Database Error"),
               db.lastError().text());
    }

    QSqlQuery query;
    query.exec(strQuery);

    QString containerName[10000];

    int index=0;
    while(query.next())
    {
        containerName[index] = query.value(0).toString();
        index++;
    }

    int numIndex = index;

    ui->treeWidget->setColumnCount(3);
    ui->treeWidget->setHeaderLabels(QStringList() << " No. Track" << "miniCAR"<< "sensor" << "ch#" << "position" << "factor" << "TR" << "status" << "sampling");
    ui->treeWidget->header()->resizeSection(0, 100);
    ui->treeWidget->header()->resizeSection(1, 170);


    QMap<QString, QTreeWidgetItem*> mapAE;
    QMap<QString, QTreeWidgetItem*> mapTYPE;
    QMap<QString, QTreeWidgetItem*> mapMESURE;
    QStringList List;

    QString cseNAME = ""; // cse
    QString aeNAME = ""; // ae
    QString typeNAME = ""; // sensor type
    QString mesureNAME = ""; // mesure

    QString strName;

/*
    const int numIndex = 9;
    QString containerName[numIndex] = {
        "mobius-yt/bridgea/soc01/item01", "mobius-yt/bridgeb/soc01/item02", "mobius-yt/bridgea/soc04/item01" ,
        "mobius-yt/bridgea/soc02/item01", "mobius-yt/bridgeb/soc01/item02", "mobius-yt/bridgea/soc03/item01" ,
        "mobius-yt/bridgea/soc03/item02", "mobius-yt/bridgec/soc05/item04", "mobius-yt/bridgea/soc05/item05"
    };
*/

    for(int i=0; i < numIndex; i++)
    {
        List = containerName[i].split("/");

        //////////////////////////// AE //////////////////////////////////////
        strName = List.at(1); // AE
        if(mapAE.contains(strName)) // 기존 컨테이너가 있다면
        {
            QTreeWidgetItem *it = mapAE.value(strName);
            it->setText(0, strName);
            it->setText(1, "");
            it->setText(2, "");
            it->setExpanded(true);
        }
        else // 새로운 컨테이너
        {
            // aeNAME = strName;
            QTreeWidgetItem *item =new QTreeWidgetItem(ui->treeWidget);
            mapAE.insert(strName, item);
            item->setText(0, strName);
            item->setText(1, "");
            item->setText(2, "");
            item->setExpanded(true);

        }

        /////////////////////////////  Senor Type  ////////////////////////////////////

        strName = List.at(1) + List.at(2); // sensor type
        if(mapTYPE.contains(strName)) // 기존 센서type 이라면 생성한다
        {
            QTreeWidgetItem *it = mapTYPE.value(strName);
            it->setText(0, List.at(2)); // 센서type의 이름만 자식노드로 추가함
            it->setText(1, "");
            it->setText(2, "");
            it->setExpanded(true);
            mapAE.value(List.at(1))->addChild(it);
            it->setExpanded(true);

        }
        else
        {
            // typeNAME = strName;
            QTreeWidgetItem *item =new QTreeWidgetItem();
            mapTYPE.insert(strName, item);
            item->setText(0, List.at(2)); //센서type의 이름만 자식노드로 추가함
            item->setText(1, "");
            item->setText(2, "");
            item->setExpanded(true);
            mapAE.value(List.at(1))->addChild(item);
            item->setExpanded(true);

        }

        /////////////////////////////  mesure Type  ////////////////////////////////////
        strName = List.at(1) + List.at(2) + List.at(3); // sensor type
        if(mapTYPE.contains(strName)) // 기존 센서type 이라면 생성한다
        {
            QTreeWidgetItem *it = mapTYPE.value(strName);
            it->setText(0, ""); // 센서type의 이름만 자식노드로 추가함
            it->setText(1, List.at(3));
            it->setText(2, "Acc");
            it->setText(3, "Ch_#" + QString::number(i));
            it->setText(4, "0,0,0");
            it->setText(5, "F1/F2/F3");
            // it->setText(6, "그룹");
            it->setIcon(7, QIcon(":/images/print.png"));
            it->setText(7, "On");
            // it->setText(8, "샘플링");
            it->setFlags(it->flags() | Qt::ItemIsEditable);
            it->setFlags(it->flags() | Qt::ItemIsUserCheckable);
            it->setCheckState(1, Qt::Unchecked);
            mapTYPE.value(List.at(1) + List.at(2))->addChild(it);
        }
        else
        {
            // typeNAME = strName;
            QTreeWidgetItem *item =new QTreeWidgetItem();
            mapTYPE.insert(strName, item);

            item->setText(0, ""); //센서type의 이름만 자식노드로 추가함
            item->setText(1, List.at(3));
            item->setText(2, "Acc");
            item->setText(3, "SeqCH #" + QString::number(i));
            item->setText(4, "0,0,0");
            item->setText(5, "F1/F2/F3");
            // item->setText(6, "그룹");
            item->setIcon(7, QIcon(":/images/print.png"));
            item->setText(7, "On");
            // item->setText(8, "샘플링");
            item->setFlags(item->flags() | Qt::ItemIsEditable);
            item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
            item->setCheckState(1, Qt::Unchecked);
            mapTYPE.value(List.at(1) + List.at(2))->addChild(item);

            QComboBox *grp = new QComboBox(this);
            grp->addItem("Posi#1");
            grp->addItem("Posi#2");
            grp->addItem("Posi#3");
            grp->setStyleSheet("QComboBox {background-color : #fafafa; }");
            ui->treeWidget->setItemWidget(item, 6, grp);

            QComboBox *sampling = new QComboBox(this);
            sampling->addItem("50Hz");
            sampling->addItem("100Hz");
            sampling->addItem("150Hz");
            sampling->addItem("200Hz");
            sampling->setStyleSheet("QComboBox {background-color : #fafafa; }");
            ui->treeWidget->setItemWidget(item, 8, sampling);
        }

    }

    db.close();
}


