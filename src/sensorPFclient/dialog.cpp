#include "dialog.h"
#include "ui_dialog.h"
#include <QDebug>
#include <QNetworkAccessManager>
#include <QJsonValue>
#include <QJsonObject>
#include <QJsonDocument>

#include <QtSql>
#include <QTableWidgetItem>
#include <QMessageBox>
#include <iostream> // QString -> QByteArray
#include <QThread>

/**
 * @brief 생성자
 * @param parent
 */
Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

    nam = new QNetworkAccessManager(this);

    /*
    connect(ui->pushButton_submit,
            SIGNAL(clicked()),
            this,
            SLOT(on_pushButton_submit_clicked()));
    connect(ui->pushButton_clear,
            SIGNAL(clicked()),
            this,
            SLOT(on_pushButton_clear_clicked()));
    */

    connect(nam,
            SIGNAL(finished(QNetworkReply*)),
            this,
            SLOT(finished(QNetworkReply*)));
}

/**
 * @brief 소멸자
 */
Dialog::~Dialog()
{
    delete ui;
}

/**
 * @brief DB 접속 및 컨테이너 목록 얻어오기
 */
void Dialog::on_pushButton_clicked()
{

    QTableWidget* table = new QTableWidget();
    table->setWindowTitle("Connect to Mysql Database Example");

    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName("garigulls.com");
    db.setDatabaseName("mobiusdb3");
    db.setUserName("root");
    db.setPassword("soft0987");
    if (!db.open())
    {
      QMessageBox::critical(0, QObject::tr("Database Error"),
                db.lastError().text());
    }

    QSqlQuery query("SELECT * FROM cnt");

    table->setColumnCount(query.record().count());
    table->setRowCount(query.size());

    int index=0;
    while (query.next())
    {
    table->setItem(index,0,new QTableWidgetItem(query.value(0).toString()));
    table->setItem(index,1,new QTableWidgetItem(query.value(1).toString()));
    index++;
    }

    table->show();

}

/**
 * @brief 제어패킷 Head와 body 문자열
 * @param strBody
 */
void Dialog::sendControlHttpString(QByteArray strBody)
{
    QString url = "http://210.105.85.7:7579/mobius-yt/bridgeA/control_soc01_ch1";
    QNetworkRequest request= QNetworkRequest(QUrl(url));

    QByteArray post_data = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<m2m:cin\n    xmlns:m2m=\"http://www.onem2m.org/xml/protocols\" \n    xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n  <con>" + strBody +"</con>  \n</m2m:cin>";

    request.setRawHeader("postman-token", "56485f48-d47d-615a-d80f-9983e16b31b1");
    request.setRawHeader("cache-control", "no-cache");
    request.setRawHeader("nmtype", "short");
    request.setRawHeader("content-type", "application/vnd.onem2m-res+xml; ty=4");
    request.setRawHeader("x-m2m-origin", "/0.2.481.1.21160310105204806");
    request.setRawHeader("x-m2m-ri", "12345");
    request.setRawHeader("locale", "ko");
    request.setRawHeader("accept", "application/xml");


    if(post_data.isEmpty())
    {
        nam->get(request);
    }
    else
    {
        nam->post(request,post_data);
    }
}


/**
 * @brief 패킷전송시작
 */
void Dialog::on_pushButton_2_clicked()
{
    sendControlHttpString("1");
}

/**
 * @brief 패킷전송중지
 */
void Dialog::on_pushButton_4_clicked()
{
    sendControlHttpString("0");
}

/**
 * @brief 범위데이터 가져오기
 */
void Dialog::on_pushButton_3_clicked()
{
    QString url = "http://210.105.85.7:7579/mobius-yt/bridgeA/raynor_soc99/data_ch1_p2p";
    QString paras = ui->pTextEdit_paras->toPlainText();

    qDebug() << "aaaaaaaaa" << paras;

    QByteArray post_data;
    /*
    QByteArray b((const char*) (paras.utf16()), paras.size() * 2);
    post_data.append(b);
    */

    url = url + QString("?") + paras;
    QNetworkRequest request= QNetworkRequest(QUrl(url));

    request.setRawHeader("accept", "application/xml");
    request.setRawHeader("x-m2m-ri", "12345");
    request.setRawHeader("x-m2m-origin", "/0.2.481.1.21160310105204806");
    request.setRawHeader("nmtype", "long");
    request.setRawHeader("cache-control", "no-cache");
    request.setRawHeader("postman-token", "3690e173-5a9e-5c6a-3e69-d7a802cafc8a");

    if(post_data.isEmpty())
    {
        nam->get(request);
    }
    else
    {
        nam->post(request,post_data);
    }
}

/**
 * @brief 네트워크 응답 처리
 * @param reply
 */
void Dialog::finished(QNetworkReply *reply)
{
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));

    if(reply->error() == QNetworkReply::NoError)
    {
        ui->textEdit_result->setText(QObject::tr(reply->readAll()));

        /*
        QString strReply = (QString)reply->readAll();

        QJsonDocument jsonResponse = QJsonDocument::fromJson(strReply.toUtf8());
        QJsonObject jsonObj = jsonResponse.object();

        qDebug() << "all data: " << strReply;
        qDebug() << "content: " << jsonObj["content"].toString();

        QString strData = jsonObj["content"].toString();

        ui->textEdit_result->setText(strData);
        */

    }
    else
    {
        ui->textEdit_result->setPlainText(reply->errorString());
    }
}


/**
 * @brief 임계값 설정
 */
void Dialog::on_pushButton_5_clicked()
{
    QByteArray strByteArrayEdit = ui->lineEdit->text().toStdString().c_str(); // QString to byteArray
    QByteArray strByteArrayID = "EVENTp2p";

    QByteArray strEventPeakValue =  strByteArrayID + "-" + strByteArrayEdit + "-";


    // sendControlHttpString("0");
    sendControlHttpString(strEventPeakValue);
    // sendControlHttpString("1");

}

/**
 * @brief 샘플링레이트
 * @param arg1
 */
void Dialog::on_comboBox_currentIndexChanged(const QString &arg1)
{
    // QString a = ui->comboBox->currentText();
    QByteArray strComboBox = arg1.toStdString().c_str();
    QByteArray strByteArrayID = "UARTset";

    QByteArray strUARTcommand = strByteArrayID + "-" + strComboBox + "-";
    sendControlHttpString(strUARTcommand);
}

/**
 * @brief soc#2 시작제어
 */
void Dialog::on_pushButton_9_clicked()
{
    ;
}
