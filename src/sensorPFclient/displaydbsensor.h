#ifndef DISPLAYDBSENSOR_H
#define DISPLAYDBSENSOR_H

#include <QtGui>
#include <QDockWidget>
class QTreeWidgetItem;

namespace Ui {
class DisplayDBsensor;
}

/**
 * @brief 센서 목록 도킹 창 클래스
 */
class DisplayDBsensor : public QDockWidget
{
    Q_OBJECT
public:
    explicit DisplayDBsensor(QWidget *parent = 0);
    ~DisplayDBsensor();
signals:
    void signalUpdatePlot(QTreeWidgetItem *item, int column);
private slots:    
    void slotTreeWidgetItemChanged(QTreeWidgetItem *item, int column);
    void slotShowContextMenu(QPoint pos);
    void slotSetPlot(QString strSelInfo);
private:
    Ui::DisplayDBsensor *ui;

    /**
     * @brief 컨텍스트 시점 TreeItem
     */
    QTreeWidgetItem*    m_treeItemOnContext;
public:
    /**
     * @brief 저장된 컨테이너
     */
    QString containerName[10000];

    /**
     * @brief 저장된 컨테이너의 갯수
     */
    int numIndex;
private:
    //void matchPlot(int nPlotIdx=0);
    int  treeWidgetDepth(QTreeWidgetItem* item);
public:
    void setTreeWidgetItemChangedConnect(bool bConnect);
    void addData(QString query);
    void treeWidgetClear();
};

#endif // DISPLAYDBSENSOR_H
