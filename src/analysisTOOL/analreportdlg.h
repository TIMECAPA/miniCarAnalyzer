#ifndef ANALREPORTDLG_H
#define ANALREPORTDLG_H

#include <QDialog>
#include "qcustomplot.h"
#include "mainwindow.h"

class QFrame;
class QCustomPlot;
class QGridLayout;

class ControlOptions;



namespace Ui {
class AnalReportDlg;
}

/**
 * @brief 분석보고서 다이얼로그 클래스
 */
class AnalReportDlg : public QDialog
{
    Q_OBJECT
public:
    explicit AnalReportDlg(QWidget *parent = 0);
    ~AnalReportDlg();
    enum EN_AXIS_DATA{
        enRawData=0,
        enRawDataX,
        enRawDataY,
        enRawDataZ,
        enFixRawData,
        enFixRawDataX,
        enFixRawDataY,
        enFixRawDataZ,
        enAxisDataMax
    };

private:
    Ui::AnalReportDlg *ui;    
    typedef std::map<QString,ControlOptions&> MapControlOtpions;   
    typedef std::pair<QString,ControlOptions&> PairControlOptions;
    /**
     * @brief 컨트롤 속성 아이템 관리 맵
     */
    MapControlOtpions m_mapControlOptions;
private slots:
    void slotRawContextMenuReq(QPoint pos);
    void slotFFTContextMenuReq(QPoint pos);
    void slotAvgLCDNumContextMenuReq(QPoint pos);
    void slotP2PLCDNumContextMenuReq(QPoint pos);
    void slotRMSLCDNumContextMenuReq(QPoint pos);
    void slotVarianceLEDContextMenuReq(QPoint pos);
    void slotSnDLEDContextMenuReq(QPoint pos);

    void slotShowRawSetupDock();
    void slotShowFFTSetupDock();
    void slotShowAvgLCDNumSetupDock();
    void slotShowP2PLCDNumSetupDock();
    void slotShowRMSLCDNumSetupDock();
    void slotShowVarianceLEDSetupDock();
    void slotShowSnDLEDSetupDock();

private:
    /**
     * @brief X축 Raw 데이터
     */
    QVector<double>     m_vXAxisRawVal[AnalReportDlg::enAxisDataMax];

    /**
     * @brief Y축 Raw 데이터
     */
    QVector<double>     m_vYAxisRawVal[AnalReportDlg::enAxisDataMax];

    /**
     * @brief FFT 데이터
     */
    CArray              m_arrFFTData[enAxisDataMax];

    /**
     * @brief 데이터 최대 값
     */
    double              m_dbYMax;

    /**
     * @brief 데이터 최소 값
     */
    double              m_dbYMin;

    /**
     * @brief 데이터 최대 값
     */
    double              m_dbFFTYMax;

    /**
     * @brief 데이터 최소 값
     */
    double              m_dbFFTYMin;

    bool                m_bVisiableRawGraph[32];
    bool                m_bVisiableFFTGraph[32];

    /**
     * @brief 드래그 Over 상태
     */
    bool  m_bDragOver;
    /**
     * @brief 드래그 시 하이라이트 Rect
     */
    QRect m_rectHighlighted;

protected:
    void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
    void dragEnterEvent(QDragEnterEvent *event) Q_DECL_OVERRIDE;
    void dragMoveEvent(QDragMoveEvent *event) Q_DECL_OVERRIDE;
    void dragLeaveEvent(QDragLeaveEvent *event) Q_DECL_OVERRIDE;
    void dropEvent(QDropEvent *event) Q_DECL_OVERRIDE;
    void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseDoubleClickEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
private:    
    void setupCustomPlot(QCustomPlot* customPlot,bool bTimeTicker=true);
    QFrame* currentDragFrame(QString strToolBarName);
    void showAllFrames(bool bShow);
    void updateControl(ControlOptions& options);
 public:
    void reloadPlot(QString strGraphName);
    void reloadFFTPlot(QString strGraphName);
    void replotAnalysisRawData(QString strGraphName,QVector<double>& vXAxis, QVector<double>& vYAxisX,QVector<double>& vYAxisY, QVector<double>& vYAxisZ,double& dbYMax,double& dbYMin);
    void replotAnalysisFFTData(QString strGraphName,QVector<double>& vXAxis, QVector<double>& vYAxisX,QVector<double>& vYAxisY, QVector<double>& vYAxisZ,double& dbYMax,double& dbYMin);
    void setVisibleRawGraph(int nMode =0, bool bShow = true);
    void setVisibleFFTGraph(int nMode =0, bool bShow = true);
    void setVisibleGraph(QCustomPlot* customPlot,int nMode =0, bool bShow = true);
    void clearAll();
    void clearPlot(int nIdx=-1);
    void setDefaultUI(quint32 uiAppliedAlgorithm);
    void setAvg();
    void setP2P();
    void setRMS();
    void setVariance();
    void setSnD();
    void saveRawDataToFile();
    void writeRawDataToFile(QCustomPlot* customplot,QString strSaveFileName);
    void saveFFTDataToFile();
    void writeFFTDataToFile(QCustomPlot* customplot,QString strSaveFileName);

    void setControlOptions(ControlOptions& options);

public:
    void loadAnalysisData(QSqlDatabase& m_dbMySQL,QString strContainerName,QString strStart,QString strEnd);
    void loadAnalysisFixData(QSqlDatabase& m_dbMySQL,QString strContainerName,QString strStart,QString strEnd,double& dbMax, double& dbMin);
};


#endif // ANALREPORTDLG_H
