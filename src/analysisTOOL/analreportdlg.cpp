#include "analreportdlg.h"
#include "ui_analreportdlg.h"


#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QMimeData>
#include <QPixmap>
#include <QDebug>
#include <QGridLayout>
#include <QDebug>
#include <time.h>       /* time */

#include <iostream>
#include <random>

/**
 * @brief 생성자
 * @param parent
 */
AnalReportDlg::AnalReportDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AnalReportDlg),
    m_bDragOver(false)
{
    ui->setupUi(this);

    for(int i=0; i<32;i++){
        m_bVisiableRawGraph[i] = true;
        m_bVisiableFFTGraph[i] = true;
    }

    ui->lcdAvgNumber->setValue(0.0000000000000);
    ui->lcdP2PNumber->setValue(0.0000000000000);
    ui->lcdRMSNumber->setValue(0.0000000000000);

    showAllFrames(false);    

    setAcceptDrops(true);

    setupCustomPlot(ui->customPlotFFT,false);
    setupCustomPlot(ui->customPlotRaw);

    ui->customPlotRaw->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->customPlotRaw, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(slotRawContextMenuReq(QPoint)));

    ui->customPlotFFT->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->customPlotFFT, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(slotFFTContextMenuReq(QPoint)));

    ui->lcdAvgNumber->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->lcdAvgNumber, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(slotAvgLCDNumContextMenuReq(QPoint)));

    ui->lcdP2PNumber->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->lcdP2PNumber, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(slotP2PLCDNumContextMenuReq(QPoint)));

    ui->lcdRMSNumber->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->lcdRMSNumber, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(slotRMSLCDNumContextMenuReq(QPoint)));

    ui->ledSnDWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->ledSnDWidget, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(slotSnDLEDContextMenuReq(QPoint)));

    ui->ledVarianceWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->ledVarianceWidget, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(slotVarianceLEDContextMenuReq(QPoint)));

}

/**
 * @brief 소멸자
 */
AnalReportDlg::~AnalReportDlg()
{
    delete ui;
}

/**
 * @brief customplot 초기 설정
 * @param customPlot
 */
void AnalReportDlg::setupCustomPlot(QCustomPlot* customPlot,bool bTimeTicker)
{
#if QT_VERSION < QT_VERSION_CHECK(4, 7, 0)
  QMessageBox::critical(this, "", "You're using Qt < 4.7, the realtime data demo needs functions that are available with Qt 4.7 to work properly");
#endif

    QFont serifFont("Times", 8, QFont::Bold);

    customPlot->legend->setVisible(true);
    customPlot->legend->setFont(serifFont);

    if(bTimeTicker){
        QSharedPointer<QCPAxisTickerDateTime> timeTicker(new QCPAxisTickerDateTime);
        customPlot->xAxis->setRange(-60*3.5, 60*11);

        //timeTicker->setTimeFormat("%m:%s");
        //timeTicker->setDateTimeFormat("day dd\nhh:mm:ss");
        timeTicker->setDateTimeFormat("yy.MM.dd \nhh:mm:ss");\
        timeTicker->setDateTimeSpec(Qt::UTC);
        //timeTicker->setTickStepStrategy(QCPAxisTicker::TickStepStrategy::tssReadability);
        timeTicker->setTickCount(10);
        customPlot->xAxis->setTicker(timeTicker);
    }
    customPlot->axisRect()->setupFullAxesBox();


    // set some pens, brushes and backgrounds:
    customPlot->xAxis->setBasePen(QPen(Qt::white, 1));
    customPlot->yAxis->setBasePen(QPen(Qt::white, 1));
    customPlot->xAxis->setTickPen(QPen(Qt::white, 1));
    customPlot->yAxis->setTickPen(QPen(Qt::white, 1));
    customPlot->xAxis->setSubTickPen(QPen(Qt::white, 1));
    customPlot->yAxis->setSubTickPen(QPen(Qt::white, 1));
    customPlot->xAxis->setTickLabelColor(Qt::white);
    customPlot->yAxis->setTickLabelColor(Qt::white);
    customPlot->xAxis->grid()->setPen(QPen(QColor(140, 140, 140), 1, Qt::DotLine));
    customPlot->yAxis->grid()->setPen(QPen(QColor(140, 140, 140), 1, Qt::DotLine));
    customPlot->xAxis->grid()->setSubGridPen(QPen(QColor(80, 80, 80), 1, Qt::DotLine));
    customPlot->yAxis->grid()->setSubGridPen(QPen(QColor(80, 80, 80), 1, Qt::DotLine));
    customPlot->xAxis->grid()->setSubGridVisible(true);
    customPlot->yAxis->grid()->setSubGridVisible(true);
    customPlot->xAxis->grid()->setZeroLinePen(Qt::NoPen);
    customPlot->yAxis->grid()->setZeroLinePen(Qt::NoPen);
    customPlot->xAxis->setUpperEnding(QCPLineEnding::esSpikeArrow);
    customPlot->yAxis->setUpperEnding(QCPLineEnding::esSpikeArrow);

    QLinearGradient plotGradient;
    plotGradient.setStart(0, 0);
    plotGradient.setFinalStop(0, 350);
    plotGradient.setColorAt(0, QColor(80, 80, 80));
    plotGradient.setColorAt(1, QColor(50, 50, 50));
    customPlot->setBackground(plotGradient);

    QLinearGradient axisRectGradient;
    axisRectGradient.setStart(0, 0);
    axisRectGradient.setFinalStop(0, 350);
    axisRectGradient.setColorAt(0, QColor(80, 80, 80));
    axisRectGradient.setColorAt(1, QColor(30, 30, 30));
    customPlot->axisRect()->setBackground(axisRectGradient);

    customPlot->setInteraction(QCP::iRangeDrag);
    //ui->customPlot->axisRect()->setRangeDrag(Qt::Vertical|Qt::Horizontal);
    customPlot->axisRect()->setRangeDragAxes(customPlot->xAxis,customPlot->yAxis);

    customPlot->setInteraction(QCP::iRangeZoom);
    customPlot->axisRect()->setRangeZoom(Qt::Vertical|Qt::Horizontal);
    // make left and bottom axes transfer their ranges to right and top axes:
    //  connect(ui->customPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), ui->customPlot->xAxis2, SLOT(setRange(QCPRange)));
    //  connect(ui->customPlot->yAxis, SIGNAL(rangeChanged(QCPRange)), ui->customPlot->yAxis2, SLOT(setRange(QCPRange)));
    customPlot->setInteraction(QCP::iSelectItems);

}

/**
 * @brief 드래그 Enter 이벤트 처리
 * @param event
 */
void AnalReportDlg::dragEnterEvent(QDragEnterEvent *event)
{

    if (event->mimeData()->hasImage()) {
        event->setAccepted(true);
        m_bDragOver = true;
        update();
    } else {
        AnalReportDlg::dragEnterEvent(event);
    }
}

/**
 * @brief 드래그 Move 이벤트 처리
 * @param event
 */
void AnalReportDlg::dragMoveEvent(QDragMoveEvent *event)
{
    //QRect rect = m_pMainGridLayout->cellRect(1,1);
    if(event->mimeData()->hasText()){
        QFrame* pFrame = currentDragFrame(event->mimeData()->text());
        if(pFrame){
            QRect rectPlot = pFrame->rect();
            QPoint pos = event->pos();
            qDebug() << "dragMoveEvent Pos x : " << pos.x() << "Pos y :" << pos.y() << endl;

            m_rectHighlighted.setLeft(pos.x());
            m_rectHighlighted.setTop(pos.y());
            m_rectHighlighted.setRight(pos.x()+rectPlot.width());
            m_rectHighlighted.setBottom(pos.y()+rectPlot.height());

            event->setDropAction(Qt::MoveAction);
            event->accept();
            //QRect updateRect = highlightedRect.united(targetSquare(event->pos()));
            update();
        }
    }

}

/**
 * @brief 드래그 Leave 이벤트 처리
 * @param event
 */
void AnalReportDlg::dragLeaveEvent(QDragLeaveEvent *event)
{
    Q_UNUSED(event);
    m_bDragOver = false;

//  QRect updateRect = highlightedRect;
    m_rectHighlighted = QRect();
//  update(updateRect);
    update();
}

/**
 * @brief 드래그 이벤트 처리
 * @param event
 */
void AnalReportDlg::dropEvent(QDropEvent *event)
{
    if (event->mimeData()->hasImage()) {
        m_bDragOver = false;
        //pixmap = qvariant_cast<QPixmap>(event->mimeData()->imageData());
        if(event->mimeData()->hasText()){
            qDebug() << "dropEvent MimeData Text : " << event->mimeData()->text() << endl;
            QPoint pos = event->pos();
            QFrame* pFrame = currentDragFrame(event->mimeData()->text());
            if(pFrame){
                pFrame->setGeometry(pos.x(),pos.y(),pFrame->rect().width(),pFrame->rect().height());
                pFrame->show();
            }
        }
        m_rectHighlighted = QRect();
        update();
    } else {
        QDialog::dropEvent(event);
    }
}

/**
 * @brief 그리기 이벤트
 * @param event
 */
void AnalReportDlg::paintEvent(QPaintEvent *event)
{
    QPainter painter;
    painter.begin(this);
    painter.fillRect(event->rect(), Qt::white);

    if (m_rectHighlighted.isValid()) {
        painter.setBrush(QColor("#ffcccc"));
        painter.setPen(Qt::NoPen);
        painter.drawRect(m_rectHighlighted.adjusted(0, 0, -1, -1));
    }

    //    for (int i = 0; i < pieceRects.size(); ++i)
    //        painter.drawPixmap(pieceRects[i], piecePixmaps[i]);
    painter.end();
}

/**
 * @brief 마우스 Press 이벤트
 * @param event
 */
void AnalReportDlg::mousePressEvent(QMouseEvent *event)
{

    QDialog::mousePressEvent(event);
}

/**
 * @brief 마우스 Release 이벤트
 * @param event
 */
void AnalReportDlg::mouseReleaseEvent(QMouseEvent *event)
{

    QDialog::mouseReleaseEvent(event);
}

/**
 * @brief 마우스 DoubleClick 이벤트
 * @param event
 */
void AnalReportDlg::mouseDoubleClickEvent(QMouseEvent *event)
{
    QDialog::mouseDoubleClickEvent(event);
}

/**
 * @brief 마우스 Move 이벤트
 * @param event
 */
void AnalReportDlg::mouseMoveEvent(QMouseEvent *event)
{    
    update(); //Frame에 afterimage 문제 때문
    QDialog::mouseMoveEvent(event);
}

/**
 * @brief 현재 드래그 Frame 지정
 * @param strToolBarName
 * @return
 */
QFrame* AnalReportDlg::currentDragFrame(QString strToolBarName)
{
    QFrame* pFrame = NULL;
    if(strToolBarName.compare(DEF_OF_FFTPLOT)==0){
        pFrame = ui->customPlotFFTFrame;
    }else if(strToolBarName.compare(DEF_OF_RAWPLOT)==0){
        pFrame = ui->customPlotRawFrame;
    }else if(strToolBarName.compare(DEF_OF_RMSLCDNUM)==0){
        pFrame = ui->numRMSLCDFrame;
    }else if(strToolBarName.compare(DEF_OF_P2PLCDNUM)==0){
        pFrame = ui->numP2PLCDFrame;
    }else if(strToolBarName.compare(DEF_OF_AVGLCDNUM)==0){
        pFrame = ui->numAvgLCDFrame;
    }else if(strToolBarName.compare(DEF_OF_VARIANCELED)==0){
        pFrame = ui->ledVarianceFrame;
    }else if(strToolBarName.compare(DEF_OF_STDEVLED)==0){
        pFrame = ui->ledSnDFrame;
    }
    return pFrame;
}
void AnalReportDlg::reloadPlot(QString strGraphName)
{
    replotAnalysisRawData(strGraphName,
                                 m_vXAxisRawVal[enRawData],
                                 m_vYAxisRawVal[enRawDataX],
                                 m_vYAxisRawVal[enRawDataY],
                                 m_vYAxisRawVal[enRawDataZ],m_dbYMax,m_dbYMin);
}

void AnalReportDlg::reloadFFTPlot(QString strGraphName)
{
    int nSize = m_arrFFTData[enRawDataX].size();
    QVector<double> vXAxis;
    QVector<double> vYAxisX,vYAxisY,vYAxisZ;
    if(nSize>0){
        RayMath::fft(m_arrFFTData[enRawDataX]);
        RayMath::fft(m_arrFFTData[enRawDataY]);
        RayMath::fft(m_arrFFTData[enRawDataZ]);
        for(int i=0; i<nSize; i++){
            vXAxis.push_back(i);
            vYAxisX.push_back(qAbs(m_arrFFTData[enRawDataX][i].real()));
            vYAxisY.push_back(qAbs(m_arrFFTData[enRawDataY][i].real()));
            vYAxisZ.push_back(qAbs(m_arrFFTData[enRawDataZ][i].real()));

            double dbYVal = 0.0;
            dbYVal = qAbs(m_arrFFTData[enRawDataX][i].real());
            if(m_dbFFTYMax < dbYVal ){
                m_dbFFTYMax = dbYVal;
            }
            dbYVal = qAbs(m_arrFFTData[enRawDataY][i].real());
            if(m_dbFFTYMax < dbYVal){
                m_dbFFTYMax = dbYVal;
            }
            dbYVal = qAbs(m_arrFFTData[enRawDataZ][i].real());
            if(m_dbFFTYMax < dbYVal){
                m_dbFFTYMax = dbYVal;
            }
        }
        m_dbFFTYMin = -1;
        replotAnalysisFFTData(strGraphName,vXAxis,vYAxisX,vYAxisY,vYAxisZ,m_dbFFTYMax,m_dbFFTYMin);
    }
}

void AnalReportDlg::replotAnalysisRawData(QString strGraphName,QVector<double>& vXAxis, QVector<double>& vYAxisX,QVector<double>& vYAxisY, QVector<double>& vYAxisZ,double& dbYMax,double& dbYMin)
{
    QCustomPlot* customPlot=NULL;
    customPlot = ui->customPlotRaw;
    int nGraphCnt = customPlot->graphCount();

    customPlot->addGraph();
    customPlot->graph(0)->setName(strGraphName+"X");

    customPlot->graph(0)->setPen(QPen(QColor(255, 0, 0), 2));
    customPlot->graph(0)->setData(vXAxis, vYAxisX);
    if(!m_bVisiableRawGraph[0]){
        setVisibleGraph(customPlot,0,false);
    }
    //customPlot->graph(0)->rescaleAxes();

    customPlot->addGraph();
    customPlot->graph(1)->setName(strGraphName+"Y");
    customPlot->graph(1)->setPen(QPen(QColor(0, 255, 0), 2));
    customPlot->graph(1)->setData(vXAxis, vYAxisY);
    if(!m_bVisiableRawGraph[1]){
        setVisibleGraph(customPlot,1,false);
    }
    //customPlot->graph(1)->rescaleAxes();


    customPlot->addGraph();
    customPlot->graph(2)->setName(strGraphName+"Z");
    customPlot->graph(2)->setPen(QPen(QColor(0,  0, 255), 2));
    customPlot->graph(2)->setData(vXAxis, vYAxisZ);
    if(!m_bVisiableRawGraph[2]){
        setVisibleGraph(customPlot,2,false);
    }
    //customPlot->graph(2)->rescaleAxes();


    customPlot->xAxis->setLabel("시간");
    customPlot->yAxis->setLabel("값");
    customPlot->rescaleAxes();
    customPlot->yAxis->setRange(dbYMin - qAbs((dbYMax-dbYMin)/5) , dbYMax + qAbs((dbYMax-dbYMin)/5));
    customPlot->replot();
}

/**
 * @brief 분석 툴 RePlot
 * @param nIdx
 * @param strGraphName
 * @param x
 * @param y
 */
void AnalReportDlg::replotAnalysisFFTData(QString strGraphName, QVector<double>& vXAxis,QVector<double>& vYAxisX,QVector<double>& vYAxisY, QVector<double>& vYAxisZ,double& dbYMax,double& dbYMin)
{
    QCustomPlot* customPlot=NULL;
    customPlot = ui->customPlotFFT;

    customPlot->addGraph();
    customPlot->graph(0)->setName(strGraphName+"X");
    customPlot->graph(0)->setPen(QPen(QColor(255, 0, 0), 2));
    customPlot->graph(0)->setData(vXAxis, vYAxisX);
    if(!m_bVisiableFFTGraph[0]){
         setVisibleGraph(customPlot,0,false);
    }

    customPlot->addGraph();
    customPlot->graph(1)->setName(strGraphName+"Y");
    customPlot->graph(1)->setPen(QPen(QColor(0, 255, 0), 2));
    customPlot->graph(1)->setData(vXAxis, vYAxisY);
    if(!m_bVisiableFFTGraph[1]){
         setVisibleGraph(customPlot,1,false);
    }

    customPlot->addGraph();
    customPlot->graph(2)->setName(strGraphName+"Z");
    customPlot->graph(2)->setPen(QPen(QColor(0, 0, 255), 2));
    customPlot->graph(2)->setData(vXAxis, vYAxisZ);
    if(!m_bVisiableFFTGraph[2]){
         setVisibleGraph(customPlot,2,false);
    }

    customPlot->xAxis->setLabel("Idx");
    customPlot->yAxis->setLabel("amplitude");
    customPlot->rescaleAxes();
    customPlot->yAxis->setRange(-1, vXAxis.size());
    customPlot->yAxis->setRange(dbYMin - qAbs((dbYMax-dbYMin)/5) , dbYMax + qAbs((dbYMax-dbYMin)/5));
    customPlot->replot();
}

void AnalReportDlg::setVisibleRawGraph(int nMode /*0*/, bool bShow /*true*/)
{
    QCustomPlot* customPlot = ui->customPlotRaw;
    if(customPlot->graph(nMode)){
        m_bVisiableRawGraph[nMode] = bShow;
        setVisibleGraph(customPlot,nMode,bShow);
        customPlot->replot();
    }
}
void AnalReportDlg::setVisibleFFTGraph(int nMode /*0*/, bool bShow /*true*/)
{
    QCustomPlot* customPlot = ui->customPlotFFT;
    if(customPlot->graph(nMode)){
        m_bVisiableFFTGraph[nMode] = bShow;
        setVisibleGraph(customPlot,nMode,bShow);
        customPlot->replot();
    }
}

void AnalReportDlg::setVisibleGraph(QCustomPlot* customPlot,int nMode /*0*/, bool bShow /*true*/)
{
    if(customPlot->graph(nMode)){
        customPlot->graph(nMode)->setVisible(bShow);
        QCPLegend *legend = customPlot->legend;
        legend->elementAt(nMode)->setVisible(bShow);
        legend->updateLayout();
    }
}

/**
 * @brief 평균 값 설정
 * @param dbAvg
 */
void AnalReportDlg::setAvg()
{
//    mt19937::result_type seed = time(0);
//    auto dice_rand = std::bind(std::uniform_int_distribution<int>(1,6),mt19937(seed));

    /* initialize random seed: */
    srand (time(NULL));
    /* generate secret number between 1 and 10: */
    int nIndex = rand() % 5;
    //ui->lcdAvgNumber->updateLCDNumerColor();

    double dbAvg = RayMath::avg(m_vYAxisRawVal[enRawDataX].toStdVector());
    ui->lcdAvgNumber->setValue(dbAvg);
}

/**
 * @brief P2P  설정
 * @param dbP2P
 */
void AnalReportDlg::setP2P()
{
    //ui->lcdP2PNumber->updateLCDNumerColor();
    double dbP2P = RayMath::p2p(m_vYAxisRawVal[enRawDataX].toStdVector());
    ui->lcdP2PNumber->setValue(dbP2P);
}

/**
 * @brief RMS  설정
 * @param dbRMS
 */
void AnalReportDlg::setRMS()
{
    //ui->lcdRMSNumber->updateLCDNumerColor();
    double dbRMS = RayMath::rms(m_vYAxisRawVal[enRawDataX].toStdVector());
    ui->lcdRMSNumber->setValue(dbRMS);
}

/**
 * @brief 분산  설정
 * @param dbVal
 */
void AnalReportDlg::setVariance()
{    
    double dbVariance =RayMath::variance(m_vYAxisRawVal[enRawDataX].toStdVector());
    ui->ledVarianceWidget->setValue(dbVariance);
}

/**
 * @brief 표준 편차 설정
 * @param dbVal
 */
void AnalReportDlg::setSnD()
{
    double dbSnD = RayMath::stdev(m_vYAxisRawVal[enRawDataX].toStdVector());
    ui->ledSnDWidget->setValue(dbSnD);
}

/**
 * @brief 컨트롤 초기화
 */
void AnalReportDlg::clearAll()
{
    clearPlot();
    ui->lcdAvgNumber->setValue(0.0);
    ui->lcdP2PNumber->setValue(0.0);
    ui->lcdRMSNumber->setValue(0.0);
    ui->ledVarianceWidget->setValue(0.0);
    ui->ledSnDWidget->setValue(0.0);
}

/**
 * @brief customplot 클리어
 * @param nIdx
 */
void AnalReportDlg::clearPlot(int nIdx)
{
    Q_UNUSED(nIdx)

    ui->customPlotRaw->clearGraphs();
    ui->customPlotRaw->replot();

    ui->customPlotFFT->clearGraphs();
    ui->customPlotFFT->replot();

}

/**
 * @brief 모든 Frame 보기/숨기기
 * @param bShow
 */
void AnalReportDlg::showAllFrames(bool bShow)
{
    ui->customPlotRawFrame->setVisible(bShow);
    ui->customPlotFFTFrame->setVisible(bShow);
    ui->numRMSLCDFrame->setVisible(bShow);
    ui->numP2PLCDFrame->setVisible(bShow);
    ui->numAvgLCDFrame->setVisible(bShow);
    ui->ledVarianceFrame->setVisible(bShow);
    ui->ledSnDFrame->setVisible(bShow);
}

/**
 * @brief UI 디폴트 처리
 * @param uiAppliedAlgorithm
 */
void AnalReportDlg::setDefaultUI(quint32 uiAppliedAlgorithm)
{
    if((uiAppliedAlgorithm & 0x1)==0){//FFT
        ui->customPlotFFT->clearGraphs();
        ui->customPlotFFT->replot();
    }
    if((uiAppliedAlgorithm & 0x2)==0){//RMS
        ui->lcdRMSNumber->setValue(0.0);
    }
    if((uiAppliedAlgorithm & 0x4)==0){//P2P
        ui->lcdP2PNumber->setValue(0.0);
    }
    if((uiAppliedAlgorithm & 0x8)==0){//Average
        ui->lcdAvgNumber->setValue(0.0);
    }
    if((uiAppliedAlgorithm & 0x10)==0){//Variance
        ui->ledVarianceWidget->setValue(0.0);
    }
    if((uiAppliedAlgorithm & 0x11)==0){//SnD
        ui->ledSnDWidget->setValue(0.0);
    }
}


/**
 * @brief Raw 컨텍스트 메뉴 요청 Slot
 * @param pos
 */
void AnalReportDlg::slotRawContextMenuReq(QPoint pos)
{
    QMenu *menu = new QMenu(this);
    menu->setAttribute(Qt::WA_DeleteOnClose);
    menu->addAction("설정하기", this, SLOT(slotShowRawSetupDock()));
    menu->popup(ui->customPlotRaw->mapToGlobal(pos));
}


/**
 * @brief FFT 컨텍스트 메뉴 요청 Slot
 * @param pos
 */
void AnalReportDlg::slotFFTContextMenuReq(QPoint pos)
{
    QMenu *menu = new QMenu(this);
    menu->setAttribute(Qt::WA_DeleteOnClose);
    menu->addAction("설정하기", this, SLOT(slotShowFFTSetupDock()));
    menu->popup(ui->customPlotFFT->mapToGlobal(pos));
}

/**
 * @brief Avg LCD 컨택스트 메뉴 요청 Slot
 * @param pos
 */
void AnalReportDlg::slotAvgLCDNumContextMenuReq(QPoint pos)
{
    QMenu *menu = new QMenu(this);
    menu->setAttribute(Qt::WA_DeleteOnClose);
    menu->addAction("설정하기", this, SLOT(slotShowAvgLCDNumSetupDock()));
    menu->popup(ui->lcdAvgNumber->mapToGlobal(pos));
}

/**
 * @brief P2P LCD 컨텍스트 메뉴 요청 Slot
 * @param pos
 */
void AnalReportDlg::slotP2PLCDNumContextMenuReq(QPoint pos)
{
    QMenu *menu = new QMenu(this);
    menu->setAttribute(Qt::WA_DeleteOnClose);
    menu->addAction("설정하기", this, SLOT(slotShowP2PLCDNumSetupDock()));
    menu->popup(ui->lcdP2PNumber->mapToGlobal(pos));
}

/**
 * @brief RMS LCD 컨텍스트 메뉴 요청 Slot
 * @param pos
 */
void AnalReportDlg::slotRMSLCDNumContextMenuReq(QPoint pos)
{
    QMenu *menu = new QMenu(this);
    menu->setAttribute(Qt::WA_DeleteOnClose);
    menu->addAction("설정하기", this, SLOT(slotShowRMSLCDNumSetupDock()));
    menu->popup(ui->lcdRMSNumber->mapToGlobal(pos));
}

/**
 * @brief 분산 LED 컨텍스트 메뉴 요청 Slot
 * @param pos
 */
void AnalReportDlg::slotVarianceLEDContextMenuReq(QPoint pos)
{
    QMenu *menu = new QMenu(this);
    menu->setAttribute(Qt::WA_DeleteOnClose);
    menu->addAction("설정하기", this, SLOT(slotShowVarianceLEDSetupDock()));
    menu->popup(ui->ledVarianceWidget->mapToGlobal(pos));
}

/**
 * @brief 표준 편차 컨텍스트 메뉴 요청 Slot
 * @param pos
 */
void AnalReportDlg::slotSnDLEDContextMenuReq(QPoint pos)
{
    QMenu *menu = new QMenu(this);
    menu->setAttribute(Qt::WA_DeleteOnClose);
    menu->addAction("설정하기", this, SLOT(slotShowSnDLEDSetupDock()));
    menu->popup(ui->ledSnDWidget->mapToGlobal(pos));
}


/**
 * @brief Raw 속성 설정 도킹창 보기 Slot
 */
void AnalReportDlg::slotShowRawSetupDock()
{
    MainWindow* main = qobject_cast<MainWindow*>(parent());
    main->showRawSetupDock(true);

}

/**
 * @brief FFT 속성 설정 도킹창 보기 Slot
 */
void AnalReportDlg::slotShowFFTSetupDock()
{
    MainWindow* main = qobject_cast<MainWindow*>(parent());
    main->showFFTSetupDock(true);

}

/**
 * @brief Avg LCD 속성 설정 도킹창 보기 Slot
 */
void AnalReportDlg::slotShowAvgLCDNumSetupDock()
{
    MainWindow* main = qobject_cast<MainWindow*>(parent());
    main->showLCDNumSetupDock(0,true);
}

/**
 * @brief P2P LCD 속성 설정 도킹창 보기 Slot
 */
void AnalReportDlg::slotShowP2PLCDNumSetupDock()
{
    MainWindow* main = qobject_cast<MainWindow*>(parent());
    main->showLCDNumSetupDock(1,true);
}

/**
 * @brief RMS LCD 속성 설정 도킹창 보기 Slot
 */
void AnalReportDlg::slotShowRMSLCDNumSetupDock()
{
    MainWindow* main = qobject_cast<MainWindow*>(parent());
    main->showLCDNumSetupDock(2,true);
}

/**
 * @brief 분산 LED 속성 설정 도킹창 보기 Slot
 */
void AnalReportDlg::slotShowVarianceLEDSetupDock()
{
    MainWindow* main = qobject_cast<MainWindow*>(parent());
    main->showLEDSetupDock(0,true);
}

/**
 * @brief 표준 편차 속성 설정 도킹창 보기 Slot
 */
void AnalReportDlg::slotShowSnDLEDSetupDock()
{
    MainWindow* main = qobject_cast<MainWindow*>(parent());
    main->showLEDSetupDock(1,true);
}

void AnalReportDlg::saveRawDataToFile()
{
    QString filters("Text files (*.txt);;All files (*.*)");
    QString defaultFilter("Text files (*.txt)");
    QTextCodec *textCodec = QTextCodec::codecForName("eucKR");
    QString strTitle = "저장하기";
    strTitle = textCodec->toUnicode(strTitle.toLocal8Bit());

    QString strSaveFileName = QFileDialog::getSaveFileName(0, strTitle, QDir::currentPath(), filters, &defaultFilter);
    if(!strSaveFileName.isEmpty()){
        writeRawDataToFile(ui->customPlotRaw,strSaveFileName);
    }

}


void AnalReportDlg::writeRawDataToFile(QCustomPlot* customplot,QString strSaveFileName)
{
    int nGraphCnt = customplot->graphCount();
    if(nGraphCnt>0){
        QStringList strlistFileName  = strSaveFileName.split(".txt");
        QString strFileExt  = ".txt";
        for(int i=0; i<nGraphCnt;i++){
            QString strWriteFileName = QString("%1_%2.%3").arg(strlistFileName.at(0)).arg(i).arg(strFileExt);
            QFile file(strWriteFileName);
            if(file.open(QIODevice::WriteOnly)){
                QString strKeyVal="0.0";
                QString strVal="0.0";
                QSharedPointer<QCPGraphDataContainer> data =ui->customPlotRaw->graph(i)->data();
                if(data){
                    QTextStream out(&file);
                    QVector<QCPGraphData>::iterator it = data->begin();
                    const QVector<QCPGraphData>::iterator itEnd = data->end();
                    int i = 0;
                    QDateTime dateTime;
                    qint64 qiMSec = 0;
                    while (it != itEnd) {
                        qiMSec = (it->key)*1000;
                        dateTime = QDateTime::fromMSecsSinceEpoch(qiMSec);
                        dateTime = dateTime.addSecs(-9*60*60);
                        //strKeyVal.sprintf("%.3f %.18f",it->key,it->value);
                        strKeyVal.sprintf("%s %.18f",dateTime.toString("yyyyMMdd_HHmmss.zzz").toLocal8Bit().constData(),it->value);
                        out << strKeyVal << "\r\n";
                        ++it;
                        ++i;
                    }
                }
            }
            file.close();
        }
    }
}
/**
 * @brief FFT 데이터 파일로 저장
 */
void AnalReportDlg::saveFFTDataToFile()
{
    QString filters("Text files (*.txt);;All files (*.*)");
    QString defaultFilter("Text files (*.txt)");
    QTextCodec *textCodec = QTextCodec::codecForName("eucKR");
    QString strTitle = "저장하기";
    strTitle = textCodec->toUnicode(strTitle.toLocal8Bit());

    QString strSaveFileName = QFileDialog::getSaveFileName(0, strTitle, QDir::currentPath(), filters, &defaultFilter);
    if(!strSaveFileName.isEmpty()){
        writeFFTDataToFile(ui->customPlotFFT,strSaveFileName);
    }
}

void AnalReportDlg::writeFFTDataToFile(QCustomPlot* customplot,QString strSaveFileName)
{
    int nGraphCnt = customplot->graphCount();
    if(nGraphCnt>0){
        QStringList strlistFileName  = strSaveFileName.split(".txt");
        QString strFileExt  = ".txt";
        for(int i=0; i<nGraphCnt;i++){
            QString strWriteFileName = QString("%1_%2.%3").arg(strlistFileName.at(0)).arg(i).arg(strFileExt);
            QFile file(strWriteFileName);
            if(file.open(QIODevice::WriteOnly)){
                QString strKeyVal="0.0";
                QString strVal="0.0";
                QSharedPointer<QCPGraphDataContainer> data =ui->customPlotRaw->graph(i)->data();
                if(data){
                    QTextStream out(&file);
                    QVector<QCPGraphData>::iterator it = data->begin();
                    const QVector<QCPGraphData>::iterator itEnd = data->end();
                    int i = 0;
                    int nSize = data.data()->size() -1;
                    while (it != itEnd) {
                        if(nSize==i){
                           strKeyVal.sprintf("%.18f",it->value);
                        }else{
                            strKeyVal.sprintf("%.18f,",it->value);
                        }
                        out << strKeyVal;
                        ++it;
                        ++i;
                    }
                }
            }
            file.close();
        }
    }
}
/**
 * @brief 컨트롤 속성 아이템 설정
 * @param options
 */
void AnalReportDlg::setControlOptions(ControlOptions& options)
{
    MapControlOtpions::iterator it = m_mapControlOptions.find(options.id());
    if(it == m_mapControlOptions.end()){
        m_mapControlOptions.insert(PairControlOptions(options.id(),options));
    }
    updateControl(options);
}

/**
 * @brief 컨트롤 업데이트
 * @param options
 */
void AnalReportDlg::updateControl(ControlOptions& options)
{
    QString strID = options.id();
    if(strID.compare(DEF_OF_FFTPLOT)==0){
        ui->customPlotFFT->replot();
    }else if(strID.compare(DEF_OF_RAWPLOT)==0){
        ui->customPlotRaw->replot();
    }else if(strID.compare(DEF_OF_RMSLCDNUM)==0){
        ui->lcdRMSNumber->setUseUpperLimit(options.useUpperLimit());
        ui->lcdRMSNumber->setUpperLimit(options.upperLimit());
        ui->lcdRMSNumber->setUseLowerLimit(options.useLowerLimit());
        ui->lcdRMSNumber->setLowerLimit(options.lowerLimit());
        ui->lcdRMSNumber->updateLCDNumerColor();
    }else if(strID.compare(DEF_OF_P2PLCDNUM)==0){
        ui->lcdP2PNumber->setUseUpperLimit(options.useUpperLimit());
        ui->lcdP2PNumber->setUpperLimit(options.upperLimit());
        ui->lcdP2PNumber->setUseLowerLimit(options.useLowerLimit());
        ui->lcdP2PNumber->setLowerLimit(options.lowerLimit());
        ui->lcdP2PNumber->updateLCDNumerColor();
    }else if(strID.compare(DEF_OF_AVGLCDNUM)==0){
        ui->lcdAvgNumber->setUseUpperLimit(options.useUpperLimit());
        ui->lcdAvgNumber->setUpperLimit(options.upperLimit());
        ui->lcdAvgNumber->setUseLowerLimit(options.useLowerLimit());
        ui->lcdAvgNumber->setLowerLimit(options.lowerLimit());
        ui->lcdAvgNumber->updateLCDNumerColor();
    }else if(strID.compare(DEF_OF_VARIANCELED)==0){
        ui->ledVarianceWidget->setUseUpperLimit(options.useUpperLimit());
        ui->ledVarianceWidget->setUpperLimit(options.upperLimit());
        ui->ledVarianceWidget->setUseLowerLimit(options.useLowerLimit());
        ui->ledVarianceWidget->setLowerLimit(options.lowerLimit());
        ui->ledVarianceWidget->updateLEDColor();
    }else if(strID.compare(DEF_OF_STDEVLED)==0){
        ui->ledSnDWidget->setUseUpperLimit(options.useUpperLimit());
        ui->ledSnDWidget->setUpperLimit(options.upperLimit());
        ui->ledSnDWidget->setUseLowerLimit(options.useLowerLimit());
        ui->ledSnDWidget->setLowerLimit(options.lowerLimit());
        ui->ledSnDWidget->updateLEDColor();
    }else{
    }

}

void AnalReportDlg::loadAnalysisData(QSqlDatabase& m_dbMySQL, QString strContainerName, QString strStart,QString strEnd)
{

    QApplication::setOverrideCursor(Qt::WaitCursor);
    m_dbYMax = std::numeric_limits<double>::lowest();
    m_dbYMin = std::numeric_limits<double>::max();
    m_dbFFTYMax = std::numeric_limits<double>::lowest();
    m_dbFFTYMin = std::numeric_limits<double>::max();

    m_vXAxisRawVal[AnalReportDlg::enRawData].clear();
    m_vYAxisRawVal[AnalReportDlg::enRawData].clear();
    m_vYAxisRawVal[AnalReportDlg::enRawDataX].clear();
    m_vYAxisRawVal[AnalReportDlg::enRawDataY].clear();
    m_vYAxisRawVal[AnalReportDlg::enRawDataZ].clear();



    if(!strContainerName.isEmpty()){

        QString strReqStartDate = strStart;// m_pSearchSettingsDock->analSettingsItem().reqStartDate();//
        QString strReqEndDate   = strEnd;//m_pSearchSettingsDock->analSettingsItem().reqEndDate();

        QStringList strlistRI = strContainerName.split('/');
        QString strSQLQuery = "";
        int nRICnt = strlistRI.count();
        if(nRICnt > 4){


            QString strRI = strlistRI[nRICnt-1];
            QString strSection = strRI.section("raw",0,0);
            strSQLQuery = "CALL analysisRangeRaw2('" + strContainerName + "' , '" + strReqStartDate + "' , '" + strReqEndDate +"')";

            qDebug() << "CALL analysisRangeRaw2 : " << strSQLQuery << endl;

            if(!strSQLQuery.isEmpty()){
                if (!m_dbMySQL.open()){
                    QMessageBox::critical(0, QObject::tr("Database Error"), m_dbMySQL.lastError().text());
                }else{
                    double dbInterval = 0.0;
                    int    nContentsSize = 0;

                    QSqlQuery query(strSQLQuery);
                    //query.record().count());
                    //query.size());

                    int index=0;
                    double dbX = 0, dbY =0;
                    QString strContents = "";

                    while (query.next()){
                        strContents = query.value(3).toString();
                        QStringList strlistContensts = strContents.split('T');
                        if(strlistContensts.count() ==2){
                            QString strDateTime  = strlistContensts.at(0);
                            QStringList strlistDateTime = strDateTime.split(' ');
                            double dbDiff =0.0;
                            if(strlistDateTime.count() == 2){
                                dbX = strlistDateTime[0].toDouble() + (9*60*60);//+9시간 1479939172.68
                                dbDiff = strlistDateTime[1].toDouble() - strlistDateTime[0].toDouble();

                                QString strRawData   = strlistContensts.at(1);
                                QStringList strlistRawData = strRawData.split('@');
                                int nRawDataCnt = strlistRawData.count();
                                dbInterval = dbDiff/nRawDataCnt;
                                QString strTemp ="";
                                QStringList strlistTemp;
                                for(int i=0; i< nRawDataCnt;i++){
                                    strTemp = strlistRawData.at(i);
                                    strlistTemp = strTemp.split(' ');
                                    if(strlistTemp.count()==3){
                                        m_vXAxisRawVal[enRawData].push_back(dbX);
                                        for(int j=0; j<3;j++){
                                            if(m_dbYMax < strlistTemp[j].toDouble()){
                                                m_dbYMax = strlistTemp[j].toDouble();
                                            }
                                            if(m_dbYMin > strlistTemp[j].toDouble()){
                                                m_dbYMin = strlistTemp[j].toDouble();
                                            }
                                        }
                                        m_vYAxisRawVal[enRawDataX].push_back(strlistTemp[0].toDouble());
                                        m_vYAxisRawVal[enRawDataY].push_back(strlistTemp[1].toDouble());
                                        m_vYAxisRawVal[enRawDataZ].push_back(strlistTemp[2].toDouble());
                                    }
                                    dbX += dbInterval;
                                }
                            }
                        }
                        index++;
                    }
                    int nYDataSize= m_vYAxisRawVal[enRawDataX].size();
                    m_arrFFTData[enRawDataX].resize(nYDataSize);
                    m_arrFFTData[enRawDataY].resize(nYDataSize);
                    m_arrFFTData[enRawDataZ].resize(nYDataSize);
                    std::complex<double> complex = 0.0;
                    for(int i=0; i<nYDataSize; i++){                        
                        complex = m_vYAxisRawVal[enRawDataX][i];
                        m_arrFFTData[enRawDataX][i] = complex;
                        complex = m_vYAxisRawVal[enRawDataY][i];
                        m_arrFFTData[enRawDataY][i] = complex;
                        complex = m_vYAxisRawVal[enRawDataZ][i];
                        m_arrFFTData[enRawDataZ][i] = complex;
                    }
                }
                m_dbMySQL.close();
            }else{
                QApplication::setOverrideCursor(Qt::ArrowCursor);
                QMessageBox::information(this,"알림","해당 컨테이너는 질의 할 데이터가 존재하지 않습니다." );
            }
        }else{
            QApplication::setOverrideCursor(Qt::ArrowCursor);
            QMessageBox::information(this,"알림","해당 컨테이너는 데이터 분석에 해당되지 않습니다." );
        }
    }else{
        QMessageBox::information(this,"알림","컨테이너를 선택하지 않았습니다" );
    }
    QApplication::setOverrideCursor(Qt::ArrowCursor);
}

void AnalReportDlg::loadAnalysisFixData(QSqlDatabase& m_dbMySQL, QString strContainerName, QString strStart,QString strEnd,double& dbMax, double& dbMin)
{
    QApplication::setOverrideCursor(Qt::WaitCursor);
    dbMax = std::numeric_limits<double>::lowest();
    dbMin = std::numeric_limits<double>::max();

    m_vXAxisRawVal[AnalReportDlg::enFixRawData].clear();
    m_vYAxisRawVal[AnalReportDlg::enFixRawData].clear();
    m_vYAxisRawVal[AnalReportDlg::enFixRawDataX].clear();
    m_vYAxisRawVal[AnalReportDlg::enFixRawDataY].clear();
    m_vYAxisRawVal[AnalReportDlg::enFixRawDataZ].clear();


    if(!strContainerName.isEmpty()){
        QString strReqStartDate = strStart;// m_pSearchSettingsDock->analSettingsItem().reqStartDate();//
        QString strReqEndDate   = strEnd;//m_pSearchSettingsDock->analSettingsItem().reqEndDate();

        QStringList strlistRI = strContainerName.split('/');
        QString strSQLQuery = "";
        int nRICnt = strlistRI.count();
        if(nRICnt > 4){
            QString strRI = strlistRI[nRICnt-1];
            QString strSection = strRI.section("raw",0,0);
            strSQLQuery = "CALL analysisRangeRaw2('" + strContainerName + "' , '" + strReqStartDate + "' , '" + strReqEndDate +"')";

            if(!strSQLQuery.isEmpty()){
                if (!m_dbMySQL.open()){
                    QMessageBox::critical(0, QObject::tr("Database Error"), m_dbMySQL.lastError().text());
                }else{
                    double dbInterval = 0.0;
                    int    nContentsSize = 0;

                    QSqlQuery query(strSQLQuery);
                    //query.record().count());
                    //query.size());

                    int index=0;
                    double dbX = 0, dbY =0;
                    QString strContents = "";

                    while (query.next()){
                        strContents = query.value(3).toString();
                        QStringList strlistContensts = strContents.split('T');
                        if(strlistContensts.count() ==2){
                            QString strDateTime  = strlistContensts.at(0);
                            QStringList strlistDateTime = strDateTime.split(' ');
                            double dbDiff =0.0;
                            if(strlistDateTime.count() == 2){
                                dbX = strlistDateTime[0].toDouble() + (9*60*60);//+9시간 1479939172.68
                                dbDiff = strlistDateTime[1].toDouble() - strlistDateTime[0].toDouble();

                                QString strRawData   = strlistContensts.at(1);
                                QStringList strlistRawData = strRawData.split('@');
                                int nRawDataCnt = strlistRawData.count();
                                dbInterval = dbDiff/nRawDataCnt;
                                QString strTemp ="";
                                QStringList strlistTemp;
                                for(int i=0; i< nRawDataCnt;i++){
                                    strTemp = strlistRawData.at(i);
                                    strlistTemp = strTemp.split(' ');
                                    if(strlistTemp.count()==3){
                                        for(int j=0; j<3;j++){
                                            if(dbMax < strlistTemp[j].toDouble()){
                                                dbMax = strlistTemp[j].toDouble();
                                            }
                                            if(dbMin > strlistTemp[j].toDouble()){
                                                dbMin = strlistTemp[j].toDouble();
                                            }
                                        }
                                        m_vXAxisRawVal[AnalReportDlg::enFixRawData].push_back(dbX);
                                        m_vYAxisRawVal[AnalReportDlg::enFixRawDataX].push_back(strlistTemp[0].toDouble());
                                        m_vYAxisRawVal[AnalReportDlg::enFixRawDataY].push_back(strlistTemp[1].toDouble());
                                        m_vYAxisRawVal[AnalReportDlg::enFixRawDataZ].push_back(strlistTemp[2].toDouble());
                                    }
                                    dbX += dbInterval;
                                }
                            }
                        }
                        index++;
                    }
                }
                m_dbMySQL.close();
            }else{
                QApplication::setOverrideCursor(Qt::ArrowCursor);
                QMessageBox::information(this,"알림","해당 컨테이너는 질의 할 데이터가 존재하지 않습니다." );
            }
        }else{
            QApplication::setOverrideCursor(Qt::ArrowCursor);
            QMessageBox::information(this,"알림","해당 컨테이너는 데이터 분석에 해당되지 않습니다." );
        }
    }else{
        QMessageBox::information(this,"알림","컨테이너를 선택하지 않았습니다" );
    }
    QApplication::setOverrideCursor(Qt::ArrowCursor);
}
