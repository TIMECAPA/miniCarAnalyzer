#ifndef FFTSETUPDOCK_H
#define FFTSETUPDOCK_H

#include <QDockWidget>
#include "mainwindow.h"

namespace Ui {
class FFTSetupDock;
}

/**
 * @brief FFT 설정 도킹창 클래스
 */
class FFTSetupDock : public QDockWidget
{
    Q_OBJECT
    Q_PROPERTY(QString id READ id WRITE setID)
public:
    explicit FFTSetupDock(QWidget *parent = 0);
    ~FFTSetupDock();
private:
    Ui::FFTSetupDock *ui;
    /**
     * @brief 메인 인도우 포인트
     */
    MainWindow*         m_pMainWindow;
    /**
     * @brief ID
     */
    QString m_strID;
public:
    void setID(QString strID);
    QString id() const;
private slots:
    void on_btnSaveDataToFile_clicked();
    void on_chkShowGraphX_clicked();
    void on_chkShowGraphY_clicked();
    void on_chkShowGraphZ_clicked();
};

#endif // FFTSETUPDOCK_H
