/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#define DEF_OF_DB_IPADDR "garigulls.com"
#define DEF_OF_DB        "QMYSQL"
#define DEF_OF_DB_NAME   "mobiusdb3"
#define DEF_OF_DB_USER   "root"
#define DEF_OF_DB_PW     "soft0987"

// #define DEF_OF_PERIOD_INTERVAL 60*1000  // Sec
#define DEF_OF_PERIOD_INTERVAL 60*10  // Sec
// #define DEF_OF_PERIOD_DATA_GAP 60         //Sec
#define DEF_OF_PERIOD_DATA_GAP 60       //Sec

#include <QMainWindow>
#include <QWidget>
#include <QTimer>
#include <QTcpSocket>
#include <QAbstractSocket>
#include <QSqlDatabase>

#include "raymath.h"

QT_BEGIN_NAMESPACE
class QAction;
class QListWidget;
class QMenu;
class QTextEdit;
class QImage;
class QLabel;
QT_END_NAMESPACE

class QCustomPlot;
class AnalReportDlg;

class SensorListDock;
class AnalysisSetupDock;
class RawAnalysisSetupDock;
class FFTSetupDock;
class LCDNumSetupDock;
class LEDSetupDock;
class QComboBox;

#define DEF_OF_FFTPLOT      "FFTPlot"
#define DEF_OF_RAWPLOT      "RawPlot"
#define DEF_OF_RMSLCDNUM    "RMSLCDNum"
#define DEF_OF_P2PLCDNUM    "P2PLCDNum"
#define DEF_OF_AVGLCDNUM    "AvgLCDNum"
#define DEF_OF_VARIANCELED  "VarianceLED"
#define DEF_OF_STDEVLED     "SnDLED"

/**
 * @brief LCD LED 속성 아이템 클래스
 */
class ControlOptions{
private:
    /**
     * @brief ID
     */
    QString m_strID;

    /**
     * @brief 상한 값 사용 상태
     */
    bool m_bUseUpperLimit;

    /**
     * @brief 상한 값
     */
    double m_dbUpperLimit;

    /**
     * @brief 하한 값 사용 상태
     */
    bool m_bUseLowerLimit;

    /**
     * @brief 하한 값
     */
    double m_dbLowerLimit;
public:
    /**
     * @brief ID 설정
     * @param strID
     */
    void setID(QString strID) { m_strID = strID;}

    /**
     * @brief 상한 값 사용 설정
     * @param bUse
     */
    void setUseUpperLimit(bool bUse){ m_bUseUpperLimit = bUse;}

    /**
     * @brief 상한 값 설정
     * @param dbVal
     */
    void setUpperLimit(double dbVal){ m_dbUpperLimit   = dbVal;}

    /**
     * @brief 하한 값 사용 설정
     * @param bUse
     */
    void setUseLowerLimit(bool bUse){ m_bUseLowerLimit = bUse;}

    /**
     * @brief 하한 값 설정
     * @param dbVal
     */
    void setLowerLimit(double dbVal){ m_dbLowerLimit   = dbVal;}

    /**
     * @brief ID 가져오기
     * @return
     */
    QString id() { return m_strID; }

    /**
     * @brief 상한 값 사용 상태
     * @return
     */
    bool useUpperLimit() const { return m_bUseUpperLimit;}

    /**
     * @brief 상한 값 가져오기
     * @return
     */
    double upperLimit() const { return m_dbUpperLimit;}

    /**
     * @brief 하한 값 사용 상태
     * @return
     */
    bool useLowerLimit() const { return m_bUseLowerLimit;}

    /**
     * @brief 하한 값 가져오기
     * @return
     */
    double lowerLimit() const { return m_dbLowerLimit;}
};

/**
 * @brief 메인 윈도우 클래스
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT
    Q_ENUMS(ExportPen)
public:
    MainWindow();    
private slots:    
    void about();
    void slotShowAnalysisSetupDock();
    void slotExecAnalSearchCond();
    void slotExecAnalData();
    void slotPauseAnalData();
    void slotStopAnalData();
public slots:
    void slotPreMinMaxDockSizes(QDockWidget*);
    void slotUpdateAnalysisReport();
private:
    void createActions();
    void createStatusBar();
    void createDockWindows();
private:    
    /**
     * @brief 보기 메뉴
     */
    QMenu*              m_pMenuView;

    /**
     * @brief 검색 조건 실행
     */
    QAction*            m_pactExecSearchCond;

    /**
     * @brief 분석 데이터 실행
     */
    QAction*            m_pactExec;

    /**
     * @brief 분석 데이터 일시정지
     */
    QAction*            m_pactPause;

    /**
     * @brief 분석 데이터 정지
     */
    QAction*            m_pactStop;

    /**
     * @brief 분석 컨테이너(센서) 선택 도킹 창
     */
    AnalysisSetupDock*  m_pAnalysisSetupDock;

    /**
     * @brief 분석보고서 다이얼로그
     */
    AnalReportDlg*      m_pAnalreportdlg;
    //AnalReportDlg*      m_pAnalreportdlg2;

    /**
     * @brief 센서 리스트 도킹 창
     */
    SensorListDock*     m_pSensorListDock;

    /**
     * @brief FFT 속성 설정 도킹 창
     */
    RawAnalysisSetupDock*  m_pRawAnalysisSetupDock;

    /**
     * @brief FFT 속성 설정 도킹 창
     */
    FFTSetupDock*       m_pFFTSetupDock;

    /**
     * @brief Avg LCD 속성 설정 도킹 창
     */
    LCDNumSetupDock*    m_pAvgLCDNumSetupDock;

    /**
     * @brief P2P LCD 속성 설정 도킹 창
     */
    LCDNumSetupDock*    m_pP2PLCDNumSetupDock;

    /**
     * @brief RMS LCD 속성 설정 도킹 창
     */
    LCDNumSetupDock*    m_pRMSLCDNumSetupDock;

    /**
     * @brief 분산 LED 속성 설정 도킹 창
     */
    LEDSetupDock*       m_pVarianceLEDSetupDock;

    /**
     * @brief 표준 편차 속성 설정 도킹 창
     */
    LEDSetupDock*       m_pSnDLEDSetupDock;

    /**
     * @brief 도킹 창 이전 Max 사이즈
     */
    QSize               m_sizePreMax;

    /**
     * @brief  도킹 창 이전 Min 사이즈
     */
    QSize               m_sizePreMin;
private:
    /**
     * @brief 데이터 최대 값
     */
    double              m_dbYMax;

    /**
     * @brief 데이터 최소 값
     */
    double              m_dbYMin;

    /**
     * @brief 데이터베이스 인스턴스
     */
    QSqlDatabase        m_dbMySQL;

    /**
     * @brief 분석 타이머
     */
    QTimer              m_timerDataAnalysis;

    /**
     * @brief 검색 설정 툴바
     */
    QToolBar            *m_toolbarSearchSettings;

    /**
     * @brief 검색 조건 툴바
     */
    QToolBar            *m_toolbarSearchCond;

    /**
     * @brief 센서 풀 네임
     */
    QString             m_strFullSensorName;

    /**
     * @brief 비교 센서 풀 네임
     */
    QString             m_strCompFullSensorName;

    /**
     * @brief FFT 데이터
     */
    CArray              m_arrFFTData;
private:
    void readSettings(QDockWidget* dock);
    void writeSettings(QDockWidget* dock);
private:
    void setDockSize(QDockWidget *dock, int, int);
    void execAnalysis(QString strStart,QString strEnd);
    void processRaw();
    void processFFT();
    void processRMS();
    void processP2P();
    void processAvg();
    void processVariance();
    void processSnD();
public:
    void setAnalysisReportConnInfo(QString strFullSensorName);
    void processAnalysis(QString strStart,QString strEnd);
    void processAnalysis(QString strContainerName, QString strStart,QString strEnd,double& dbMax, double& dbMin);
    void updateSensorListDock(QString strContainser, QString strAlgorithm);
    void showRawSetupDock(bool bShow=true);
    void showFFTSetupDock(bool bShow=true);
    void showLCDNumSetupDock(int nIndex = -1, bool bShow=true);
    void showLEDSetupDock(int nIndex = -1,bool bShow=true);
public:
    void saveRawDataToFile();
    void setVisibleRawGraph(int nMode =0, bool bShow = true);

    void saveFFTDataToFile();
    void setVisibleFFTGraph(int nMode =0, bool bShow = true);

    void setControlOptions(ControlOptions& options);
    void doDemoFFT();
    void doDemoSelfFFT();

private:
    bool isPowerOf2(int n);

public:
    QSqlDatabase* getSqlDatabase() { return &m_dbMySQL; };
};


#endif
