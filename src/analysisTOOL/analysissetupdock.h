#ifndef ANALYSISSETUPDOCK_H
#define ANALYSISSETUPDOCK_H

#include <QDockWidget>

class QSqlDatabase;

namespace Ui {
class AnalysisSetupDock;
}

/**
 * @brief 분석 컨테이너(센서) 설정 도킹창 클래스
 */
class AnalysisSetupDock : public QDockWidget
{
    Q_OBJECT
public:
    explicit AnalysisSetupDock(QWidget *parent = 0);
    ~AnalysisSetupDock();
private slots:
    void on_btnAdd_clicked();
    void on_btnAllAdd_clicked();

private:
    Ui::AnalysisSetupDock *ui;
public:
    void addData(QSqlDatabase& dbMySQL,QString query);
};

#endif // ANALYSISSETUPDOCK_H
