#include "fftsetupdock.h"
#include "ui_fftsetupdock.h"

/**
 * @brief 생성자
 * @param parent
 */
FFTSetupDock::FFTSetupDock(QWidget *parent) :
    QDockWidget(parent),
    ui(new Ui::FFTSetupDock)
{
    ui->setupUi(this);
    m_pMainWindow = qobject_cast<MainWindow*>(parent);

    m_strID = "";
}



/**
 * @brief 소멸자
 */
FFTSetupDock::~FFTSetupDock()
{
    delete ui;
}

/**
 * @brief ID 설정
 * @param strID
 */
void FFTSetupDock::setID(QString strID)
{
    m_strID = strID;
}

/**
 * @brief ID 가져오기
 * @return
 */
QString FFTSetupDock::id() const
{
    return m_strID;
}

/**
 * @brief FFT 데이터 저장 클릭 이벤트
 */
void FFTSetupDock::on_btnSaveDataToFile_clicked()
{
    m_pMainWindow->saveFFTDataToFile();
}

void FFTSetupDock::on_chkShowGraphX_clicked()
{
    Qt::CheckState chkState = ui->chkShowGraphX->checkState();
    bool bShow = false;
    chkState == Qt::Checked ? bShow = true : bShow = false;
    m_pMainWindow->setVisibleFFTGraph(0,bShow);
}

void FFTSetupDock::on_chkShowGraphY_clicked()
{
    Qt::CheckState chkState = ui->chkShowGraphY->checkState();
    bool bShow = false;
    chkState == Qt::Checked ? bShow = true : bShow = false;
    m_pMainWindow->setVisibleFFTGraph(1,bShow);
}

void FFTSetupDock::on_chkShowGraphZ_clicked()
{
    Qt::CheckState chkState = ui->chkShowGraphZ->checkState();
    bool bShow = false;
    chkState == Qt::Checked ? bShow = true : bShow = false;
    m_pMainWindow->setVisibleFFTGraph(2,bShow);
}
