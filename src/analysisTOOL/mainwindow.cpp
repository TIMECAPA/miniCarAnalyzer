/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

//! [0]
#include <QtWidgets>
#ifndef QT_NO_PRINTDIALOG
#include <QtPrintSupport>
#endif

#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QImage>

#include "mainwindow.h"
#include "qcustomplot.h"
#include "dndtoolbar.h"
#include "dndtoolbutton.h"
#include "analreportdlg.h"
#include "sensorlistdock.h"
#include "rawanalysissetupdock.h"
#include "fftsetupdock.h"
#include "lcdnumsetupdock.h"
#include "ledsetupdock.h"
#include "analysissetupdock.h"

/**
 * @brief 생성자
 */
MainWindow::MainWindow()
{

    m_pAnalysisSetupDock = NULL;
    m_pAnalreportdlg     = NULL;
    //m_pAnalreportdlg2    = NULL;
    m_pSensorListDock    = NULL;

    m_pRawAnalysisSetupDock = NULL;
    m_pFFTSetupDock      = NULL;
    m_pAvgLCDNumSetupDock= NULL;
    m_pP2PLCDNumSetupDock= NULL;
    m_pRMSLCDNumSetupDock= NULL;
    m_pVarianceLEDSetupDock= NULL;
    m_pSnDLEDSetupDock= NULL;

    m_dbYMax = std::numeric_limits<double>::lowest();
    m_dbYMin = std::numeric_limits<double>::max();


    m_pAnalreportdlg = new AnalReportDlg(this);
    m_pAnalreportdlg->setWindowFlags(Qt::SubWindow);
    this->setCentralWidget(m_pAnalreportdlg);

    //m_pAnalreportdlg2 = new AnalReportDlg(this);
    //m_pAnalreportdlg2->setWindowFlags(Qt::SubWindow);
    //m_pAnalreportdlg2->setVisible(true);

    m_dbMySQL = QSqlDatabase::addDatabase(DEF_OF_DB);
    m_dbMySQL.setHostName(DEF_OF_DB_IPADDR);
    m_dbMySQL.setDatabaseName(DEF_OF_DB_NAME);
    m_dbMySQL.setUserName(DEF_OF_DB_USER);
    m_dbMySQL.setPassword(DEF_OF_DB_PW);

    createActions();
    createStatusBar();
    createDockWindows();


    setWindowTitle(tr("miniCarAnalyzer"));
    setUnifiedTitleAndToolBarOnMac(true);

    connect(&m_timerDataAnalysis, SIGNAL(timeout()), this, SLOT(slotUpdateAnalysisReport()));

    doDemoFFT();
    doDemoSelfFFT();
    doDemoSelfFFT();

    m_strCompFullSensorName =  "/mobius-yt/bridgea/smartcs_soc03";
}

/**
 * @brief FFT 데모
 */
void MainWindow::doDemoFFT()
{
    const Complex test[] = { 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0 };
    CArray data(test, 8);

    // forward fft
    RayMath::fft(data);
    for (int i = 0; i < 8; ++i)
    {
       std::cout << data[i] << std::endl;
    }

}

/**
 * @brief FFT Self 데모
 */
void MainWindow::doDemoSelfFFT()
{
    QVector<double>    vRawData;
    vRawData.push_back(0.0);
    vRawData.push_back(1.0);
    vRawData.push_back(0.0);
    vRawData.push_back(-1.0);
    vRawData.push_back(-0.5);
    vRawData.push_back(-1.0);
    vRawData.push_back(0.0);
    vRawData.push_back(1.0);

    int nYDataSize= vRawData.size();
    m_arrFFTData.resize(nYDataSize);
    std::complex<double> complex = 0.0;
    for(int i=0; i<nYDataSize; i++){
        complex = vRawData[i];
        m_arrFFTData[i] = complex;
    }
    RayMath::fft(m_arrFFTData);
    std::cout << "fft" << std::endl;
    for (int i = 0; i < nYDataSize; ++i)
    {
        complex = m_arrFFTData[i];
        std::cout << m_arrFFTData[i] << std::endl;        
    }
}

/**
 * @brief 2의 승수 판단
 * @param n
 * @return
 */
bool MainWindow::isPowerOf2(int n)
{
    int nRef = 1;
    while(nRef <n){
        nRef <<=1;
    }
    if(nRef==n){
        return true;
    }else{
        return false;
    }
}

/**
 * @brief About
 */
void MainWindow::about()
{
   QMessageBox::about(this, tr("analysisTool"),
            tr("The <b>Dock Widgets</b> example demonstrates how to "
               "use Qt's dock widgets. You can enter your own text, "
               "click a customer to add a customer name and "
               "address, and click standard paragraphs to add them."));
}

/**
 * @brief 메뉴 및  Action 연결
 */
void MainWindow::createActions()
{    
    QMenu *fileMenu = menuBar()->addMenu(tr("&File"));
    QAction *quitAct = fileMenu->addAction(tr("&Quit"), this, &QWidget::close);
    quitAct->setShortcuts(QKeySequence::Quit);
    quitAct->setStatusTip(tr("Quit the application"));

    m_pMenuView = menuBar()->addMenu(tr("&View"));
    menuBar()->addSeparator();

    QMenu *helpMenu = menuBar()->addMenu(tr("&Help"));
    QAction *aboutAct = helpMenu->addAction(tr("&About"), this, &MainWindow::about);
    aboutAct->setStatusTip(tr("Show the application's About box"));
    QAction *aboutQtAct = helpMenu->addAction(tr("About &Qt"), qApp, &QApplication::aboutQt);
    aboutQtAct->setStatusTip(tr("Show the Qt library's About box"));

    //DnDToolbar 추가
    QToolBar *dndPlotToolBar = new QToolBar(tr("Plot"),this);
    addToolBar(dndPlotToolBar);

    DnDToolButton *dndRawPlotToolButton = new DnDToolButton(DEF_OF_RAWPLOT);
    dndRawPlotToolButton->setIcon(QIcon(":/images/raw_plot.png"));
    dndPlotToolBar->addWidget(dndRawPlotToolButton);

    DnDToolButton *dndFFTPlotToolButton = new DnDToolButton(DEF_OF_FFTPLOT);
    dndFFTPlotToolButton->setIcon(QIcon(":/images/fft_plot.png"));
    dndPlotToolBar->addWidget(dndFFTPlotToolButton);

    DnDToolButton *dndRMSLCDNumToolButton = new DnDToolButton(DEF_OF_RMSLCDNUM);
    dndRMSLCDNumToolButton->setIcon(QIcon(":/images/RMSLCD.png"));
    dndPlotToolBar->addWidget(dndRMSLCDNumToolButton);

    DnDToolButton *dndP2PLCDNumToolButton = new DnDToolButton(DEF_OF_P2PLCDNUM);
    dndP2PLCDNumToolButton->setIcon(QIcon(":/images/P2PLCD.png"));
    dndPlotToolBar->addWidget(dndP2PLCDNumToolButton);

    DnDToolButton *dndAvgLCDNumToolButton = new DnDToolButton(DEF_OF_AVGLCDNUM);
    dndAvgLCDNumToolButton->setIcon(QIcon(":/images/AvgLCD.png"));
    dndPlotToolBar->addWidget(dndAvgLCDNumToolButton);

    DnDToolButton *dndVarianceLEDToolButton = new DnDToolButton(DEF_OF_VARIANCELED);
    dndVarianceLEDToolButton->setIcon(QIcon(":/images/VarianceLED.png"));
    dndPlotToolBar->addWidget(dndVarianceLEDToolButton);

    DnDToolButton *dndSnDLEDToolButton = new DnDToolButton(DEF_OF_STDEVLED);
    dndSnDLEDToolButton->setIcon(QIcon(":/images/SnDLED.png"));
    dndPlotToolBar->addWidget(dndSnDLEDToolButton);

    //설정
    QToolBar *setupToolBar = new QToolBar(tr("Setup"),this);
    addToolBar(setupToolBar);

    const QIcon iconSetup(":/images/setup.png");
    QAction* actSetup = new QAction(iconSetup, tr("&Setup"), this);
    connect(actSetup, &QAction::triggered, this, &MainWindow::slotShowAnalysisSetupDock);
    setupToolBar->addAction(actSetup);


    //분석 데이터 실행
    m_toolbarSearchSettings = new QToolBar(tr("SearchSetting"),this);
    addToolBar(m_toolbarSearchSettings);

    QLabel* lblAnalDataMRange = new QLabel(this);
    lblAnalDataMRange->setText("분석데이터분량(분단위)  ");
    m_toolbarSearchSettings->addWidget(lblAnalDataMRange);

    QComboBox* cboAnalysisMInterval = new QComboBox(this);
    cboAnalysisMInterval->setObjectName("cboAnalysisMInterval");
    cboAnalysisMInterval->addItem("1");
    cboAnalysisMInterval->addItem("3");
    cboAnalysisMInterval->addItem("5");
    cboAnalysisMInterval->addItem("10");
    cboAnalysisMInterval->addItem("20");
    cboAnalysisMInterval->addItem("30");
    cboAnalysisMInterval->addItem("40");
    cboAnalysisMInterval->addItem("50");
    cboAnalysisMInterval->addItem("60");
    m_toolbarSearchSettings->addWidget(cboAnalysisMInterval);

    QLabel* lblDataUpdateMPeriod = new QLabel(this);
    lblDataUpdateMPeriod->setText(" 데이터 update 주기(분단위)  ");
    m_toolbarSearchSettings->addWidget(lblDataUpdateMPeriod);

    QComboBox* cboDataUpdateMPeriod  = new QComboBox(this);
    cboDataUpdateMPeriod->setObjectName("cboDataUpdateMPeriod");
    cboDataUpdateMPeriod->addItem("1");
    cboDataUpdateMPeriod->addItem("3");
    cboDataUpdateMPeriod->addItem("5");
    cboDataUpdateMPeriod->addItem("10");
    cboDataUpdateMPeriod->addItem("20");
    cboDataUpdateMPeriod->addItem("30");
    cboDataUpdateMPeriod->addItem("60");
    m_toolbarSearchSettings->addWidget(cboDataUpdateMPeriod);



    const QIcon iconExec(":/images/player_play.png");
    m_pactExec = new QAction(iconExec, tr("&Exce"), this);
    connect(m_pactExec, &QAction::triggered, this, &MainWindow::slotExecAnalData);
    m_toolbarSearchSettings->addAction(m_pactExec);

    const QIcon iconPause(":/images/player_pause.png");
    m_pactPause = new QAction(iconPause, tr("&Pause"), this);
    connect(m_pactPause, &QAction::triggered, this, &MainWindow::slotPauseAnalData);
    m_toolbarSearchSettings->addAction(m_pactPause);
    m_pactPause->setEnabled(false);

    const QIcon iconStop(":/images/player_stop.png");
    m_pactStop = new QAction(iconStop, tr("&Stop"), this);
    connect(m_pactStop, &QAction::triggered, this, &MainWindow::slotStopAnalData);
    m_toolbarSearchSettings->addAction(m_pactStop);
    m_pactStop->setEnabled(false);
    //dndPlotToolBar->addWidget(stopToolButton);
    //dndPlotToolBar->insertWidget(stopAction,stopToolButton);

    //검색 조건
    m_toolbarSearchCond = new QToolBar(tr("SearchSetting"),this);
    addToolBar(m_toolbarSearchCond);

    QLabel* lblAnalStartDateTime = new QLabel(this);
    lblAnalStartDateTime->setText("분석시작일시 ");
    m_toolbarSearchCond->addWidget(lblAnalStartDateTime);

    QDateTimeEdit* dteditStartDateTime = new QDateTimeEdit(this);
    dteditStartDateTime->setObjectName("dteditStartDateTime");
    dteditStartDateTime->setDateTime(QDateTime::currentDateTime().addSecs(-60*10));
    dteditStartDateTime->setDisplayFormat("yyyy.MM.dd hh.mm.ss");
    m_toolbarSearchCond->addWidget(dteditStartDateTime);

    QLabel* lblAnalEndDateTime = new QLabel(this);
    lblAnalEndDateTime->setText("분석종료일시 ");
    m_toolbarSearchCond->addWidget(lblAnalEndDateTime);

    QDateTimeEdit* dteditEndDateTime = new QDateTimeEdit(this);
    dteditEndDateTime->setObjectName("dteditEndDateTime");
    dteditEndDateTime->setDateTime(QDateTime::currentDateTime());
    dteditEndDateTime->setDisplayFormat("yyyy.MM.dd hh.mm.ss");
    m_toolbarSearchCond->addWidget(dteditEndDateTime);

    const QIcon iconExecSearchCond(":/images/player_play.png");
    m_pactExecSearchCond = new QAction(iconExecSearchCond, tr("&ExceSearhCond"), this);
    connect(m_pactExecSearchCond, &QAction::triggered, this, &MainWindow::slotExecAnalSearchCond);
    m_toolbarSearchCond->addAction(m_pactExecSearchCond);
}

/**
 * @brief 상태 바 생성
 */
void MainWindow::createStatusBar()
{
    statusBar()->showMessage(tr("Ready"));
}

/**
 * @brief 도킹 창 생성
 */
void MainWindow::createDockWindows()
{
    m_pSensorListDock = new SensorListDock(this);
    //m_pSensorListDock->addData(m_dbMySQL,"SELECT * FROM cnt");
    m_pSensorListDock->setAllowedAreas(Qt::LeftDockWidgetArea|Qt::RightDockWidgetArea);
    m_pSensorListDock->setSizePolicy(QSizePolicy::Policy::Minimum,QSizePolicy::Policy::Maximum);
    m_pSensorListDock->setMinimumHeight(600);
    m_pSensorListDock->setMaximumWidth(300);
    addDockWidget(Qt::LeftDockWidgetArea, m_pSensorListDock);
    m_pMenuView->addAction(m_pSensorListDock->toggleViewAction());

    m_pRawAnalysisSetupDock = new RawAnalysisSetupDock(this);
    m_pRawAnalysisSetupDock->setAllowedAreas(Qt::RightDockWidgetArea);
    m_pRawAnalysisSetupDock->setSizePolicy(QSizePolicy::Policy::Minimum,QSizePolicy::Policy::Maximum);
    m_pRawAnalysisSetupDock->setMinimumHeight(400);
    m_pRawAnalysisSetupDock->setMaximumWidth(300);
    m_pRawAnalysisSetupDock->setFloating(true);
    addDockWidget(Qt::RightDockWidgetArea, m_pRawAnalysisSetupDock);
    m_pMenuView->addAction(m_pRawAnalysisSetupDock->toggleViewAction());
    m_pRawAnalysisSetupDock->setVisible(false);
    m_pRawAnalysisSetupDock->setWindowTitle("Raw");
    m_pRawAnalysisSetupDock->setID(DEF_OF_RAWPLOT);

    m_pFFTSetupDock = new FFTSetupDock(this);
    //m_pSensorListDock->addData(m_dbMySQL,"SELECT * FROM cnt");
    m_pFFTSetupDock->setAllowedAreas(Qt::RightDockWidgetArea);
    m_pFFTSetupDock->setSizePolicy(QSizePolicy::Policy::Minimum,QSizePolicy::Policy::Maximum);
    m_pFFTSetupDock->setMinimumHeight(400);
    m_pFFTSetupDock->setMaximumWidth(300);
    m_pFFTSetupDock->setFloating(true);
    addDockWidget(Qt::RightDockWidgetArea, m_pFFTSetupDock);
    m_pMenuView->addAction(m_pFFTSetupDock->toggleViewAction());
    m_pFFTSetupDock->setVisible(false);
    m_pFFTSetupDock->setWindowTitle("FFT");
    m_pFFTSetupDock->setID(DEF_OF_FFTPLOT);

    m_pAvgLCDNumSetupDock = new LCDNumSetupDock(this);
    //m_pSensorListDock->addData(m_dbMySQL,"SELECT * FROM cnt");
    m_pAvgLCDNumSetupDock->setAllowedAreas(Qt::RightDockWidgetArea);
    m_pAvgLCDNumSetupDock->setSizePolicy(QSizePolicy::Policy::Minimum,QSizePolicy::Policy::Maximum);
    m_pAvgLCDNumSetupDock->setMinimumHeight(400);
    m_pAvgLCDNumSetupDock->setMaximumWidth(300);
    m_pAvgLCDNumSetupDock->setFloating(true);
    addDockWidget(Qt::RightDockWidgetArea, m_pAvgLCDNumSetupDock);
    m_pMenuView->addAction(m_pAvgLCDNumSetupDock->toggleViewAction());
    m_pAvgLCDNumSetupDock->setVisible(false);
    m_pAvgLCDNumSetupDock->setWindowTitle("Avg");
    m_pAvgLCDNumSetupDock->setID(DEF_OF_AVGLCDNUM);

    m_pP2PLCDNumSetupDock = new LCDNumSetupDock(this);
    //m_pSensorListDock->addData(m_dbMySQL,"SELECT * FROM cnt");
    m_pP2PLCDNumSetupDock->setAllowedAreas(Qt::RightDockWidgetArea);
    m_pP2PLCDNumSetupDock->setSizePolicy(QSizePolicy::Policy::Minimum,QSizePolicy::Policy::Maximum);
    m_pP2PLCDNumSetupDock->setMinimumHeight(400);
    m_pP2PLCDNumSetupDock->setMaximumWidth(300);
    m_pP2PLCDNumSetupDock->setFloating(true);
    addDockWidget(Qt::RightDockWidgetArea, m_pP2PLCDNumSetupDock);
    m_pMenuView->addAction(m_pP2PLCDNumSetupDock->toggleViewAction());
    m_pP2PLCDNumSetupDock->setVisible(false);
    m_pP2PLCDNumSetupDock->setWindowTitle("P2P");
    m_pP2PLCDNumSetupDock->setID(DEF_OF_P2PLCDNUM);

    m_pRMSLCDNumSetupDock = new LCDNumSetupDock(this);
    //m_pSensorListDock->addData(m_dbMySQL,"SELECT * FROM cnt");
    m_pRMSLCDNumSetupDock->setAllowedAreas(Qt::RightDockWidgetArea);
    m_pRMSLCDNumSetupDock->setSizePolicy(QSizePolicy::Policy::Minimum,QSizePolicy::Policy::Maximum);
    m_pRMSLCDNumSetupDock->setMinimumHeight(400);
    m_pRMSLCDNumSetupDock->setMaximumWidth(300);
    m_pRMSLCDNumSetupDock->setFloating(true);
    addDockWidget(Qt::RightDockWidgetArea, m_pRMSLCDNumSetupDock);
    m_pMenuView->addAction(m_pRMSLCDNumSetupDock->toggleViewAction());
    m_pRMSLCDNumSetupDock->setVisible(false);
    m_pRMSLCDNumSetupDock->setWindowTitle("RMS");
    m_pRMSLCDNumSetupDock->setID(DEF_OF_RMSLCDNUM);

    m_pVarianceLEDSetupDock = new LEDSetupDock(this);
    //m_pSensorListDock->addData(m_dbMySQL,"SELECT * FROM cnt");
    m_pVarianceLEDSetupDock->setAllowedAreas(Qt::RightDockWidgetArea);
    m_pVarianceLEDSetupDock->setSizePolicy(QSizePolicy::Policy::Minimum,QSizePolicy::Policy::Maximum);
    m_pVarianceLEDSetupDock->setMinimumHeight(400);
    m_pVarianceLEDSetupDock->setMaximumWidth(300);
    m_pVarianceLEDSetupDock->setFloating(true);
    addDockWidget(Qt::RightDockWidgetArea, m_pVarianceLEDSetupDock);
    m_pMenuView->addAction(m_pVarianceLEDSetupDock->toggleViewAction());
    m_pVarianceLEDSetupDock->setVisible(false);
    m_pVarianceLEDSetupDock->setWindowTitle("Variance");
    m_pVarianceLEDSetupDock->setID(DEF_OF_VARIANCELED);

    m_pSnDLEDSetupDock = new LEDSetupDock(this);
    //m_pSensorListDock->addData(m_dbMySQL,"SELECT * FROM cnt");
    m_pSnDLEDSetupDock->setAllowedAreas(Qt::RightDockWidgetArea);
    m_pSnDLEDSetupDock->setSizePolicy(QSizePolicy::Policy::Minimum,QSizePolicy::Policy::Maximum);
    m_pSnDLEDSetupDock->setMinimumHeight(400);
    m_pSnDLEDSetupDock->setMaximumWidth(300);
    m_pSnDLEDSetupDock->setFloating(true);
    addDockWidget(Qt::RightDockWidgetArea, m_pSnDLEDSetupDock);
    m_pMenuView->addAction(m_pSnDLEDSetupDock->toggleViewAction());
    m_pSnDLEDSetupDock->setVisible(false);
    m_pSnDLEDSetupDock->setWindowTitle("SnD");
    m_pSnDLEDSetupDock->setID(DEF_OF_STDEVLED);
}

/**
 * @brief 도킹 창 설정 값 읽기
 * @param dock
 */
void MainWindow::readSettings(QDockWidget* dock) {
    QSettings settings("Company Name", "Application Name");
    settings.beginGroup("LibraryDock");
    dock->setFloating(settings.value("docked").toBool());
    dock->resize(settings.value("size", QSize(1, 1)).toSize());
    dock->move(settings.value("pos", QPoint(200, 200)).toPoint());
    addDockWidget((Qt::DockWidgetArea)settings.value("dockarea", Qt::RightDockWidgetArea).toInt(), dock);
    settings.endGroup();
}

/**
 * @brief 도킹 창 설정 값 쓰기
 * @param dock
 */
void MainWindow::writeSettings(QDockWidget* dock) {
    QSettings settings("Company Name", "Application Name");
    settings.beginGroup("LibraryDock");
    settings.setValue("dockarea", dockWidgetArea(dock));
    settings.setValue("docked", dock->isFloating());
    settings.setValue("size", dock->size());
    settings.setValue("pos", dock->pos());
    settings.endGroup();
}

/**
 * @brief 도킹 창 사이즈 설정
 * @param dock
 * @param nWidth
 * @param nHeight
 */
void MainWindow::setDockSize(QDockWidget* dock, int nWidth,int nHeight)
{
    m_sizePreMax=dock->maximumSize();
    m_sizePreMin=dock->minimumSize();
    if (nWidth>=0){
        if (dock->width()<nWidth){
            dock->setMinimumWidth(nWidth);
        }else{
            dock->setMaximumWidth(nWidth);
        }
    }
    if (nHeight>=0){
        if (dock->height()<nHeight){
            dock->setMinimumHeight(nHeight);
        }else {
            dock->setMaximumHeight(nHeight);
        }
    }
    //qApp->processEvents()
    QTimer::singleShot(1, this, SLOT(slotPreMinMaxDockSizes(QDockWidget*)));
}

/**
 * @brief 도킹 창 이전 Min/Max 설정 Slot
 * @param dock
 */
void MainWindow::slotPreMinMaxDockSizes(QDockWidget* dock)
{
    dock->setMinimumSize(m_sizePreMin);
    dock->setMaximumSize(m_sizePreMax);
}

/**
 * @brief 데이터 분석 처리
 * @param strStart
 * @param strEnd
 */
void MainWindow::processAnalysis(QString strStart,QString strEnd)
{
    QString strContainerName =  m_strFullSensorName + "/data_ch1_raw";//m_pSensorListDock->currentContainerName();//ui->cboContainer->currentText();
    m_pAnalreportdlg->loadAnalysisData(m_dbMySQL,strContainerName,strStart,strEnd);
}

/**
 * @brief 데이터 분석 처리
 * @param strContainerName
 * @param dbXAxis
 * @param dbYAxis
 * @param strStart
 * @param strEnd
 * @param dbMax
 * @param dbMin
 */
void MainWindow::processAnalysis(QString strContainerName, QString strStart,QString strEnd,double& dbMax, double& dbMin)
{
    m_pAnalreportdlg->loadAnalysisFixData(m_dbMySQL,strContainerName,strStart,strEnd,dbMax,dbMin);
}

/**
 * @brief 분석 컨테이너(센서) 설정 도킹창 보기 Slot
 */
void MainWindow::slotShowAnalysisSetupDock()
{
    if(!m_pAnalysisSetupDock){
        m_pAnalysisSetupDock = new AnalysisSetupDock(this);
        m_pAnalysisSetupDock->addData(m_dbMySQL,"SELECT * FROM ae");
        // m_pAnalysisSetupDock->addData(m_dbMySQL,"SELECT * FROM cnt");
        //m_pAnalysisSetupDock->setAllowedAreas(Qt::TopDockWidgetArea);
        addDockWidget(Qt::NoDockWidgetArea, m_pAnalysisSetupDock);
        m_pMenuView->addAction(m_pAnalysisSetupDock->toggleViewAction());
        m_pAnalysisSetupDock->setFloating(true);
        m_pAnalysisSetupDock->setHidden(true);
    }
    m_pAnalysisSetupDock->setVisible(true);
}

/**
 * @brief 검색 조건 분석 실행 Slot
 */
void MainWindow::slotExecAnalSearchCond()
{
    slotStopAnalData();

    QObjectList children = m_toolbarSearchCond->children();
    QDateTimeEdit* dteditStart = NULL;
    QDateTimeEdit* dteditEnd   = NULL;
    foreach(QObject* obj, children){
        qDebug() << obj->objectName() << endl;
        if(obj->objectName().compare("dteditStartDateTime")==0){
            dteditStart = qobject_cast<QDateTimeEdit*>(obj);
        }
        if(obj->objectName().compare("dteditEndDateTime")==0){
            dteditEnd = qobject_cast<QDateTimeEdit*>(obj);
        }
    }
    if(dteditStart && dteditEnd){
        if(!m_strFullSensorName.isEmpty()){
            QDateTime dtStart = dteditStart->dateTime();
            dtStart = dtStart.addSecs(-9*60*60);
            QDateTime dtEnd = dteditEnd->dateTime();
            dtEnd   = dtEnd.addSecs(-9*60*60);
            QString strStart = dtStart.toString("yyyyMMddThhmmss");
            QString strEnd   = dtEnd.toString("yyyyMMddThhmmss");

            execAnalysis(strStart,strEnd);
        }
    }
}

/**
 * @brief 분석 데이터 실행 Slot
 */
void MainWindow::slotExecAnalData()
{
    if(!m_strFullSensorName.isEmpty()){
        m_pactExec->setEnabled(false);
        m_pactPause->setEnabled(true);
        m_pactStop->setEnabled(true);


        QObjectList children = m_toolbarSearchSettings->children();
        QComboBox* cboUpdate = NULL;
        foreach(QObject* obj, children){
            qDebug() << obj->objectName() << endl;
            if(obj->objectName().compare("cboDataUpdateMPeriod")==0){
                cboUpdate = qobject_cast<QComboBox*>(obj);
            }
        }
        if(cboUpdate){
            QString strPeriod = cboUpdate->currentText();
            //m_timerDataAnalysis.start(0);
            slotUpdateAnalysisReport();
            m_timerDataAnalysis.start(strPeriod.toInt()*DEF_OF_PERIOD_INTERVAL); // Interval 0 means to refresh as fast as possible
        }
    }
}

/**
 * @brief 분석 데이터 일시 정지 Slot
 */
void MainWindow::slotPauseAnalData()
{
    m_pactExec->setEnabled(true);
    m_pactPause->setEnabled(false);
    m_pactStop->setEnabled(true);
    //qDebug() << "slotPauseAnalData " << endl;
    m_timerDataAnalysis.stop();
}

/**
 * @brief 분석 데이터 정지 Slot
 */
void MainWindow::slotStopAnalData()
{
    m_pactExec->setEnabled(true);
    m_pactPause->setEnabled(false);
    m_pactStop->setEnabled(true);
    //qDebug() << "slotStopAnalData " << endl;
    m_timerDataAnalysis.stop();
    m_pAnalreportdlg->setDefaultUI(0);
}

/**
 * @brief 센서 리스트 도킹 창 업데이트
 * @param strContainser
 * @param strAlgorithm
 */
void MainWindow::updateSensorListDock(QString strContainser, QString strAlgorithm)
{
    if(m_pSensorListDock){
        m_pSensorListDock->updateSensorList(strContainser,strAlgorithm);
    }
}

/**
 * @brief 분석 보고 다이얼로그 업데이트 Slot
 */
void MainWindow::slotUpdateAnalysisReport()
{
    if(!m_strFullSensorName.isEmpty()){        
        QObjectList children = m_toolbarSearchSettings->children();
        QComboBox* cboInterval = NULL;
        foreach(QObject* obj, children){
            //qDebug() << obj->objectName() << endl;
            if(obj->objectName().compare("cboAnalysisMInterval")==0){
                cboInterval = qobject_cast<QComboBox*>(obj);
            }
        }
        if(cboInterval){
            QString strInterval = cboInterval->currentText();
            QDateTime dtStart = QDateTime::currentDateTime();
            dtStart = dtStart.addSecs(-9*60*60);
            dtStart = dtStart.addSecs(-(strInterval.toInt()*DEF_OF_PERIOD_DATA_GAP));
            QDateTime dtEnd = QDateTime::currentDateTime();
            dtEnd   = dtEnd.addSecs(-9*60*60);

            QString strStart = dtStart.toString("yyyyMMddThhmmss");
            QString strEnd   = dtEnd.toString("yyyyMMddThhmmss");

            execAnalysis(strStart,strEnd);
        }
    }
}

/**
 * @brief 분석 실행
 * @param strStart
 * @param strEnd
 */
void MainWindow::execAnalysis(QString strStart,QString strEnd)
{
    quint32 uiAppliedAlgorithm = 0x00000000;
    m_pAnalreportdlg->clearPlot();

    //qDebug() << "Size : " << QDateTime::currentDateTime().toString("mm.ss.zzz") <<  endl;
    processAnalysis(strStart,strEnd);    

    double dbMax,dbMin;
    processAnalysis(m_strCompFullSensorName + "/data_ch1_raw",strStart,strEnd,dbMax,dbMin);

    //qDebug() << "Size : " << QDateTime::currentDateTime().toString("mm.ss.zzz") << m_vYAxisRawVal.size() <<  endl;
    processRaw();
    //qDebug() << "Size : " << QDateTime::currentDateTime().toString("mm.ss.zzz") << endl;
    //m_dbMax = *std::max_element(m_vYAxisRawVal.constBegin(),m_vYAxisRawVal.constEnd());
    //m_dbMin = *std::min_element(m_vYAxisRawVal.constBegin(),m_vYAxisRawVal.constEnd());
    QVector<QString>* vAlgorithms = m_pSensorListDock->currentSensorAlgorithms();
    foreach(QString strAlgorithmName, *vAlgorithms){
        if(strAlgorithmName.compare("FFT")==0){
            processFFT();
            uiAppliedAlgorithm |= 0x1;
        }else if(strAlgorithmName.compare("RMS")==0){
            processRMS();
            uiAppliedAlgorithm |= 0x2;
        }else if(strAlgorithmName.compare("P2P")==0){
            processP2P();
            uiAppliedAlgorithm |= 0x4;
        }else if(strAlgorithmName.compare("Average")==0){
            processAvg();
            uiAppliedAlgorithm |= 0x8;
        }else if(strAlgorithmName.compare("Variance")==0){
            processVariance();
            uiAppliedAlgorithm |= 0x10;
        }else if(strAlgorithmName.compare("SnD")==0){
            processSnD();
            uiAppliedAlgorithm |= 0x11;
        }
    }
    m_pAnalreportdlg->setDefaultUI(uiAppliedAlgorithm);
}

/**
 * @brief 분석 보고서 연결 정보 설정
 * @param strFullSensorName
 */
void MainWindow::setAnalysisReportConnInfo(QString strFullSensorName)
{
    if(m_strFullSensorName.compare(strFullSensorName)!=0){
        m_pAnalreportdlg->clearAll();        
        m_strFullSensorName = strFullSensorName;
    }
}

/**
 * @brief Raw 데이터 처리
 */
void MainWindow::processRaw()
{
    //m_pAnalreportdlg->replotAnalTool(0,m_strFullSensorName, m_vXAxisRawVal[MainWindow::enRawData], m_vYAxisRawVal[MainWindow::enRawData]);
    //m_pAnalreportdlg->replotAnalTool(0,m_strCompFullSensorName, m_vXAxisRawVal[MainWindow::enFixRawData], m_vYAxisRawVal[MainWindow::enFixRawData]);
    m_pAnalreportdlg->reloadPlot(m_strFullSensorName);
}

/**
 * @brief FFT 처리
 */
void MainWindow::processFFT()
{
    m_pAnalreportdlg->reloadFFTPlot(m_strFullSensorName);
}

/**
 * @brief 평균 처리
 */
void MainWindow::processAvg()
{    
    m_pAnalreportdlg->setAvg();
}

/**
 * @brief P2P 처리
 */
void MainWindow::processP2P()
{    
    m_pAnalreportdlg->setP2P();
}

/**
 * @brief RMS 처리
 */
void MainWindow::processRMS()
{    
    m_pAnalreportdlg->setRMS();
}

/**
 * @brief 분선 처리
 */
void MainWindow::processVariance()
{
    m_pAnalreportdlg->setVariance();

}

/**
 * @brief 표준 편차 처리
 */
void MainWindow::processSnD()
{
    m_pAnalreportdlg->setSnD();
}

/**
 * @brief RAW 설정 도킹 창 보기
 * @param bShow
 */
void MainWindow::showRawSetupDock(bool bShow)
{
    if(m_pRawAnalysisSetupDock){
        m_pRawAnalysisSetupDock->setVisible(bShow);
    }
}

/**
 * @brief FFT 설정 도킹 창 보기
 * @param bShow
 */
void MainWindow::showFFTSetupDock(bool bShow)
{
    if(m_pFFTSetupDock){
        m_pFFTSetupDock->setVisible(bShow);
    }
}

/**
 * @brief LCD 설정 도킹 창 보기
 * @param nIndex
 * @param bShow
 */
void MainWindow::showLCDNumSetupDock(int nIndex, bool bShow)
{
    if(nIndex==0){//Avg
        if(m_pAvgLCDNumSetupDock){
            m_pAvgLCDNumSetupDock->setVisible(bShow);
        }
    }else if(nIndex==1){//P2P
        if(m_pP2PLCDNumSetupDock){
            m_pP2PLCDNumSetupDock->setVisible(bShow);
        }
    }else if(nIndex==2){//RMS
        if(m_pRMSLCDNumSetupDock){
            m_pRMSLCDNumSetupDock->setVisible(bShow);
        }
    }else{
    }
}

/**
 * @brief LED 설정 도킹 창 보기
 * @param nIndex
 * @param bShow
 */
void MainWindow::showLEDSetupDock(int nIndex, bool bShow)
{
    if(nIndex==0){//Variance
        if(m_pVarianceLEDSetupDock){
            m_pVarianceLEDSetupDock->setVisible(bShow);
        }
    }else if(nIndex==1){ //SnD
        if(m_pSnDLEDSetupDock){
            m_pSnDLEDSetupDock->setVisible(bShow);
        }
    }else{
    }
}

/**
 * @brief Raw 데이터 파일 저장
 */
void MainWindow::saveRawDataToFile()
{
    if(m_pAnalreportdlg){
        m_pAnalreportdlg->saveRawDataToFile();
    }
}

/**
 * @brief 그래프 Show/Hide
 * @param nMode
 * @param bShow
 */
void MainWindow::setVisibleRawGraph(int nMode /*0*/, bool bShow  /*true*/)
{
    if(m_pAnalreportdlg){
        m_pAnalreportdlg->setVisibleRawGraph(nMode,bShow);
    }
}

/**
 * @brief FFT 데이터 파일 저장
 */
void MainWindow::saveFFTDataToFile()
{
    if(m_pAnalreportdlg){
        m_pAnalreportdlg->saveFFTDataToFile();
    }
}

/**
 * @brief 그래프 Show/Hide
 * @param nMode
 * @param bShow
 */
void MainWindow::setVisibleFFTGraph(int nMode /*0*/, bool bShow  /*true*/)
{
    if(m_pAnalreportdlg){
        m_pAnalreportdlg->setVisibleFFTGraph(nMode,bShow);
    }
}
/**
 * @brief 컨트롤 속성 아이템 설정
 * @param options
 */
void MainWindow::setControlOptions(ControlOptions& options)
{
    if(m_pAnalreportdlg){
      m_pAnalreportdlg->setControlOptions(options);
      //m_pAnalreportdlg->setVisible(true);
    }
}

