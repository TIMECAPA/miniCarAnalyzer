#include "analysissetupdock.h"
#include "ui_analysissetupdock.h"

#include <QDebug>
#include <QStringList>
#include <QtSql>
#include <QMessageBox>
#include "mainwindow.h"

/**
 * @brief 생성자
 * @param parent
 */
AnalysisSetupDock::AnalysisSetupDock(QWidget *parent) :
    QDockWidget(parent),
    ui(new Ui::AnalysisSetupDock)
{
    ui->setupUi(this);
    ui->cboAlgorithm->addItem("FFT");
    ui->cboAlgorithm->addItem("RMS");
    ui->cboAlgorithm->addItem("P2P");
    ui->cboAlgorithm->addItem("Average");
    ui->cboAlgorithm->addItem("Variance");//분산
    ui->cboAlgorithm->addItem("S/D"); //Standard Deviation
}

/**
 * @brief 소멸자
 */
AnalysisSetupDock::~AnalysisSetupDock()
{
    delete ui;
}

/**
 * @brief 분석 대상 센서 추가 클릭 이벤트
 */
void AnalysisSetupDock::on_btnAdd_clicked()
{
    MainWindow* main = qobject_cast<MainWindow*>(parent());
    QString strContainerName = ui->cboContainer->currentText();
    QString strAlgorithm     = ui->cboAlgorithm->currentText();
    if(!strContainerName.isEmpty() || !strAlgorithm.isEmpty() ){
       main->updateSensorListDock(strContainerName,strAlgorithm);
    }
}

/**
 * @brief 컨테이너 정보 가져오기
 * @param dbMySQL
 * @param strQuery
 */
void AnalysisSetupDock::addData(QSqlDatabase& dbMySQL, QString strQuery)
{
    ui->cboContainer->clear();
    if (!dbMySQL.open()){
        QMessageBox::critical(0, QObject::tr("Database Error"),
        dbMySQL.lastError().text());
    }

    QSqlQuery sqlQuery;
    sqlQuery.exec(strQuery);

    QString strContainerName;
    QStringList strlistContainers;

    int index=0;
    while(sqlQuery.next())
    {
        strContainerName = sqlQuery.value(0).toString();
        strlistContainers = strContainerName.split("/");
        ui->cboContainer->addItem(strContainerName);
    }
    dbMySQL.close();
}

void AnalysisSetupDock::on_btnAllAdd_clicked()
{
    std::vector<QString> list(ui->cboAlgorithm->count());
    QString strAlgorithm;

    MainWindow* main = qobject_cast<MainWindow*>(parent());
    QString strContainerName = ui->cboContainer->currentText();


    for(auto i = 0; i < list.size(); i++) {
       strAlgorithm = ui->cboAlgorithm->itemText(i);

        if(!strContainerName.isEmpty() || !strAlgorithm.isEmpty() ){
           main->updateSensorListDock(strContainerName,strAlgorithm);
        }

    }
}

