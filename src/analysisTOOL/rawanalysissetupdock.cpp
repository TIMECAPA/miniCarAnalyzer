#include "rawanalysissetupdock.h"
#include "ui_rawanalysissetupdock.h"

RawAnalysisSetupDock::RawAnalysisSetupDock(QWidget *parent) :
    QDockWidget(parent),
    ui(new Ui::RawAnalysisSetupDock)
{
    ui->setupUi(this);

    m_pMainWindow = qobject_cast<MainWindow*>(parent);

    m_strID = "";
}

RawAnalysisSetupDock::~RawAnalysisSetupDock()
{
    delete ui;
}

/**
 * @brief ID 설정
 * @param strID
 */
void RawAnalysisSetupDock::setID(QString strID)
{
    m_strID = strID;
}

/**
 * @brief ID 가져오기
 * @return
 */
QString RawAnalysisSetupDock::id() const
{
    return m_strID;
}

/**
 * @brief RAW 데이터 저장 클릭 이벤트
 */
void RawAnalysisSetupDock::on_btnSaveDataToFile_clicked()
{
    m_pMainWindow->saveRawDataToFile();
}

/**
 * @brief 센서X 그래프 Show/Hide
 */
void RawAnalysisSetupDock::on_chkShowGraphX_clicked()
{
    Qt::CheckState chkState = ui->chkShowGraphX->checkState();
    bool bShow = false;
    chkState == Qt::Checked ? bShow = true : bShow = false;
    m_pMainWindow->setVisibleRawGraph(0,bShow);
}

/**
 * @brief 센서Y 그래프 Show/Hide
 */
void RawAnalysisSetupDock::on_chkShowGraphY_clicked()
{
    Qt::CheckState chkState = ui->chkShowGraphY->checkState();
    bool bShow = false;
    chkState == Qt::Checked ? bShow = true : bShow = false;
    m_pMainWindow->setVisibleRawGraph(1,bShow);
}

/**
 * @brief 센서Z 그래프 Show/Hide
 */
void RawAnalysisSetupDock::on_chkShowGraphZ_clicked()
{
    Qt::CheckState chkState = ui->chkShowGraphZ->checkState();
    bool bShow = false;
    chkState == Qt::Checked ? bShow = true : bShow = false;
    m_pMainWindow->setVisibleRawGraph(2,bShow);
}
