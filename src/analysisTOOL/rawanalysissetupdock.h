#ifndef RAWANALYSISSETUPDOCK_H
#define RAWANALYSISSETUPDOCK_H

#include <QDockWidget>
#include "mainwindow.h"

namespace Ui {
class RawAnalysisSetupDock;
}

class RawAnalysisSetupDock : public QDockWidget
{
    Q_OBJECT
    Q_PROPERTY(QString id READ id WRITE setID)
public:
    explicit RawAnalysisSetupDock(QWidget *parent = 0);
    ~RawAnalysisSetupDock();    
private:
    Ui::RawAnalysisSetupDock *ui;

    /**
     * @brief 메인 인도우 포인트
     */
    MainWindow*         m_pMainWindow;
    /**
     * @brief ID
     */
    QString m_strID;
public:
    void setID(QString strID);
    QString id() const;
private slots:
    void on_btnSaveDataToFile_clicked();

    void on_chkShowGraphX_clicked();
    void on_chkShowGraphY_clicked();
    void on_chkShowGraphZ_clicked();
};

#endif // RAWANALYSISSETUPDOCK_H
