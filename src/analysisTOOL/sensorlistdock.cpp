#include "sensorlistdock.h"
#include "ui_sensorlistdock.h"

#include <QTreeWidgetItem>
#include <QDebug>
#include <QStringList>
#include <QtSql>
#include <QMenu>
#include <QMessageBox>

#include "mainwindow.h"

/**
 * @brief 생성자
 * @param parent
 */
SensorListDock::SensorListDock(QWidget *parent) :
    QDockWidget(parent),
    ui(new Ui::SensorListDock)
{
    ui->setupUi(this);

    ui->treeWidget->setColumnCount(3);
    ui->treeWidget->setHeaderLabels(QStringList() << "구성" << "측정항목" << "항목명");
    ui->treeWidget->header()->resizeSection(0, 140);
    ui->treeWidget->header()->resizeSection(1, 120);

    // 우버튼 단축메뉴
    ui->treeWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->treeWidget, SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(slotShowContextMenu(QPoint)));      
    connect(this, SIGNAL(signalUpdatePlot(QTreeWidgetItem *, int )), parent, SLOT(slotUpdatePlot(QTreeWidgetItem *, int)));

    m_ptreeItemOnContext = NULL;

}

/**
 * @brief 소멸자
 */
SensorListDock::~SensorListDock()
{
    delete ui;
}

/**
 * @brief TreeWidget 클리어
 */
void SensorListDock::treeWidgetClear()
{
    ui->treeWidget->clear();
}

/**
 * @brief TreeWidget Depth
 * @param item
 * @return
 */
int SensorListDock::treeWidgetDepth(QTreeWidgetItem* item)
{
    int nDepth = 0;
    //QTreeWidgetItem *item = ...;
    while(item!=0){
      nDepth++;
      item = item->parent();
    }
    return nDepth;
}

/**
 * @brief 컨텍스트 메뉴 보기 Slot
 * @param pos
 */
void SensorListDock::slotShowContextMenu(QPoint pos)
{
    //QTreeWidgetItem* item =  ui->treeWidget->itemAt(pos);
    if(ui->treeWidget->topLevelItem(0)){
        m_ptreeItemOnContext = ui->treeWidget->itemAt(pos);
        int nDepth = treeWidgetDepth(m_ptreeItemOnContext);
        int column = 1;
        QString strTemp = m_ptreeItemOnContext->text(0);
        if(nDepth==3){
            //if(item->parent()){//Only Child
            QSignalMapper *pSignalMapper = new QSignalMapper(this);
            QString strSensorName = "/" + m_ptreeItemOnContext->parent()->parent()->text(column - 1) + "/" + m_ptreeItemOnContext->parent()->text(column - 1) + "/" + m_ptreeItemOnContext->text(column - 1 );
            QMenu *menu=new QMenu(this);
            QAction* actSensorConnection = menu->addAction(QString("연결하기"));
            //QAction* actionPlotDeletion = menu->addAction(QString("실시간 그래프 삭제"));

            connect(actSensorConnection,SIGNAL(triggered()),pSignalMapper,SLOT(map()));
            //connect(actionPlotDeletion,SIGNAL(triggered()),pSignalMapper,SLOT(map()));

            pSignalMapper->setMapping(actSensorConnection,"0;" +strSensorName );
           // pSignalMapper->setMapping(actionPlotDeletion,"1;" +strSensorName);

            connect(pSignalMapper, SIGNAL(mapped(QString)),this, SLOT(slotSetConnection(QString)));
            menu->popup(ui->treeWidget->viewport()->mapToGlobal(pos));
        }
    }

}

/**
 * @brief 분석 센선 연결 하기 설정
 * @param strSelInfo
 */
void SensorListDock::slotSetConnection(QString strSelInfo)
{
    MainWindow* mainWindow = qobject_cast<MainWindow*>(this->parent());

    QStringList  strListInfo = strSelInfo.split(';');
    if(strListInfo.size()==2 && m_ptreeItemOnContext != NULL){
        QString strIndex = strListInfo.at(0);
        int nIdx = strIndex.toInt();
        int nDepth = treeWidgetDepth(m_ptreeItemOnContext);
        if(nDepth==3){
            setCurrentSensorAlgorithms(m_ptreeItemOnContext);
            if(nIdx == 0){
                QString strSensorName = "/" + m_ptreeItemOnContext->parent()->parent()->text(0)
                                        + "/" + m_ptreeItemOnContext->parent()->text(0)
                                        + "/" + m_ptreeItemOnContext->text(0);
                m_strCurrSensorName = strSensorName;
                mainWindow->setAnalysisReportConnInfo(m_strCurrSensorName);
            }
        }
        //qDebug()<< "Plot Index  : " << strListInfo.at(0) << "Sensor Name  : " <<strListInfo.at(1) << endl;
    }else{
        QMessageBox::information(this,"PLOT INFO","정보가 잘못되었습니다.");
    }
}

/**
 * @brief 센선 리스트 업데이트
 * @param strContainser
 * @param strAlgorithm
 */
void SensorListDock::updateSensorList(QString strContainser,QString strAlgorithm)
{
    QStringList strlistContainers = strContainser.split("/");
    int nSize = strlistContainers.size();
    if(nSize==4){
        int nCount = ui->treeWidget->topLevelItemCount();
        QTreeWidgetItem *itemParent= NULL;
        QTreeWidgetItem *itemChild = NULL;
        QTreeWidgetItem *itemSensor= NULL;
        if(nCount>0){
            itemParent = ui->treeWidget->topLevelItem(0);
            QList<QTreeWidgetItem*> listAE = ui->treeWidget->findItems(strlistContainers.at(2), Qt::MatchExactly | Qt::MatchRecursive);
            if(listAE.count()>0){//AE
                itemChild = listAE.at(0);
                int nSensorCnt = itemChild->childCount();
                bool bExisted=false;
                for(int i=0; i<nSensorCnt;i++){
                    if(itemChild->child(i)->text(0).compare(strlistContainers.at(3))==0){
                        itemSensor = itemChild->child(i);
                        int nAlgorithmCnt = itemSensor->childCount();//Algorithm
                        bool bExistedAlgorithm=false;
                        for(int i=0; i<nAlgorithmCnt;i++){
                            qDebug() << itemSensor->child(i)->text(1) << endl;
                            if(itemSensor->child(i)->text(1).compare(strAlgorithm)==0){
                                bExistedAlgorithm = true;
                                break;
                            }
                        }
                        if(!bExistedAlgorithm){
                            addChildToListWidget(strAlgorithm,itemSensor,1);
                        }
                        bExisted = true;
                        break;
                    }
                }
                if(!bExisted){
                    itemSensor = addChildToListWidget(strlistContainers.at(3),itemChild,0);
                    addChildToListWidget(strAlgorithm,itemSensor,1);
                }
            }else{
                itemChild = addChildToListWidget(strlistContainers.at(2),itemParent,0);
                itemSensor = addChildToListWidget(strlistContainers.at(3),itemChild,0);
                addChildToListWidget(strAlgorithm,itemSensor,1);
            }
        }else{
            itemParent = new QTreeWidgetItem(ui->treeWidget);
            itemParent->setText(0, strlistContainers.at(1));
            itemParent->setText(1, "");
            itemParent->setText(2, "");
            itemParent->setExpanded(true);
            if(itemParent){
                itemChild = addChildToListWidget(strlistContainers.at(2),itemParent,0);
                itemSensor = addChildToListWidget(strlistContainers.at(3),itemChild,0);
                addChildToListWidget(strAlgorithm,itemSensor,1);
            }

        }
        ui->treeWidget->sortItems(1,Qt::AscendingOrder);
        if(itemSensor){
            int nDepth = treeWidgetDepth(itemSensor);
            if(nDepth ==3){
                QString strSensorName = "/" + itemSensor->parent()->parent()->text(0)
                                        + "/" + itemSensor->parent()->text(0)
                                        + "/" + itemSensor->text(0);
                if(m_strCurrSensorName.compare(strSensorName)==0){
                    setCurrentSensorAlgorithms(itemSensor);
                }
            }
        }
    }


}

/**
 * @brief ListWidget에 자식 추가
 * @param strContents
 * @param itemParent
 * @param nColumn
 * @return
 */
QTreeWidgetItem* SensorListDock::addChildToListWidget(QString strContents, QTreeWidgetItem* itemParent, int nColumn)
{
    QTreeWidgetItem *itemChild =new QTreeWidgetItem();
    if(nColumn==0){
        itemChild->setText(0, strContents);
        itemChild->setText(1, "");
        itemChild->setText(2, "");
    }else if(nColumn==1){
        itemChild->setText(0, "");
        itemChild->setText(1, strContents);
        itemChild->setText(2, "");
    }else if(nColumn==2){
        itemChild->setText(0, "");
        itemChild->setText(1, "");
        itemChild->setText(2, strContents);
    }
    itemParent->addChild(itemChild);
    itemParent->setExpanded(true);
    return itemChild;
}

/**
 * @brief 현재 센서 알고리즘 설정
 * @param item
 */
void SensorListDock::setCurrentSensorAlgorithms(QTreeWidgetItem* item)
{
    if(item){
        m_vCurrSensorAlgorithms.clear();
        QList<QTreeWidgetItem*> children = item->takeChildren();
        foreach(QTreeWidgetItem* child, children){
            m_vCurrSensorAlgorithms.push_back(child->text(1));
            item->addChild(child);
        }
    }
}

/**
 * @brief 현재 센서 알고리즘 가져오기
 * @return
 */
QVector<QString>* SensorListDock::currentSensorAlgorithms()
{
    return &m_vCurrSensorAlgorithms;
}
