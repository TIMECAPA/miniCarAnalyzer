#include "ledsetupdock.h"
#include "ui_ledsetupdock.h"

/**
 * @brief 생성자
 * @param parent
 */
LEDSetupDock::LEDSetupDock(QWidget *parent) :
    QDockWidget(parent),
    ui(new Ui::LEDSetupDock)
{
    ui->setupUi(this);
    ui->frameRed->setStyleSheet("background-color: red");
    ui->frameGreen->setStyleSheet("background-color: green");
    ui->frameYellow->setStyleSheet("background-color: yellow");
    ui->dbspinUpperLimit->setValue(1.000000);
    ui->dbspinLowerLimit->setValue(-1.000000);

    m_strID = "";
    bool bChecked = ui->chkUseUpperLimit->checkState() == Qt::Checked;
    m_controlOptions.setUseUpperLimit(bChecked);
    m_controlOptions.setUpperLimit(ui->dbspinUpperLimit->value());
    bChecked = ui->chkUseLowerLimit->checkState() == Qt::Checked;
    m_controlOptions.setUseLowerLimit(bChecked);
    m_controlOptions.setLowerLimit(ui->dbspinLowerLimit->value());

    m_pMainWindow = qobject_cast<MainWindow*>(parent);
}

/**
 * @brief 소멸자
 */
LEDSetupDock::~LEDSetupDock()
{
    delete ui;
}

/**
 * @brief ID 설정
 * @param strID
 */
void LEDSetupDock::setID(QString strID)
{
    m_strID = strID;
    m_controlOptions.setID(strID);
}

/**
 * @brief ID 가져오기
 * @return
 */
QString LEDSetupDock::id() const
{
    return m_strID;
}

/**
 * @brief 상한 값 사용 클릭 이벤트
 */
void LEDSetupDock::on_chkUseUpperLimit_clicked()
{
    m_controlOptions.setUseUpperLimit(ui->chkUseUpperLimit->checkState()==Qt::Checked);
    if(m_pMainWindow) m_pMainWindow->setControlOptions(m_controlOptions);
}

/**
 * @brief 상한 값 스핀 값 변경 이벤트
 * @param arg1
 */
void LEDSetupDock::on_dbspinUpperLimit_valueChanged(double arg1)
{
    m_controlOptions.setUpperLimit(arg1);
    bool bChecked = ui->chkUseUpperLimit->checkState()==Qt::Checked;
    if(bChecked) m_pMainWindow->setControlOptions(m_controlOptions);
}

/**
 * @brief 하한 값 사용 클릭 이벤트
 */
void LEDSetupDock::on_chkUseLowerLimit_clicked()
{
    m_controlOptions.setUseLowerLimit(ui->chkUseLowerLimit->checkState() == Qt::Checked);
    if(m_pMainWindow) m_pMainWindow->setControlOptions(m_controlOptions);
}

/**
 * @brief 하한 값 스핀 값 변경 이벤트
 * @param arg1
 */
void LEDSetupDock::on_dbspinLowerLimit_valueChanged(double arg1)
{
    bool bChecked = ui->chkUseLowerLimit->checkState()==Qt::Checked;
    m_controlOptions.setLowerLimit(arg1);
    if(bChecked) m_pMainWindow->setControlOptions(m_controlOptions);
}
