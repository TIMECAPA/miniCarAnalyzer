#ifndef LCDNUMSETUPDOCK_H
#define LCDNUMSETUPDOCK_H

#include <QDockWidget>
#include "mainwindow.h"

namespace Ui {
class LCDNumSetupDock;
}

/**
 * @brief LCD 속성 설정 도킹창 클래스
 */
class LCDNumSetupDock : public QDockWidget
{
    Q_OBJECT
    Q_PROPERTY(QString id READ id WRITE setID)
public:
    explicit LCDNumSetupDock(QWidget *parent = 0);
    ~LCDNumSetupDock();
private slots:
    void on_chkUseUpperLimit_clicked();
    void on_dbspinUpperLimit_valueChanged(double arg1);
    void on_chkUseLowerLimit_clicked();
    void on_dbspinLowerLimit_valueChanged(double arg1);
private:
    Ui::LCDNumSetupDock *ui;
    /**
     * @brief 메인 윈도우 포인트
     */
    MainWindow*         m_pMainWindow;
    /**
     * @brief 컨트롤 속성 아이템
     */
    ControlOptions      m_controlOptions;
    /**
     * @brief ID
     */
    QString m_strID;
public:
    void setID(QString strID);
    QString id() const;
};

#endif // LCDNUMSETUPDOCK_H
