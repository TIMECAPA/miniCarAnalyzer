QT += network
QT += widgets
QT += sql
QT += gui


qtHaveModule(printsupport): QT += printsupport

HEADERS         = mainwindow.h \
                  qcustomplot.h \
                  analreportdlg.h \
    sensorlistdock.h \
    analysissetupdock.h \
    fftsetupdock.h \
    lcdnumsetupdock.h \
    ledsetupdock.h \
    rawanalysissetupdock.h
SOURCES         = main.cpp \
                  mainwindow.cpp \
                  qcustomplot.cpp \
                  analreportdlg.cpp \
    sensorlistdock.cpp \
    analysissetupdock.cpp \
    fftsetupdock.cpp \
    lcdnumsetupdock.cpp \
    ledsetupdock.cpp \
    rawanalysissetupdock.cpp

RESOURCES       = analysisTOOL.qrc

FORMS += \
    analreportdlg.ui \
    sensorlistdock.ui \
    analysissetupdock.ui \
    fftsetupdock.ui \
    lcdnumsetupdock.ui \
    ledsetupdock.ui \
    rawanalysissetupdock.ui

win32: LIBS += -L$$PWD/../../sdk/mysql-connector-c-6.1.6-win32/lib/ -llibmysql

INCLUDEPATH += $$PWD/../../sdk/mysql-connector-c-6.1.6-win32/include
DEPENDPATH += $$PWD/../../sdk/mysql-connector-c-6.1.6-win32/include



macx: LIBS += -L$$PWD/../../sdk/mysql-connector-c-6.1.6-osx10.8-x86_64/lib/ -lmysqlclient

INCLUDEPATH += $$PWD/../../sdk/mysql-connector-c-6.1.6-osx10.8-x86_64/include
DEPENDPATH += $$PWD/../../sdk/mysql-connector-c-6.1.6-osx10.8-x86_64/include


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../sdk/RayUtils/lib/ -lRayUtils
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../sdk/RayUtils/lib/ -lRayUtils

INCLUDEPATH += $$PWD/../../sdk/RayUtils/inc
DEPENDPATH += $$PWD/../../sdk/RayUtils/inc
