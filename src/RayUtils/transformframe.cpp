#include "transformframe.h"

#include <QPainter>
#include <QPaintEvent>
#include <QStylePainter>
#include <QStyleOptionFocusRect>
#include <QStyle>
#include <QDebug>

/**
 * @brief 생성자
 * @param parent
 * @param f
 */
TransformFrame::TransformFrame(QWidget* parent, Qt::WindowFlags f)
    : QFrame(parent,f),
      m_bPressed(false)

{
    m_pParent = parent;
    setEditable(false);
    //setFrameName("DEMO");
    //updateBackBuffer();
    setWindowFlags(Qt::SubWindow);
    m_pSizeGrip = new QSizeGrip(this);
    m_pSizeGrip->setVisible(false);
    //QGridLayout * layout = new QGridLayout(tableWidget);
    //this->setMouseTracking(true);
    //setAutoFillBackground(true);

}

/**
 * @brief 소멸자
 */
TransformFrame::~TransformFrame()
{

}

/**
 * @brief 편집가능 상태 가져오기
 * @return
 */
bool TransformFrame::editable() const
{
    return m_bEditable;
}

/**
 * @brief 편집 가능 상태 설정
 * @param b
 */
void TransformFrame::setEditable(bool b)
{
    m_bEditable = b;
}

/**
 * @brief 백 버프 스크린에 그리기
 */
void TransformFrame::updateBackBuffer()
{
    m_pixBackBuffer = QPixmap(size());
    m_pixBackBuffer.fill(this, 0, 0);

    QPainter painter(&m_pixBackBuffer);
    painter.initFrom(this);
    //drawGrid(&painter);
    //drawCurves(&painter);
    //drawTracker(&painter);
    update();
}

/**
 * @brief Frame 이름 가져오기
 * @return
 */
QString TransformFrame::frameName() const
{
    return m_strFrameName;
}
/**
 * @brief Frame 이름 설정
 * @param strFrameName
 */
void TransformFrame::setFrameName(QString strFrameName)
{
    m_strFrameName = strFrameName;
}

/**
 * @brief Frame 타이틀 그리기
 * @param painter
 * @param rect
 */
void TransformFrame::darawTitle(QPainter *painter,QRect& rect)
{
    QRect rectText(rect);
    QPoint pt(1,1);
    rectText.setTopLeft(pt);
    painter->setPen(Qt::black);
    painter->setFont(QFont("Consolas", 12));
    //painter->drawText(rectText, Qt::TextDontClip| Qt::AlignCenter, m_strFrameName);
    painter->drawText(rectText, Qt::TextDontClip, m_strFrameName);
}
/**
 * @brief Frame Border 그리기
 * @param painter
 * @param rect
 */
void TransformFrame::drawFrameBorder(QPainter *painter,QRect& rect)
{
//    int Margin = 0;
//    QRect rect(Margin, Margin,width() - 2 * Margin, height() - 2 * Margin);    
    if(editable()){
        int width = 2;
        Qt::PenStyle style = Qt::DotLine;
        Qt::PenCapStyle cap = Qt::PenCapStyle(0);
        Qt::PenJoinStyle join = Qt::PenJoinStyle(0);
        painter->setPen(QPen(Qt::blue, width, style, cap, join));
        painter->drawRect(rect.adjusted(0, 0, -1, -1));
    }else{
        painter->setPen(Qt::black);
        painter->drawRect(rect.adjusted(0, 0, -1, -1));
    }
}

/**
 * @brief Frame 숨김 버튼 그리기
 * @param painter
 * @param rect
 */
void TransformFrame::drawHideButton(QPainter *painter,QRect& rect)
{
    if(editable()){
        //m_rectHideButton.setRect(rect.right()-12,-2,16,16);
        m_rectHideButton.setRect(rect.right()-16,-1,16,16);
        QPixmap pixmap(":/images/closetab.png");
        painter->drawPixmap(m_rectHideButton,pixmap);
    }
}

/**
 * @brief Frame paintEvent 처리
 * @param event
 */
void TransformFrame::paintEvent(QPaintEvent *event)
{
    QRect rect = event->rect();
    QPainter painter;
    painter.begin(this);

    darawTitle(&painter,rect);
    drawFrameBorder(&painter,rect);
    drawHideButton(&painter,rect);

    painter.setPen(palette().dark().color());
    painter.setBrush(Qt::blue);
    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.save();
    //painter.fillRect(rect, Qt::blue);
    //painter.drawRect(rect.adjusted(0, 0, -1, -1));
    //QRect rectMoveFrameGrip(0,0,25,10);
    //painter.drawRoundedRect(rectMoveFrameGrip, 1, 1, Qt::RelativeSize);


//    if (highlightedRect.isValid()) {
//        painter.setBrush(QColor("#ffcccc"));
//        painter.setPen(Qt::NoPen);
//        painter.drawRect(highlightedRect.adjusted(0, 0, -1, -1));
//    }
    painter.restore();
    painter.setRenderHint(QPainter::Antialiasing, false);
    painter.setPen(palette().dark().color());
    painter.setBrush(Qt::NoBrush);
    painter.end();

//    QStylePainter painter(this);
//    painter.drawPixmap(0, 0, m_pixBackBuffer);

//    if (rubberBandIsShown) {
//        painter.setPen(palette().light().color());
//        painter.drawRect(rubberBandRect.normalized()
//                                       .adjusted(0, 0, -1, -1));
//    }

//    if (hasFocus()) {
//        QStyleOptionFocusRect option;
//        option.initFrom(this);
//        option.backgroundColor = palette().dark().color();
//        painter.drawPrimitive(QStyle::PE_FrameFocusRect, option);
//    }
}

/**
 * @brief 마우스 Press 이벤트 처리
 * @param event
 */
void TransformFrame::mousePressEvent(QMouseEvent *event)
{
    //updateBackBuffer();
    if(event->button() == Qt::LeftButton && editable()){
       m_bPressed = true;
       m_nMPressGlobalX = event->globalX();
       m_nMPressGlobalY = event->globalY();
       m_rectMPressGeo = geometry();
       if(m_rectHideButton.contains(event->x(),event->y())){
           setEditable(false);
           m_bPressed = false;
           m_pSizeGrip->setVisible(false);
           this->setVisible(false);
       }
    }
    //qDebug() << "Mouse Press X : " << m_nMPressGlobalX << "Y : " << m_nMPressGlobalY << endl;
    QFrame::mousePressEvent(event);
}

/**
 * @brief 마우스 Release 이벤트 처리
 * @param event
 */
void TransformFrame::mouseReleaseEvent(QMouseEvent *event)
{
    m_bPressed = false;
    QFrame::mouseReleaseEvent(event);
}

/**
 * @brief 마우스 DoubleClick 이벤트 처리
 * @param event
 */
void TransformFrame::mouseDoubleClickEvent(QMouseEvent *event)
{
    if(editable()){
        setEditable(false);
        setCursor(Qt::ArrowCursor);
        m_pSizeGrip->setVisible(false);
    }else{
        setEditable(true);
        setCursor(Qt::SizeAllCursor);
        m_pSizeGrip->setVisible(true);
    }
    update();
    QFrame::mouseDoubleClickEvent(event);
}

/**
 * @brief 마우스 Move 이벤트 처리
 * @param event
 */
void TransformFrame::mouseMoveEvent(QMouseEvent *event)
{
    if(editable()){
        if(m_rectHideButton.contains(event->x(),event->y())){
            setCursor(Qt::ArrowCursor);
        }else{
            setCursor(Qt::SizeAllCursor);
        }
    }else{
        setCursor(Qt::ArrowCursor);
    }
    if(m_bPressed && editable()){
//        move(event->globalX() - m_nMClickCoordX - m_pParent->geometry().x(),
//             event->globalY() - m_nMClickCoordY - m_pParent->geometry().y());
        //qDebug() << "Mouse Move Global X : " << event->globalX() << "Global Y : " << event->globalY() << endl;
        //qDebug() << "Mouse Move Parent Geo X : " << m_pParent->geometry().x() << "Y : " << m_pParent->geometry().y() << endl;
        //qDebug() << "Mouse Move Pos X : " << event->pos().x() << "Y : " << event->pos().y() << endl;
        int nGlobalXDiff = event->globalX()  - m_nMPressGlobalX;
        int nGlobalYDiff = event->globalY()  - m_nMPressGlobalY;
        qDebug() << "Mouse Move Diff X : " << nGlobalXDiff << "Y : " << nGlobalYDiff << endl;
        //QPoint pos = mapToGlobal(rect.topLeft());
        move(nGlobalXDiff + m_rectMPressGeo.left(), nGlobalYDiff + m_rectMPressGeo.top());
    }
    QFrame::mouseMoveEvent(event);
}

/**
 * @brief Frame Resize 이벤트 처리
 * @param event
 */
void TransformFrame::resizeEvent(QResizeEvent *event)
{        
   m_pSizeGrip->setGeometry(width()-16,height()-16,16,16);
   if(m_pParent){
        m_pParent->update();
   }
   QFrame::resizeEvent(event);
}
