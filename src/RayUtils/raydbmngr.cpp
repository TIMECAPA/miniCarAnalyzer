#include "raydbmngr.h"

#include <QtSql>
#include <QMessageBox>

/**
 * @brief 생성자
 */
RayDBMngr::RayDBMngr()
{
}

/**
 * @brief 초기화
 */
void RayDBMngr::init()
{
    m_db = QSqlDatabase::addDatabase("QMYSQL");
    m_db.setHostName("garigulls.com");
    m_db.setDatabaseName("mobiusdb3");
    m_db.setUserName("root");
    m_db.setPassword("soft0987");
}

/**
 * @brief 데이터베이스 열기
 * @return
 */
bool RayDBMngr::open()
{
    bool bOpen = true;
    if(!m_db.isOpen()){
        if (!m_db.open()){
            QMessageBox::critical(0, QObject::tr("Database Error"), m_db.lastError().text());
            bOpen = false;
        }
    }
    //QSqlQuery query("SELECT * FROM cnt");
    return bOpen;
}

/**
 * @brief 데이터베이스 닫기
 */
void RayDBMngr::close()
{
    if(m_db.isOpen()){
        m_db.close();
    }
}

/**
 * @brief 데이터베이스 인스턴스 가져오기
 * @return
 */
QSqlDatabase* RayDBMngr::getDatabase()
{
    if(m_db.isOpen()){
        return &m_db;
    }
    return NULL;
}
