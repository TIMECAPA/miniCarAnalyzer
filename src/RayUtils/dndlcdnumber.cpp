#include "dndlcdnumber.h"
#include <QDebug>

/**
 * @brief 생성자
 * @param parent
 */
DnDLCDNumber::DnDLCDNumber(QWidget* parent)
    : QLCDNumber(parent)
{
    m_pPalette = new QPalette();

    m_dbVal = 0.0;

    setUseUpperLimit(false);
    setUseLowerLimit(false);

    setUpperLimit(std::numeric_limits<double>::max());
    setLowerLimit(std::numeric_limits<double>::lowest());

    updateLCDNumerColor(); //black


}

/**
 * @brief 생성자
 * @param numDigits
 * @param parent
 */
DnDLCDNumber::DnDLCDNumber(uint numDigits, QWidget* parent)
    : QLCDNumber(numDigits, parent)
{
    m_pPalette = new QPalette();
}

DnDLCDNumber::~DnDLCDNumber()
{

}

/**
 * @brief 값 설정
 * @param dbVal
 *
 */
void DnDLCDNumber::setValue(double dbVal)
{
    m_dbVal = dbVal;
    QLCDNumber::display(dbVal);
    updateLCDNumerColor();
}
/**
 * @brief 설정 값 가져오기
 * @return
 */
double DnDLCDNumber::value() const
{
    return m_dbVal;
}

/**
 * @brief 컨트롤의 색 업데이트 처리
 */
void DnDLCDNumber::updateLCDNumerColor()
{
    double dbVal = value();
    int nIndex = -1;
    double dbUpperLimit=std::numeric_limits<double>::max();
    double dbLowerLimit=std::numeric_limits<double>::lowest();
    if(useUpperLimit()){
        dbUpperLimit = upperLimit();
    }
    if(useLowerLimit()){
        dbLowerLimit  = lowerLimit();
    }

    if(dbVal <= dbUpperLimit && dbVal >= dbLowerLimit){
        nIndex = 2;//green
    }else if(dbVal > dbUpperLimit){
        nIndex = 1;//red
    }else if(dbVal < dbLowerLimit){
        nIndex = 0;//yellow
    }else{
        nIndex = -1;
    }

    // qDebug() << "updateLCDNumerColor Index : " << nIndex << endl;

    if(nIndex == 0){
        m_pPalette->setColor(QPalette::WindowText,Qt::yellow);
    }else if(nIndex==1){
        m_pPalette->setColor(QPalette::WindowText,Qt::red);
    }else if(nIndex==2){
        m_pPalette->setColor(QPalette::WindowText,Qt::green);
    }else if(nIndex==3){
        m_pPalette->setColor(QPalette::WindowText,Qt::blue);
    }else{
        m_pPalette->setColor(QPalette::WindowText,Qt::black);
    }
    this->setPalette(*m_pPalette);
}

/**
 * @brief 상한 값 설정
 * @param dbUpperLimit
 */
void DnDLCDNumber::setUpperLimit(double dbUpperLimit)
{
    m_dbUpperLimit = dbUpperLimit;
}

/**
 * @brief 상환 설정 값 가져오기
 * @return
 */
double DnDLCDNumber::upperLimit() const
{
    return m_dbUpperLimit;
}

/**
 * @brief 하한 값 설정
 * @param dbLowerLimit
 */
void DnDLCDNumber::setLowerLimit(double dbLowerLimit)
{
    m_dbLowerLimit = dbLowerLimit;
}

/**
 * @brief 하한 설정 값 가져오기
 * @return
 */
double DnDLCDNumber::lowerLimit() const
{
    return m_dbLowerLimit;
}

/**
 * @brief 상한 값 사용 설정
 * @param bUsage
 */
void DnDLCDNumber::setUseUpperLimit(bool bUsage)
{
    m_bUseUpperLimit = bUsage;
}

/**
 * @brief 상한 값 사용 설정 상태 가져오기
 * @return
 */
bool DnDLCDNumber::useUpperLimit() const
{
    return m_bUseUpperLimit;
}

/**
 * @brief 하한 값 사용 설정
 * @param bUsage
 */
void DnDLCDNumber::setUseLowerLimit(bool bUsage)
{
    m_bUseLowerLimit = bUsage;
}

/**
 * @brief 하한 값 사용 설정 상태 가져오기
 * @return
 */
bool DnDLCDNumber::useLowerLimit() const
{
    return m_bUseLowerLimit;
}
