#ifndef DNDLED_H
#define DNDLED_H

#include <QWidget>
class QSvgRenderer;

/**
 * @brief QWidget 확장 클래스
 */
class DnDLED : public QWidget
{
    Q_OBJECT
    Q_PROPERTY(double value READ value WRITE setValue)
    Q_PROPERTY(double upperLimit READ upperLimit WRITE setUpperLimit)
    Q_PROPERTY(double lowerLimit READ lowerLimit WRITE setLowerLimit)
    Q_PROPERTY(bool useUpperLimit READ useUpperLimit WRITE setUseUpperLimit)
    Q_PROPERTY(bool useLowerLimit READ useLowerLimit WRITE setUseLowerLimit)
public:
    explicit DnDLED(QWidget *parent = 0);
    ~DnDLED();
protected:
    void paintEvent(QPaintEvent *event);
private:
    /**
     * @brief SVG 렌더링 처리
     */
    QSvgRenderer *renderer ;

    /**
     * @brief 현재 값
     */
    double    m_dbVal;

    /**
     * @brief 상한 값
     */
    double    m_dbUpperLimit;

    /**
     * @brief 하한 값
     */
    double    m_dbLowerLimit;

    /**
     * @brief 상한 값 사용 상태
     */
    bool      m_bUseUpperLimit;

    /**
     * @brief 상한 값 사용 상태
     */
    bool      m_bUseLowerLimit;
public slots:
    void setValue(double dbVal);
    void setUpperLimit(double dbUpperLimit);
    void setLowerLimit(double dbLowerLimit);
    void setUseUpperLimit(bool bUsage);
    void setUseLowerLimit(bool bUsage);
public:
    void updateLEDColor();
    double value() const;
    double upperLimit() const;
    double lowerLimit() const;
    bool useUpperLimit() const;
    bool useLowerLimit() const;

};

#endif // DNDLED_H
