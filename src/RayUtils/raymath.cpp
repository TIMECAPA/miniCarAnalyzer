#include "raymath.h"
#include <QDebug>

/**
 * @brief 생성자
 */
RayMath::RayMath()
{

}



/**
 * @brief Cooley–Tukey FFT (in-place, divide-and-conquer),Higher memory requirements and redundancy although more intuitive
 * @param x
 */
void RayMath::fft(CArray& x)
{
    const size_t N = x.size();
    if (N <= 1) return;

    // divide
    CArray even = x[std::slice(0, N/2, 2)];
    CArray  odd = x[std::slice(1, N/2, 2)];

    // conquer
    fft(even);
    fft(odd);

    // combine
    for (size_t k = 0; k < N/2; ++k)
    {
        Complex t = std::polar(1.0, -2 * PI * k / N) * odd[k];
        x[k    ] = even[k] + t;
        x[k+N/2] = even[k] - t;
    }
}


/**
 * @brief Cooley-Tukey FFT (in-place, breadth-first, decimation-in-frequency), Better optimized but less intuitive
 * @param x
 */
void RayMath::DIF_FFT(CArray &x)
{
        // DFT
        unsigned int N = x.size(), k = N, n;
        double thetaT = 3.14159265358979323846264338328L / N;
        Complex phiT = Complex(cos(thetaT), sin(thetaT)), T;
        while (k > 1)
        {
                n = k;
                k >>= 1;
                phiT = phiT * phiT;
                T = 1.0L;
                for (unsigned int l = 0; l < k; l++)
                {
                        for (unsigned int a = l; a < N; a += n)
                        {
                                unsigned int b = a + k;
                                Complex t = x[a] - x[b];
                                x[a] += x[b];
                                x[b] = t * T;
                        }
                        T *= phiT;
                }
        }
        // Decimate
        unsigned int m = (unsigned int)log2(N);
        for (unsigned int a = 0; a < N; a++)
        {
                unsigned int b = a;
                // Reverse bits
                b = (((b & 0xaaaaaaaa) >> 1) | ((b & 0x55555555) << 1));
                b = (((b & 0xcccccccc) >> 2) | ((b & 0x33333333) << 2));
                b = (((b & 0xf0f0f0f0) >> 4) | ((b & 0x0f0f0f0f) << 4));
                b = (((b & 0xff00ff00) >> 8) | ((b & 0x00ff00ff) << 8));
                b = ((b >> 16) | (b << 16)) >> (32 - m);
                if (b > a)
                {
                        Complex t = x[a];
                        x[a] = x[b];
                        x[b] = t;
                }
        }
        //// Normalize (This section make it not working correctly)
        //Complex f = 1.0 / sqrt(N);
        //for (unsigned int i = 0; i < N; i++)
        //	x[i] *= f;
}


/**
 * @brief inverse fft (in-place)
 * @param x
 */
void RayMath::ifft(CArray& x)
{
    // conjugate the complex numbers
    x = x.apply(std::conj);

    // forward fft
    fft( x );

    // conjugate the complex numbers again
    x = x.apply(std::conj);

    // scale the numbers
    x /= x.size();
}


/**
 * @brief Vector 컨테이너 데이터의 평균 계산
 * @param vVal
 * @return
 */
double RayMath::avg(const std::vector<double>& vVal)
{
    double dbAvg=0.0;
//    int nSize = vVal.size();
//    if(nSize>0){
//        dbAvg = std::accumulate(vVal.begin(),vVal.end(),0);
//        qDebug() << dbAvg << endl;
//        dbAvg = dbAvg/nSize;
//    }
    std::vector<double>::const_iterator itBegin = vVal.cbegin();
    std::vector<double>::const_iterator itEnd   = vVal.cend();
    double dbYTotal=0.0;
    int nCnt=0;
    for(;itBegin != itEnd; itBegin++){
        dbYTotal += *itBegin;
        nCnt++;
    }

    if(nCnt != 0){
        dbAvg = dbYTotal/nCnt;
    }
    return dbAvg;
}

/**
 * @brief Vector 컨테이너 데이터의 P2P 계산
 * @param vNumbers
 * @return
 */
double RayMath::RayMath::p2p(const std::vector<double>& vNumbers)
{
    double dbP2P = 0.0;
    double dbMax =0.0,dbMin=0.0;
    int nSize = vNumbers.size();
    if(nSize>0){
        dbMax = *std::max_element(vNumbers.cbegin(),vNumbers.cend());
        dbMin = *std::min_element(vNumbers.cbegin(),vNumbers.cend());
        dbP2P = dbMax - dbMin;
    }
    return dbP2P;
}

/**
 * @brief Vector 컨테이너 데이터의 RMS 계산
 * @param vNumbers
 * @return
 */
double RayMath::rms(const std::vector<double>& vNumbers)
{
    double dbSum = 0.0;
    double dbRMS = 0.0;
    int nSize = vNumbers.size();
    if(nSize>0){
        std::vector<double>::const_iterator itBegin = vNumbers.cbegin();
        std::vector<double>::const_iterator itEnd   = vNumbers.cend();
        for(;itBegin != itEnd; itBegin++){
            dbSum += (*itBegin) * (*itBegin);
        }
        dbRMS = sqrt(dbSum/static_cast<double>(nSize));
    }
    return dbRMS;
}

/**
 * @brief Vector 컨테이너 데이터의 Variance 계산
 * @param vNumbers
 * @return
 */
double RayMath::variance(const std::vector<double>& vNumbers)
{
    double dbVar=0.0;
    int nSize = vNumbers.size();
    if(nSize>1){
        double dbAvg = avg(vNumbers);
        std::vector<double>::const_iterator itBegin = vNumbers.cbegin();
        std::vector<double>::const_iterator itEnd   = vNumbers.cend();
        for(;itBegin != itEnd;itBegin++){
            dbVar += (*itBegin - dbAvg) * (*itBegin - dbAvg);
        }
        dbVar = dbVar/(nSize-1) ;
    }
    return dbVar;
}

/**
 * @brief Vector 컨테이너 데이터의 표준 편차 계산
 * @param vNumbers
 * @return
 */
double RayMath::stdev(const std::vector<double>& vNumbers)
{
    double dbSnD = 0.0;
    double dbVar = variance(vNumbers);
    dbSnD = sqrt(dbVar);
    return dbSnD;
}
