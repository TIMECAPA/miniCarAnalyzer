#-------------------------------------------------
#
# Project created by QtCreator 2016-11-02T11:00:51
#
#-------------------------------------------------

QT       += network sql
QT       += gui
QT       += svg

greaterThan(QT_MAJOR_VERSION, 4): QT += printsupport

TARGET = rayUtils
TEMPLATE = lib
#CONFIG += staticlib

SOURCES += raydbmngr.cpp \
    raymath.cpp \
    dndtoolbar.cpp \
    transformframe.cpp \
    dndtoolbutton.cpp \
    dndtreewidget.cpp \
    dndled.cpp \
    dndlcdnumber.cpp

HEADERS += raydbmngr.h \
    raymath.h \
    dndtoolbar.h \
    transformframe.h \
    dndtoolbutton.h \
    dndtreewidget.h \
    dndled.h \
    dndlcdnumber.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}

DESTDIR  = ../../sdk/rayUtils/lib

RESOURCES += \
    rayutils.qrc


win32 {
    PWD_WIN = $${PWD}
    message( $${PWD_WIN})
    DESTDIR_WIN = $${OUT_PWD}
    message($${DESTDIR_WIN})
    PWD_WIN ~= s,/,\\,g
    message($${PWD_WIN})
    DESTDIR_WIN ~= s,/,\\,g
    message($${DESTDIR_WIN})
}
macx {
   # copyfiles.commands = cp -r $$PWD/copy_to_output/* $$OUT_PWD
}
linux {
   # copyfiles.commands = cp -r $$PWD/copy_to_output/* $$OUT_PWD
}

win32:CONFIG(release, debug|release): copyfiles.commands = $$quote(cmd /c xcopy /S /I $${PWD_WIN}\\copy_to_output $${DESTDIR_WIN})
else:win32:CONFIG(debug, debug|release): copyfiles.commands = $$quote(cmd /c xcopy /S /I $${PWD_WIN}\\copy_to_output $${DESTDIR_WIN})
message($${copyfiles.commands})
#QMAKE_EXTRA_TARGETS += copyfiles
#POST_TARGETDEPS += copyfiles

#mkpath
#@createdir.commands = $(MKDIR) $$OUT_PWD/uitests
#copyfiles.commands = $(COPY) $$PWD/$$OTHER_FILES $$OUT_PWD/uitests
#first.depends = $(first) createdir copyfiles
#export(first.depends)
#export(createdir.commands)
#export(copyfiles.commands)
#QMAKE_EXTRA_TARGETS += first createdir copyfiles
#@
#However, the solution does not explain a few things:
#'depends' and 'commands' are required? Are they for?
