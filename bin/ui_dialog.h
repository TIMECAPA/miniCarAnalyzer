/********************************************************************************
** Form generated from reading UI file 'dialog.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOG_H
#define UI_DIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextEdit>

QT_BEGIN_NAMESPACE

class Ui_Dialog
{
public:
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QTextEdit *textEdit_result;
    QPlainTextEdit *pTextEdit_paras;

    void setupUi(QDialog *Dialog)
    {
        if (Dialog->objectName().isEmpty())
            Dialog->setObjectName(QStringLiteral("Dialog"));
        Dialog->setEnabled(true);
        Dialog->resize(632, 499);
        Dialog->setModal(true);
        pushButton = new QPushButton(Dialog);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(230, 20, 171, 41));
        pushButton_2 = new QPushButton(Dialog);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(230, 70, 171, 41));
        pushButton_3 = new QPushButton(Dialog);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
        pushButton_3->setGeometry(QRect(250, 440, 161, 41));
        textEdit_result = new QTextEdit(Dialog);
        textEdit_result->setObjectName(QStringLiteral("textEdit_result"));
        textEdit_result->setGeometry(QRect(40, 150, 551, 241));
        pTextEdit_paras = new QPlainTextEdit(Dialog);
        pTextEdit_paras->setObjectName(QStringLiteral("pTextEdit_paras"));
        pTextEdit_paras->setGeometry(QRect(150, 400, 351, 31));

        retranslateUi(Dialog);

        QMetaObject::connectSlotsByName(Dialog);
    } // setupUi

    void retranslateUi(QDialog *Dialog)
    {
        Dialog->setWindowTitle(QApplication::translate("Dialog", "sensorCONTROL", 0));
        pushButton->setText(QApplication::translate("Dialog", "START", 0));
        pushButton_2->setText(QApplication::translate("Dialog", "PAUSE", 0));
        pushButton_3->setText(QApplication::translate("Dialog", "\353\262\224\354\234\204\353\215\260\354\235\264\355\204\260 \352\260\200\354\240\270\354\230\244\352\270\260", 0));
        pTextEdit_paras->setPlainText(QApplication::translate("Dialog", "cra=20151006T074218?crb=20161006T074218", 0));
    } // retranslateUi

};

namespace Ui {
    class Dialog: public Ui_Dialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOG_H
